using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Kelloggs
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            int error = httpException != null ? httpException.GetHttpCode() : 0;

            Server.ClearError();
            if (error == 500)
            {
                Response.Redirect(String.Format("~/Error500", error, exception.Message));
            }
            if (error == 404)
            {
                RouteData routeData = new RouteData();
                Response.Clear();
                Server.ClearError();
                routeData.Values.Add("controller", "Promo");
                routeData.Values.Add("action", "Error404");
                var requestContext = new RequestContext(new HttpContextWrapper(Context), routeData);
                var controller = ControllerBuilder.Current.GetControllerFactory().CreateController(requestContext, "Promo");

                controller.Execute(requestContext);
               // Response.Redirect(String.Format("~/Error404", error, exception.Message));
            }
            if (error == 0)
            {
                Response.Redirect(String.Format("~/Error500", error, exception.Message));
            }
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            HttpApplication context = (HttpApplication)sender;
            context.Response.SuppressFormsAuthenticationRedirect = true;
        }
    }
}
