﻿namespace PringlesHalo.Common
{
    public class Constantes
    {
        public const string DIR_UPLOAD_GALERIA = "~/Content/Uploads/Perfil";
        public const string DIR_UPLOAD = "~/Content/Uploads/Ticket";
        public const string PATH_ERR404 = "/Error404";
        public const string PATH_ERR500 = "/Error500";
        public const string MSJ_ERR_USUARIO_NO_ENCONTRADO = "No existe el usuario.";
        public const string MSJ_ERR_PERFIL_NO_ENCONTRADO = "No existe el perfil.";
        public const string MSJ_ERR_MATCH_NO_ENCONTRADO = "No existe el Match.";
    }
}