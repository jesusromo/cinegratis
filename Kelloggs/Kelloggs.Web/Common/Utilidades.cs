﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Kelloggs.Utils
{
    public class Utilidades
    {
        /// <summary>
        /// Obtiene la fecha de Central Standad Time.
        /// </summary>
        /// <returns>DateTime</returns>
        public static DateTime DateNow()
        {
            DateTime utcTime = DateTime.UtcNow;
            string nzTimeZoneKey = "Central Standard Time";
            TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
            DateTime nzDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, nzTimeZone);
            return nzDateTime;
        }

        /// <summary>
        /// Obtiene la fecha de Central Standad Time.
        /// </summary>
        /// <returns>DateTime</returns>
        public static DateTime FechaColmbia()
        {
            DateTime utcTime = DateTime.UtcNow;
            string nzTimeZoneKey = "SA Pacific Standard Time";
            TimeZoneInfo nzTimeZone = TimeZoneInfo.FindSystemTimeZoneById(nzTimeZoneKey);
            DateTime nzDateTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, nzTimeZone);
            return nzDateTime;
        }




        /// <summary>
        /// Obtiene una cadena de valores random.
        /// </summary>
        /// <returns>Cadena aleatoria.</returns>
        public static string RandomString()
        {
            return Guid.NewGuid().ToString();
        }

        /// <summary>
        /// Elimina un archivo.
        /// </summary>
        /// <param name="pathArchivo">Path del archivo a eliminar.</param>
        public static void EliminarArchivo(string pathArchivo)
        {
            if (File.Exists(pathArchivo))
                File.Delete(pathArchivo);
        }

        public static string GenerarSlug(string cadena)
        {
            string str = EliminarAcentos(cadena).ToLower();
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "");
            str = Regex.Replace(str, @"\s+", " ").Trim();
            str = str.Substring(0, str.Length <= 45 ? str.Length : 45).Trim();
            str = Regex.Replace(str, @"\s", "-");
            return str;
        }
        private static string EliminarAcentos(string cadena)
        {
            byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(cadena);
            return System.Text.Encoding.ASCII.GetString(bytes);
        }




    }

}