﻿namespace Kelloggs.Models.Clases
{
    public class RegisterDTO
    {
        public string __RequestVerificationToken { get; set; }
        public EpsilonUserDTO EpsilonUserDTO { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}