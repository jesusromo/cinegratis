﻿using System;

namespace Kelloggs.Models.Clases
{
    public class EpsilonUserDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public String Celular { get; set; }
        public string ProfileID { get; set; }
        public string UsernameForServiceAuth { get; set; }
        public DateTime BirthDate { get; set; }
        public string Cedula { get; set; }
        public string Gender { get; set; }
        public string FamilyRole { get; set; }
        public ProfileAttributesDTO ProfileAttributes { get; set; }

        public String NombreMescalina { get; set; }
        public String PaternoMescalina { get; set; }
        public String MaternoMescalina { get; set; }
        public String CelularMescalina { get; set; }

        public String AddressLine1 { get; set; }
        public String City { get; set; }

        public EpsilonUserDTO()
        {
            NombreMescalina = "Mescalina.Nombre";
            PaternoMescalina = "Mescalina.Paterno";
            MaternoMescalina = "Mescalina.Materno";
            CelularMescalina = "4444444444";
        }
    }

    public class ProfileAttributesDTO
    {
        public string PhoneNumberNoVal { get; set; }
        public string CEDULATEXT { get; set; }

        public string FamilyRole { get; set; }
    }
}