﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.Models.Clases
{
    public class RespuestaCodigo
    {
        public Boolean Error { get; set; }
        public String Texto { get; set; }

        public RespuestaCodigo()
        {
            Error = true;
        }
    }
}