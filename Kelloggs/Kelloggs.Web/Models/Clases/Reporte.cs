﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.Models.Clases
{
    public class Reporte
    {
        public String IdUsuario { get; set; }
        public String NumeroDeLote { get; set; }
        public String YearDeLote { get; set; }
        public String MonthDeLote { get; set; }
        public String TipoDeProducto { get; set; }
        public string UsuarioId { get; set; }
        public String NombreDeUsuario { get; set; }
        public String Nombre { get; set; }
        public String Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public String CorreoElectronico { get; set; }
        public String Direccion { get; set; }
        public String Ciudad { get; set; }
        public String Telefono { get; set; }
        public DateTime FechaDeCanje { get; set; }
        public int Puntaje { get; set; }
    }




    public class Reporte2
    {
        public string UsuarioId { get; set; }
        public string NombreDeUsuario { get; set; }
        public string CorreoElectronico { get; set; }
        public string ValorCodigo { get; set; }
        public int TipoDePremio { get; set; }
    }
}