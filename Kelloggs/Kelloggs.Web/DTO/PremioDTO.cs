﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class PremioDTO
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Marca { get; set; }
        public int Cantidad { get; set; }
        public string NombreArchivo { get; set; }
        public int CantidadDisponible { get; set; }
        public bool Canjeado { get; set; }
    }

    public class SettingsDTO
    {
        public string Id { get; set; }
    }
}