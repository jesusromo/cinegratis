﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class SubProductDTO
    {
        public string SubProductId { get; set; }
        public string ProductId { get; set; }
        public string Id { get; set; }
        public string SubProduct { get; set; }
    }
}