﻿namespace Kelloggs.DTO
{
    public class ParametersDto
    {
        public int Paged { get; set; }

        public int ItemsPaged { get; set; }
    }
}