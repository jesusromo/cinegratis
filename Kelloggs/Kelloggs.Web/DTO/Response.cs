﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Kelloggs.DTO
{
    public class Response
    {
        public string Message { get; set; }
        public bool Success { get; set; }
        public object Value { get; set; }

        public int Code { get; set; }
        public Guid TicketId { get; set; }
    }
}