﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class PremiosSemanaDTO
    {
        public int Semana { get; set; }
        public string Premio { get; set; }
        public string DescripcionPremio { get; set; }
        public string Codigo { get; set; }
    }
}