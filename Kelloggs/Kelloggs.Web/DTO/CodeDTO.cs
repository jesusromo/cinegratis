﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class CodeDTO
    {
        public string Lote { get; set; }
        public string FechaVencimiento { get; set; }
        public string TipoProductId { get; set; }
        public string ProductId { get; set; }
        public string SubproductId { get; set; }
    }
}