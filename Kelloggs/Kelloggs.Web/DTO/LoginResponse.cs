﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class LoginResponse
    {
        public bool Success { get; set; }
        public bool EmailError  { get; set; }
        public bool NickNameError  { get; set; }
    }
}