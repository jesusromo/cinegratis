﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class GanadorInfoDTO
    {
        public string GanadorRenaultKwidIconic { get; set; }

        public List<SemanaGanadorDto> SemanaGanadorMotoAkt { get; set; }
        public List<SemanaGanadorDto> SemanaGanadorTarjetaBancolombia { get; set; }
    }
    public class SemanaGanadorDto
    {
        public int Semana { get; set; }
        public List<string> Nombres { get; set; }
    }
}