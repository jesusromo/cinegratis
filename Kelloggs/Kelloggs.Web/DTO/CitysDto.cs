﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class CitysDto
    {
        public string Name { get; set; }

        public string Value { get; set; }
    }
}