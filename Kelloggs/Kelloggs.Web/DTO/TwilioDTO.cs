﻿namespace Kelloggs.DTO
{
    public class TwilioDTO
    {
        public string body { get; set; }
        public string from { get; set; }
        public string to { get; set; }
        public string mediaUrl { get; set; }
        public string keyCode { get; set; }
        public bool hasImage { get; set; }
    }
}