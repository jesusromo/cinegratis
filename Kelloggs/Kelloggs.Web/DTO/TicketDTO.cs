﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class TicketDTO
    {
        public Guid Id { get; set; }
        public string Lote { get; set; }
        public string FechaLote { get; set; }
        public string CodigoBarras { get; set; }
        public bool Status { get; set; }
        public string StatusStr { get; set; }
        public byte[] ImgBarras { get; set; }
        public string ImgBarrasStr { get; set; }
        public byte[] ImgLoteFecha { get; set; }
        public string ImgLoteFechaStr { get; set; }
        public double Puntaje { get; set; }
        public bool HasImgBarras { get; set; }
        public bool HasImgLote { get; set; }
        public DateTime FechaRegistro { get; set; }
        public bool Correcto { get; set; }


        public string FechaCanje { get; set; }
        public string HoraCanje { get; set; }
    }
}