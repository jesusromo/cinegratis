﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class GanadorSyncLog
    {
        public Guid UserId { get; set; }
        public Guid ProductId { get; set; }
        public int Entregado { get; set; }
    }
}