﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class PagedResultDto<T>
    {

        #region Public Properties

        /// <summary>
        /// Gets or sets the items.
        /// </summary>
        public List<T> Items { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public int Total { get; set; }

        #endregion
    }
}