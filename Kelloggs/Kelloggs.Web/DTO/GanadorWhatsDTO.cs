﻿namespace Kelloggs.DTO
{
    public class GanadorWhatsDTO
    {
        public System.Guid GanadorId { get; set; }
        public string ValorCodigo { get; set; }
        public string Telefono { get; set; }
    }
}