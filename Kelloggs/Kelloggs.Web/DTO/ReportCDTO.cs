﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class ReportCDTO
    {
        public string IdUsuario { get; set; }
        public string LoteA { get; set; }
        public string NombrePremio { get; set; }
        public string LoteB { get; set; }
        public string RegistroCompletado { get; set; }
        public string PremioEntregado { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string TipoProducto { get; set; }
        public string NombreUsuario { get; set; }
        public string Apellido { get; set; }
        public string FechaNacimiento { get; set; }
        public string Cedula { get; set; }
        public string CorreoElectonico { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Telefono { get; set; }
        public string FechaCanjeCompleta { get; set; }
        public string FechaCanje { get; set; }
        public string HoraCanje { get; set; }
        public DateTime FechaCargada { get; set; }
        public int Puntaje { get; set; }
    }




    public class ReportCDTO2
    {
        public string Lote { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public string TipoProducto { get; set; }
        public string NombreUsuario { get; set; }
        public string Apellido { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Cedula { get; set; }
        public string CorreoElectonico { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Telefono { get; set; }
        public DateTime FechaCanje { get; set; }
        public DateTime HoraCanje { get; set; }
    }
}