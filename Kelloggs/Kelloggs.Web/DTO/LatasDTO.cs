﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
	public class LatasDTO
	{
		public Guid Id { get; set; }
		public string ProductImageMembrana { get; set; }
		public string Usuario { get; set; }
		public DateTime FechaRegistro { get; set; }
		public string LoteA { get; set; }
		public string LoteB { get; set; }
		public string CorreoElectronico { get; set; }
		public bool Cargado { get; set; }

	}
}