﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class RangoFecha
    {
        public string FechaInicial { get; set; }

        public string FechaFinal { get; set; }
    }
}