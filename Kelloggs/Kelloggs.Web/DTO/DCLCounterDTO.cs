﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class DCLCounterDTO
    {
        public string Nombre { get; set; }
        public string Cantidad { get; set; }
    }
}