﻿namespace Kelloggs.DTO
{
	public class EtapasDto
	{
		public int Etapa { get; set; }
		public string Email { get; set; }
	}
}