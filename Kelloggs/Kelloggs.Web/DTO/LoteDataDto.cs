﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kelloggs.DTO
{
    public class LoteDataDto
    {
        public string LoteA { get; set; }
        public string LoteB { get; set; }
        public Guid TicketId { get; set; }
    }
}
