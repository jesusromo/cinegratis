﻿(function () {
	'use strict';

	angular.module('promoApp')
		.controller('main', mainController);

	mainController.$inject = ['$scope', 'cloud', '$http', '$rootScope', '$timeout', 'fileUpload', '$window'];

	function mainController($scope, cloud, $http, $rootScope, $timeout, fileUpload, $window) {
		$scope.etapaActual = 0;
		$scope.usuario = JSON.parse(localStorage.getItem('Usuario'));
		$scope.loading = false;
        $scope.ingresalotes = false;
		$scope.listaConverzacion = [];
        $scope.dontCloseLogin = false;
		$scope.texto = "";
		$scope.currentMembranaId = "";
        $scope.loteParams = {
			LoteA: "",
			LoteB: "",
			TicketId : ""
        };
        $scope.ultimaBoton = {};
		$scope.uploadFile = uploadFile;
		$scope.UsuarioAutentificado = false;
        $scope.lotestextoCount = 0;
		$scope.nuevaHora = function () {
			return new Date();
		};
	 $scope.pintaRespuesta = function(texto) {
            $scope.listaConverzacion.push({
				Hora: $scope.nuevaHora(),
				Texto: texto,
                Izquierda: false
			});
			setTimeout(function () {
                document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
            }, 100);
		}

        $scope.pintaGloboRojo = function (texto) {
            $scope.listaConverzacion.push({
                Hora: $scope.nuevaHora(),
                Texto: texto,
				Izquierda: true,
				EsBoton: false,
                NombreImagen: "11"
            });
			setTimeout(function () {
                document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
            }, 100);
		}

        $scope.pintaGloboRojo2 = function (texto,imgNumber) {
            $scope.listaConverzacion.push({
                Hora: $scope.nuevaHora(),
                Texto: texto,
                Izquierda: true,
                EsBoton: false,
                NombreImagen: imgNumber
            });
            setTimeout(function () {
                document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
            }, 100);
        }

		$scope.goHome = function() {
            window.location.href = "/";
        }

		$scope.EtapaActiva = function (etapa) {
			if (!etapa.EsBoton) {
				return;
			}
			if (etapa.Etapa == $scope.etapaActual || (etapa.Etapa == 1 && !$scope.Logueado()) || ((etapa.Etapa == 5) && (etapa.Etapa == $scope.etapaActual-1) )) {
				$scope.SiguinteEtapa(etapa);
			}
		}

		$scope.irARedes = function(item) {
            switch (item) {
				case 1:
					window.open('https://www.instagram.com/pringles_latam/', '_blank');
					breaK;
				case 2:
					window.open('https://www.facebook.com/PringlesLA', '_blank');
					breaK;
				case 3:
					window.open('https://twitter.com/Pringles_LATAM', '_blank');
                    breaK;
                default:
            }
        }

		$scope.SiguinteEtapa = function (etapa) {

            $('#loaderFull').removeClass("d-none");
            $('.loader-img').removeClass("d-none");
            $scope.loading = true;

			if (etapa.SiguienteEtapa == 11) {
                $scope.loading = false;

				return
			} if (etapa.SiguienteEtapa == 14) {
				$scope.descarga();
                $scope.loading = false;
				return
			}
			if (etapa.SiguienteEtapa == 15) {
				localStorage.removeItem('Usuario');
				$scope.usuario = null;
				$scope.listaConverzacion = [];
				$scope.etapaActual = 0;
				$scope.inicio();
                $scope.loading = false;
                return
			}
			if (etapa.SiguienteEtapa == 16) {
				$scope.listaConverzacion = [];
				$scope.etapaActual = 0;
				$scope.inicio();
                $scope.loading = false;
                return
			}

			if (etapa.Texto != "NoPintar") {
                $scope.pintaRespuesta(etapa.Texto);
            }
            
            if (etapa.SiguienteEtapa == 12 || etapa.SiguienteEtapa == 13) {
				console.log("iniciando registro o login");
				$scope.iniciarFormulario(etapa.SiguienteEtapa == 13 ? true : false);  /* --> ya solo mandamos si es registro = true y si no es registro = false*/
                $scope.dontCloseLogin = true;
				$("#registroPage").removeClass("d-none");
				$("#chatPage").addClass("d-none");
				var parametros = {
					"Etapa": $scope.ultimaBoton.SiguienteEtapa,
					"Email": $scope.usuario ? $scope.usuario.UsernameForServiceAuth : "",
				};
            } else {
				$scope.ultimaBoton = etapa;
				$scope.etapaActual = etapa.SiguienteEtapa;
				var parametros = {
					"Etapa": etapa.SiguienteEtapa,
					"Email": $scope.usuario ? $scope.usuario.UsernameForServiceAuth:"",
				};
            }
			if (!$scope.Logueado() && ($scope.etapaActual == 3 || $scope.etapaActual == 4) && (etapa.SiguienteEtapa != 12 && etapa.SiguienteEtapa != 13)) {

				var parametros = {
					"Etapa": 1,
					"Email": $scope.usuario ? $scope.usuario.UsernameForServiceAuth : "",
				};
			}
            $scope.loading = true;
			$http({ method: "POST", url: "/BuscarSiguienteEtapa", params: parametros })
				.success(function (result) {
					result.Value.forEach(function (sms) {
						sms.Hora = $scope.nuevaHora();
						$scope.listaConverzacion.push(sms);
					});
					setTimeout(function () {
						document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
                        
					}, 100);
					if (!$scope.dontCloseLogin) {
                        $scope.loading = false;
                    } else {
                        $scope.dontCloseLogin = false;
                    }
                    

				}).error(function (result) {

				});

		}
		$scope.Logueado = function () {
			if ($scope.usuario) {
				return true;
			} else {
				return false;
			}
		}
		$scope.fileNameChanged = function () {
			var file = $scope.imageFile;
            var uploadUrl = '/api/uploadMembrana';
            uploadFileToUrl(file, uploadUrl);
		}

		function uploadFile(event) {
			var files = event.target.files;
			if (files.length != 1) {
                console.log("solo adjunta una imagen");
            } else {
				var file = files[0];
                var uploadUrl = '/api/uploadMembrana';
				uploadFileToUrl(file, uploadUrl);
            }
		}

		$scope.validarLote = function(text) {
			var lataGrande = /^([a-zA-Z])[0-9a-zA-Z]{7}[0-9]{1}[0-9]{4}$/.test(text);
			var lataChica = /^[0-9a-zA-Z]{7}[a-zA-Z]{1}[0-9]{4}$/.test(text);
			var lataAny = /^[0-9a-zA-Z]{7}[0-9a-zA-Z]{1}[0-9]{4}$/.test(text);
			var lataguion = /^[0-9a-zA-Z]{7}[0-9]{2}-[0-9]{4}$/.test(text);
			if (lataGrande || lataChica || lataAny || lataguion) {
                return true;
            } else {
                return false;
            }
        }

		$scope.finalizaLote = function() { //TODO: verificar los formatos con expresion regular
			$scope.lotestextoCount++;
			if ($scope.lotestextoCount == 1) {
                $scope.pintaRespuesta($scope.currentText);
				if (!$scope.validarLote($scope.currentText)) {
					$scope.lotestextoCount--;
					$scope.pintaGloboRojo2("","12");
                } else {
					$scope.loteParams.LoteA = $scope.currentText;
					$scope.pintaGloboRojo2("¡Muy bien!, ahora escribe tu segundo lote.","");
                    $scope.currentText = "";
                }
            } else {
				if ($scope.lotestextoCount == 2) {
					$scope.pintaRespuesta($scope.currentText);
					if (!$scope.validarLote($scope.currentText)) {
                        $scope.lotestextoCount--;
						$scope.pintaGloboRojo2("", "12");
                    } else {
                        $scope.loteParams.LoteB = $scope.currentText;
                        $scope.ingresalotes = false;
						$scope.currentText = "";
						//Carga de lotes a db
						$scope.loading = true;
						$http({ method: "POST", url: "/ActualizaLote", params: $scope.loteParams })
							.success(function (result) {
								if (result.Success) {
									$scope.loading = false;
									$scope.SiguinteEtapa({
										EsBoton: true,
										Etapa: 8,
										Hora: "",
										Izquierda: true,
										NombreImagen: "",
										PreguntaId: 25,
										SiguienteEtapa: 9,
										Texto: "NoPintar"
									});
									$scope.descargaUnica();
									setTimeout(function () {
										document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
									}, 100);
								} else {
									if (result.Message == "NOPREMIO") {
										$scope.loading = false;
										$scope.SiguinteEtapa({
											EsBoton: true,
											Etapa: 10,
											Hora: "",
											Izquierda: true,
											NombreImagen: "",
											PreguntaId: 28,
											SiguienteEtapa: 17,
											Texto: "NoPintar"
										});
									} else {
										$scope.loading = false;
										$scope.SiguinteEtapa({
											EsBoton: true,
											Etapa: 10,
											Hora: "",
											Izquierda: true,
											NombreImagen: "",
											PreguntaId: 28,
											SiguienteEtapa: 10,
											Texto: "NoPintar"
										});
									}
									setTimeout(function () {
										document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
									}, 100);
								}

							}).error(function (result) {

							});
                    }
                }
            }
        }
		$scope.inicio = function () {
			if (!$scope.Logueado()) {
				$scope.listaConverzacion.push({ "Etapa": 0, "Izquierda": true, "Texto": "¡Hola, aquí Mr. P!", "Hora": $scope.nuevaHora(), "EsBoton": false, "SiguienteEtapa": 11, "NombreImagen": "0" });
				$scope.listaConverzacion.push({ "Etapa": 0, "Izquierda": true, "Texto": " Yo voy a ayudarte a conseguir esa entrada de CINE GRATIS que tanto deseas ¿Ya sabes cómo hacerle para ganar una entrada de CINE GRATIS?", "Hora": $scope.nuevaHora(), "EsBoton": false, "SiguienteEtapa": 11, "NombreImagen": "" });
				$scope.listaConverzacion.push({ "Etapa": 0, "Izquierda": true, "Texto": 'Si', "Hora": $scope.nuevaHora(), "EsBoton": true, "SiguienteEtapa": 3, "NombreImagen": "" });
				$scope.listaConverzacion.push({ "Etapa": 0, "Izquierda": true, "Texto": 'No', "Hora": $scope.nuevaHora(), "EsBoton": true, "SiguienteEtapa": 2, "NombreImagen": "" });

			} else {
				$scope.SiguinteEtapa({ "Etapa": 2, "Izquierda": true, "Texto": 'NoPintar', "Hora": $scope.nuevaHora(), "EsBoton": true, "SiguienteEtapa": 4, "NombreImagen": "" });
			}
		};
		$scope.inicio();


		function uploadFileToUrl(file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);
            $scope.loading = true;
            $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                })
				.success(function (data) {
					if (data.Success) {
                        $scope.currentMembranaId = data.TicketId;
                        $scope.ingresalotes = true;
                        $scope.loading = false;
						$scope.pintaGloboRojo("Ahora escribe en la conversación el lote de tus productos, cada uno por separado.");
                        $scope.lotestextoCount = 0;
                        $scope.loteParams = {
                            LoteA: "",
                            LoteB: "",
                            TicketId: data.TicketId
                        };
                    } else {
                        $scope.loading = false;
                        $scope.SiguinteEtapa({
                            EsBoton: true,
                            Etapa: 18,
                            Hora: "",
                            Izquierda: true,
                            NombreImagen: "",
                            PreguntaId: 44,
                            SiguienteEtapa: 18,
                            Texto: "NoPintar"
                        });
                        setTimeout(function () {
                            document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
                        }, 100);
                    }
					
                })
                .error(function () {
                    alert("Error intentalo de nuevo");
                });
        }
		$scope.descarga = function () {
			$http({
				method: 'GET',
				url: 'Descarga',
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				headers = headers();

				var filename = "Pringles_CineGratis_" + new Date().toISOString().slice(0, 10) + ".zip";
				var contentType = headers['content-type'];

				var linkElement = document.createElement('a');
				try {
					var blob = new Blob([data], { type: contentType });
					var url = window.URL.createObjectURL(blob);

					linkElement.setAttribute('href', url);
					linkElement.setAttribute("download", filename);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});
					linkElement.dispatchEvent(clickEvent);
				} catch (ex) {
					console.log(ex);
				}
			}).error(function (data) {
				console.log(data);
			});
		};

		$scope.descargaUnica = function () {
			$http({
				method: 'GET',
				url: 'DescargaUnica',
				responseType: 'arraybuffer'
			}).success(function (data, status, headers) {
				headers = headers();

				var filename = "Pringles_CineGratis_" + new Date().toISOString().slice(0, 10) + ".zip";
				var contentType = headers['content-type'];

				var linkElement = document.createElement('a');
				try {
					var blob = new Blob([data], { type: contentType });
					var url = window.URL.createObjectURL(blob);

					linkElement.setAttribute('href', url);
					linkElement.setAttribute("download", filename);

					var clickEvent = new MouseEvent("click", {
						"view": window,
						"bubbles": true,
						"cancelable": false
					});
					linkElement.dispatchEvent(clickEvent);
				} catch (ex) {
					console.log(ex);
				}
			}).error(function (data) {
				console.log(data);
			});
		};

		//EPSILON
		$scope.iniciarFormulario = function (esRegistroParam) {
            $scope.loading = true;
			var hash = window.location.pathname;
			var container = document.getElementById('reg-container');
			var esRegistro = esRegistroParam;
			var estado = "stage";

			if (window.location.origin.includes('stage') ||
                window.location.origin.includes('azurewebsites') ||
                window.location.origin.includes('ngrok')) {
                estado = "stage";
            } else {
                estado = "prod";
            }

			var configObj = {
				environment: estado, // or 'prod', 'stage' for testing*/
				moduleKey:
					'b48e6675-dcc3-46a3-9447-084b83b4243f', //'08b7232a-c989-4e17-bfc0-124ef20e7b12', // from the module admin tool
				showLogin: !esRegistro,
				showSignup: esRegistro,
				onProfileSave: function (authUser, callback) {
					var form = $('#__AjaxAntiForgeryForm');
					var token = $('input[name="__RequestVerificationToken"]', form).val();
					$scope.usuario = authUser;
					localStorage.setItem('Usuario', JSON.stringify(authUser));
					$.ajax('/Authenticate/Registro', {
						type: 'POST',
						data: {
							__RequestVerificationToken: token,
							EpsilonUserDTO: authUser
						}
					}).done(function (data) {
						$(".registropage").addClass("d-none");
						$("#chatPage").removeClass("d-none");
						setTimeout(function () {
							document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
						}, 100);
					});
				},

				onAuth: function (authUser, callback) {
					var form = $('#__AjaxAntiForgeryForm');
					var token = $('input[name="__RequestVerificationToken"]', form).val();
					authUser.Celular = $('#KSTL-Registration-PhoneNumber2').val();
					authUser.BirthDate = $('#KSTL-Registration-BirthDate').val();
					authUser.Cedula = $('#KSTL-Registration-CedulaText').val();
					authUser.Gender = $('#KSTL-Registration-Gender').val();
					authUser.FamilyRole = $('#KSTL-Registration-FamilyRole').val();
					$scope.usuario = authUser;
					localStorage.setItem('Usuario', JSON.stringify(authUser));
					$.ajax('/Authenticate/Registro', {
						type: 'POST',
						data: {
							__RequestVerificationToken: token,
							EpsilonUserDTO: authUser
						},
						success: callback
					}).done(function (data) {
						//esRegistro
						if (esRegistro) {
							// ANALYTICS                   
							if (gtag) {
							    gtag('event', 'registrousuario', { 'event_category': 'usuariosregistrados', 'event_label': 'registrousuario' });
							}
						} else {
							//Login
							if (gtag) {
							    gtag('event', 'login', { 'event_category': 'login', 'event_label': 'login' });
							}
						}

						//console.log(data);
					});
				},
				onExit: function (authUser) {
					$("#chatPage").removeClass("d-none");
					$("#registroPage").addClass("d-none");
					setTimeout(function () {
						document.getElementById("chat").scrollTop = document.getElementById("chat").scrollHeight;
					}, 100);
				}
			};

			function changeValue(input, value) {
				var nativeInputValueSetter = Object.getOwnPropertyDescriptor(
					window.HTMLInputElement.prototype,
					"value"
				).set;
				nativeInputValueSetter.call(input, value);
				var inputEvent = new Event("input", { bubbles: true });
				input.dispatchEvent(inputEvent);
			}

			function initReg() {
                $scope.loading = true;
				var regModule = KSTL.Registration.init(container, configObj);
				// subscribe to module event notifications
				var eventViewRender = regModule.bind('view:render', function (topic, view) {
					$("input").attr('autocomplete', 'off');
					var f = new Date();
					f.setFullYear(f.getFullYear() - 18);
					f = transformarFecha(f);
					$('#KSTL-Registration-BirthDate').attr("type", "text");

					$("#KSTL-Registration-wrapper-FirstName label").text("NOMBRE");
					$("#KSTL-Registration-wrapper-LastName label").text("APELLIDOS");
					$("#KSTL-Registration-wrapper-UserName label").text("EMAIL");
					$("#KSTL-Registration-wrapper-YourEmailAddress label").text("E-MAIL");
					$("#KSTL-Registration-wrapper-MobilePhone label").text("TELÉFONO");
					$("#KSTL-Registration-wrapper-PhoneNumber2 label").text("TELÉFONO");
					$("#KSTL-Registration-wrapper-BirthDate label").text("FECHA DE NACIMIENTO");
					$("#KSTL-Registration-wrapper-AddressLine1 label").text("DIRECCIÓN");
					$("#KSTL-Registration-wrapper-City label").text("CIUDAD");
					$("#KSTL-Registration-wrapper-Password label").text("CONTRASEÑA");
					$("#KSTL-Registration-wrapper-PasswordConfirm label").text("CONFIRMA TU CONTRASEÑA");
					$("#KSTL-Registration-wrapper-Gender label").remove();
					$("#KSTL-Registration-wrapper-Gender option[value='']").html("GÉNERO");
					$("#KSTL-Registration-wrapper-FamilyRole label").text("ROL EN LA FAMILIA");
					$("#KSTL-Registration-wrapper-TermsConditionPrvcyPolicyCombo label").html(" RECONOZCO HABER LEÍDO Y ENTENDIDO LA <a class=\"link linkRojo\" target=\"_blank\" href=\"https://www.kelloggs.com.co/es_CO/politicas-de-privacidad.html\">POLÍTICA DE PRIVACIDAD</a>");
					$("#KSTL-Registration-wrapper-HeaderSubscription label").html("ACEPTO Y AUTORIZO EL TRATAMIENTO/USO DE MIS DATOS PERSONALES CONFORME ESTA <a class=\"link linkRojo\" target=\"_blank\" href=\"/Autorizacion\">AUTORIZACIÓN</a> ");
					//Datepicker
					if (esRegistro) {
						if ($("#VerContrasena").length == 0) {
							//Vanilla JS datepicker
							var f = new Date();
							f.setFullYear(f.getFullYear() - 18);
							f = transformarFecha(f);
							$('#KSTL-Registration-BirthDate').attr("type", "text");
							$('#KSTL-Registration-BirthDate').addClass("d-none");
							$('#KSTL-Registration-City').addClass("d-none");
                            $('#KSTL-Registration-BirthDate').parent().append('<input name="MyDatePicker" type="text" id="MyDatePicker" placeholder="FECHA DE NACIMIENTO" class="BirthDate form-control form-control" required="" tooltip="Introduce tu fecha de nacimiento" autocomplete="off">');
							$("#MyDatePicker").attr("min", f);
							var inputBirthDate = document.querySelector('input[name="MyDatePicker"]');
							var datepicker1 = new Datepicker(inputBirthDate, {
								autohide: true,
								todayHighlight: true,
								language: 'es-CO',
								format: 'yyyy/mm/dd',
								updateOnBlur: true
							});
							$('#MyDatePicker').on('changeDate', function (e) {
								if ($('#MyDatePicker').val() != "")
									changeValue(document.querySelector('input[name="BirthDate"]'), $('#MyDatePicker').val());
								$('#KSTL-Registration-BirthDate').addClass("d-none");
							});
							//Select2 para ciudaddes
							$('#KSTL-Registration-City').parent().append('<select class="form-control" id="MySelectCiudad" name="MySelectCiudad" tooltip="Introduce tu ciudad"></select>');
							$("#MySelectCiudad").select2({
								placeholder: 'CIUDAD',
								allowClear: true,
								width: '100%',
								dropdownAutoWidth: true,
								ajax: {
									url: "/api/getCitys",
									dataType: 'json',
									delay: 250,
									data: function (params) {
										return {
											searchTerm: params.term,
											page: params.page
										};
									},
									processResults: function (data, params) {
										params.page = params.page || 1;
										return {
											results: $.map(data.result, function (citys) {
												return {
													text: citys.Name,
													id: citys.Value
												};
											}),
											pagination: {
												more: (params.page * 30) < data.result.length
											}
										};
									},
									cache: true
								},
								minimumInputLength: 3,
								language: "es"
							});
							$('#MySelectCiudad').on('change', function () {
								var ciudad = $("#MySelectCiudad option:selected").text();
								changeValue(document.querySelector('input[name="City"]'), ciudad);
								$('#KSTL-Registration-City').addClass("d-none");
							})
							//Quitar punto de mas Suscribete a nuestro newsletter.
							$('#KSTL-Registration-wrapper-HeaderSubscription div:first').remove();

							//Select para Genero
							/* $('#KSTL-Registration-Gender').parent().append('<select class="form-control" id="MySelectGenero" name="MySelectGenero" title="GÉNERO" tooltip="GÉNERO">' +
								 '<option value="" disabled selected>GÉNERO</option>' +
								 '<option value="HOMBRE">HOMBRE</option>' +
								 '<option value="MUJER">MUJER</option>' +
								 '<option value="OTRO">OTRO</option>' +
								 '</select>');
							 $('#MySelectGenero').on('change', function () {
								 changeValue(document.querySelector('input[name="Gender"]'), this.value);
							 });*/
							//Select para rol en la familia
							$('#KSTL-Registration-FamilyRole').parent().append('<select class="form-control" id="MySelectFamilyRole" name="MySelectFamilyRole" title="ROL EN LA FAMILIA" tooltip="ROL EN LA FAMILIA">' +
								'<option value="" disabled selected>ROL EN LA FAMILIA</option>' +
								'<option value="Madre">Madre</option>' +
								'<option value="Padre">Padre</option>' +
								'<option value="Hij@ Mayor">Hij@ Mayor</option>' +
								'<option value="Hij@ Medio">Hij@ Medio</option>' +
								'<option value="Hij@ Menor">Hij@ Menor</option>' +
								'<option value="Otro">Otro</option>' +
								'</select>');
							$('#MySelectFamilyRole').on('change', function () {
								changeValue(document.querySelector('input[name="FamilyRole"]'), this.value);
							});
						}
						//fin de Datepicker
						// wrapper and inputs containers
						//var formWrapper = document.querySelector('div[form-fields]');
						var formWrapper = document.querySelector('div .ng-form');
						formWrapper.className += ' form-wrapper';
						var formChildElems = formWrapper.querySelectorAll(':scope > div');
						// title
						formChildElems[0].className += ' title-container';
						// start in 1 to ignore title, minus 2 to ignore submit button and errors container
						for (var i = 1; i < formChildElems.length - 2; i += 1) {
							formChildElems[i].className += ' form-group';
						}
						// button container
						formChildElems[formChildElems.length - 1].className += ' button-container';
					}
					else {
						//Para checar si es el formulario de completar datos de Login
						if ($('#KSTL-Registration-wrapper-HeaderCopyPage1').length > 0) {
							if ($("#VerContrasena").length == 0) {
								$('#KSTL-Registration-City').parent().append('<select class="form-control" id="MySelectCiudad" name="MySelectCiudad" tooltip="Introduce tu ciudad"></select>');
								$("#MySelectCiudad").select2({
									placeholder: '',
									allowClear: true,
									width: '100%',
									dropdownAutoWidth: true,
									ajax: {
										url: "/api/getCitys",
										dataType: 'json',
										delay: 250,
										data: function (params) {
											return {
												searchTerm: params.term,
												page: params.page
											};
										},
										processResults: function (data, params) {
											params.page = params.page || 1;
											return {
												results: $.map(data.result, function (citys) {
													return {
														text: citys.Name,
														id: citys.Value
													};
												}),
												pagination: {
													more: (params.page * 30) < data.result.length
												}
											};
										},
										cache: true
									},
									minimumInputLength: 3,
									language: "es"
								});
								$('#MySelectCiudad').on('change', function () {
									var ciudad = $("#MySelectCiudad option:selected").text();
									changeValue(document.querySelector('input[name="City"]'), ciudad);
								})
								//Quitar punto de mas Suscribete a nuestro newsletter.
								$('#KSTL-Registration-wrapper-HeaderSubscription div:first').remove();

								//Select para Genero
								/*$('#KSTL-Registration-Gender').parent().append('<select class="form-control" id="MySelectGenero" name="MySelectGenero" title="GÉNERO" tooltip="GÉNERO">' +
									'<option value="" disabled selected>GÉNERO</option>' +
									'<option value="HOMBRE">HOMBRE</option>' +
									'<option value="MUJER">MUJER</option>' +
									'<option value="OTRO">OTRO</option>' +
									'</select>');
								$('#MySelectGenero').on('change', function () {
									changeValue(document.querySelector('input[name="Gender"]'), this.value);
								});*/
								//Select para rol en la familia
								$('#KSTL-Registration-FamilyRole').parent().append('<select class="form-control" id="MySelectFamilyRole" name="MySelectFamilyRole" title="ROL EN LA FAMILIA" tooltip="ROL EN LA FAMILIA">' +
									'<option value="" disabled selected>ROL EN LA FAMILIA</option>' +
									'<option value="Madre">Madre</option>' +
									'<option value="Padre">Padre</option>' +
									'<option value="Hij@ Mayor">Hij@ Mayor</option>' +
									'<option value="Hij@ Medio">Hij@ Medio</option>' +
									'<option value="Hij@ Menor">Hij@ Menor</option>' +
									'<option value="Otro">Otro</option>' +
									'</select>');
								$('#MySelectFamilyRole').on('change', function () {
									changeValue(document.querySelector('input[name="FamilyRole"]'), this.value);
								});


								$(".subTitleIniciaSesion > span").html("COMPLETA TUS DATOS DE REGISTRO");
								$(".subTitleIniciaSesion").addClass("subtitleCompletaDatos");
								$("#KSTL-Registration-wrapper-HeaderCopyPage1").remove();
								$("#KSTL-Registration-wrapper-FirstName").addClass("d-none");
								$("#KSTL-Registration-wrapper-LastName").addClass("d-none");
								$("#KSTL-Registration-wrapper-BirthDate").addClass("d-none");
								$('#KSTL-Registration-City').addClass("d-none");
								if ($("#KSTL-Registration-AddressLine1").val())
									$("#KSTL-Registration-wrapper-AddressLine1").addClass("d-none");
								if ($("#KSTL-Registration-City").val())
									$("#KSTL-Registration-wrapper-City").addClass("d-none");
								if ($("#KSTL-Registration-FamilyRole").val())
									$("#KSTL-Registration-wrapper-FamilyRole").addClass("d-none");
								if ($("#KSTL-Registration-Gender").val())
									$("#KSTL-Registration-wrapper-Gender").addClass("d-none");
								if ($("#KSTL-Registration-PhoneNumber2").val())
									$("#KSTL-Registration-wrapper-PhoneNumber2").addClass("d-none");
								//$(".subscription-EM:first").remove();
								$("#KSTL-Registration-TermsConditionPrvcyPolicyCombo").addClass("inputCompletaDatos");
								$("#KSTL-Registration-wrapper-HeaderSubscription input").addClass("inputCompletaDatos");
								//QUito el subscription Id repetido
								$("#KSTL-Registration-wrapper-HeaderSubscription > div div:nth-child(2)").remove();
								$(".containerLogin").removeClass("containerLogin").addClass("containerCompletaDatos");
								$("#KSTL-Registration-wrapper-CedulaText").addClass("cedulaCompletaDatos");
								$("#KSTL-Registration-wrapper-AddressLine1").addClass("addressLineCompletaDatos");
								$("#KSTL-Registration-wrapper-City").addClass("cityCompletaDatos");
								$("#KSTL-Registration-wrapper-FamilyRole").addClass("familyRoleCompletaDatos");
								$("#KSTL-Registration-wrapper-Gender").addClass("genderCompletaDatos");
								$("#KSTL-Registration-wrapper-PhoneNumber2").addClass("phoneNumberCompletaDatos");
								$("#KSTL-Registration-wrapper-TermsConditionPrvcyPolicyCombo").addClass("termsCompletaDatos");
								$("#KSTL-Registration-wrapper-HeaderSubscription").addClass("newsLetterCompletaDatos");
								$("#KSTL-Registration-wrapper-ContinueButton1").addClass("continueBtnCompletaDatos");
								$(".not-register").remove();
							}
						}
					}
					// change button
					var submitBtn = document.querySelector('div[form-fields] button');
					// submitBtn.className += ' btn btn--default';    <- Cambios React

					//ver contraseña
					if ($("#VerContrasena").length == 0) {
						$(document).on('click',
							".toggle-password",
							function () {
								$(this).toggleClass("glyphicon-eye-close glyphicon-eye-open");
								var input = $($(this).attr("toggle"));
								if (input.attr("type") == "password") {
									input.attr("type", "text");
								} else {
									input.attr("type", "password");
								}
							});
					}

					//agrego span para password
					var inputPassword = $("#KSTL-Registration-Password");
					if ($("#VerContrasena").length == 0)
						inputPassword.after("<span id='VerContrasena' tabindex='0' toggle='#KSTL-Registration-Password' class='glyphicon glyphicon-eye-close field-icon toggle-password'></span>");
					if (esRegistro) {
						var inputPassword2 = $("#KSTL-Registration-PasswordConfirm");
						if ($("#VerContrasena2").length == 0)
							inputPassword2.after("<span id='VerContrasena2' tabindex='0' toggle='#KSTL-Registration-PasswordConfirm' class='glyphicon glyphicon-eye-close field-icon toggle-password'></span>");
					}

					// quito el h2 de Registrate hoy
					if ($('h2').length != 0)
						var heading = document.querySelector('h2').remove();

					// remove top copy
					if (esRegistro) {
						$('#KSTL-Registration-wrapper-HeaderPage1').remove();
						$('#KSTL-Registration-wrapper-HeaderCopyPage1').remove();
						$("#KSTL-Registration-wrapper-ContinueButton1 button span").text("REGÍSTRATE");
					}
					if (!esRegistro) {
						// remove top copy
						$('#KSTL-Registration-wrapper-ForgotPasswordFormHeaderCopy').remove();
						var forgotPassContainer = $('#KSTL-Registration-wrapper-ForgotPasswordLink');
						var link = forgotPassContainer.find('a');
						forgotPassContainer.addClass('forgot-password');
						link.addClass('link');
						$("#KSTL-Registration-wrapper-ContinueButton button span").text("INICIA SESIÓN");
						$("#KSTL-Registration-wrapper-SendEmailButton button span").text("ENVIAR");
						$("#KSTL-Registration-wrapper-ContinueButton1 button span").text("CONTINUAR");

						var forgotPassInput = document.querySelector('#KSTL-Registration-YourEmailAddress');
						if (forgotPassInput !== null) {
							$("#KSTL-Registration-wrapper-YourEmailAddress label").remove();
							$("#KSTL-Registration-wrapper-YourEmailAddress input").attr("placeholder", "EMAIL");
						}
					}

					// convert labels into placeholders
					var labels = document.querySelectorAll('label');
					for (var i = 0; i < labels.length; i += 1) {
						var label = labels[i];
						var input = label.parentElement.parentElement.querySelector('input');
						if (input == null) {
							continue;
						}
						if (input.type === 'checkbox') continue;
						var labelText = label.innerText;

						labelText = labelText.replace('*', '').toUpperCase();
						if (labelText.indexOf('DIRECCIÓN') > -1) {
							labelText = labelText.replace('DIRECCIÓN DE ', '');
						}
						input.removeAttribute('ns-popover');
						input.removeAttribute('ns-popover-placement');
						input.removeAttribute('ns-popover-theme');
						input.removeAttribute('ns-popover-timeout');
						input.setAttribute('placeholder', labelText);
						input.setAttribute('aria-label', labelText);
						input.className += ' form-control';
						label.parentElement.removeChild(label);
					}

					// checkbox
					var checkbox = $('#KSTL-Registration-PrivacyPolicy');
					if (checkbox.length != 0) {
						checkbox.addClass('form-checkbox');

						const checkboxLabel = checkbox.parent().find('label');
						if (checkboxLabel.length != 0) {
							checkboxLabel.addClass('form-label');
						}
					}

					// errors
					var errorsContainer = document.querySelectorAll('.error-field, .error-form, .success-message');
					for (let i = 0; i < errorsContainer.length; i += 1) {
						var errorEl = errorsContainer[i];
						errorEl.className += ' form-error';
						errorEl.parentElement.appendChild(errorEl);
					}

					// block more than 10 characters on phone input
					$('#KSTL-Registration-PhoneNumber2').on('keyup', function () {
						if (this.value.length > 10) {
							this.value = this.value.substr(0, 10);
						}
					});
					// agregar titulo a iframe
					var iframe = $('iframe');
					iframe.attr('title', iframe.attr('src'));

                    $('#loaderFull').addClass("d-none");
					$('.loader-img').addClass("d-none");
					$('#regViewContent').removeClass("noClick");
					$('#chat').removeClass("noClick");
                });

				var eventFormSubmit = regModule.bind('form:submit', function (a, b, c, d) {

					$(".epsilonlogin button, .epsilonregistro button ").addClass("d-none");
					$(".epsilonlogin button, .epsilonregistro button").parent();
					setTimeout(function () {
						$(".epsilonlogin button, .epsilonregistro button").removeClass("d-none");
						$("#login_spinner").remove();
                        $scope.loading = false;
					}, 2800);
				});
			}

			(function () {
				var src, kreg, s;

				if (configObj.environment === 'stage') {
					src = 'https://stage.registrationv1.kglobalservices.com/scripts/registration.min.js';
				} else {
					src = 'https://www.registrationv1.kglobalservices.com/scripts/registration.min.js';
				}

				kreg = document.createElement('script');
				kreg.type = 'text/javascript';
				kreg.onload = kreg.onreadystatechange = function () {
					if (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete') {
						kreg.onload = kreg.onreadystatechange = null;
						initReg();
                    }
				};
				kreg.src = src;
				s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(kreg, s);
			})();
		}


		function transformarFecha(date) {
			var d = new Date(date),
				month = '' + (d.getMonth() + 1),
				day = '' + d.getDate(),
				year = d.getFullYear();
			if (month.length < 2)
				month = '0' + month;
			if (day.length < 2)
				day = '0' + day;
			return [year, month, day].join('-');
		}


		function checar() {
			$('.error-field').each(function () {
				var div = $(this).html();
				if (div.includes("div")) {
					// console.log('aqui ya hay un error');
				} else {
					// console.log('no hay error');
				}
			});

		}
		//EPSILON


	};
})();

