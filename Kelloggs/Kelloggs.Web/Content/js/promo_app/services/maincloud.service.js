﻿(function () {
    'use strict';

    angular
        .module('promoApp')
        .service('cloud', cloud);

    cloud.$inject = ['$http'];

    function cloud($http) {
        var service = {
            testget: testget,
        };
         
        var baseURL = '/api/'; 
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
        return service;

        function testget() {
            return $http.get(baseURL + "productos")
                .then(testgetCompleted)
                .catch(testgetFailed);

            function testgetCompleted(response) {
                return response.data;
            }

            function testgetFailed(xhr) {
                console.log(xhr);
            }
        }
    }
})();
