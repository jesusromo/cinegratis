﻿(function () {
    'use strict';

    angular
        .module('pringlesApp')
        .service('cloud', cloud);

    cloud.$inject = ['$http'];

    function cloud($http) {
        var service = {
            getProducts: getProducts,
            getSubProducts: getSubProducts,
            saveProduct: saveProduct,
            editProduct: editProduct,
            editSubProduct: editSubProduct,
            saveSubProduct: saveSubProduct,
            deleteSubProduct: deleteSubProduct,
            deleteProduct: deleteProduct,
            getPremios: getPremios,
            deletePremio: deletePremio,
            savePremio: savePremio,
            getGanadores: getGanadores,
            geUsuarios: geUsuarios,
            getReporte2: getReporte2,
            getReporte3: getReporte3,
            getConfigs: getConfigs,
            editConfig: editConfig,
            getLatas: getLatas,
            testVision: testVision,
            sendToWinners: sendToWinners,
            getdlc: getdlc,
            validarLata: validarLata,
            RechazarLata: RechazarLata,
            downloadReportC: downloadReportC,
            downloadReportB: downloadReportB,
            downloadReportUser: downloadReportUser
        };
         
        var baseURL = '/api/'; 
        var config = { headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' } };
        return service;

        function downloadReportB(FechaInicial, FechaFinal) {
            return $http(
                    {
                        url: baseURL + "dwreportb",
                        method: 'POST',
						params: { "FechaInicial": FechaInicial, "FechaFinal": FechaFinal },
                        headers: {
                            'Content-type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        },
                        responseType: 'arraybuffer'
                    })
                .then(downloadReportBCompleted)
                .catch(downloadReportBFailed);

            function downloadReportBCompleted(response) {
                return response.data;
            }

            function downloadReportBFailed(xhr) {
                console.log(xhr);
            }
        }


        function downloadReportUser(FechaInicial, FechaFinal) {
	        return $http(
			        {
						url: baseURL + "dwreportUser",
				        method: 'POST',
				        params: { "FechaInicial": FechaInicial, "FechaFinal": FechaFinal },
				        headers: {
					        'Content-type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
				        },
				        responseType: 'arraybuffer'
			        })
		        .then(downloadReportBCompleted)
		        .catch(downloadReportBFailed);

	        function downloadReportBCompleted(response) {
		        return response.data;
	        }

	        function downloadReportBFailed(xhr) {
		        console.log(xhr);
	        }
        }


        function downloadReportC(FechaInicial, FechaFinal) {

            return $http(
                {
                    url: baseURL + "dwreportc",
                    method: 'POST',
                    params: { "FechaInicial":FechaInicial, "FechaFinal":FechaFinal},
                    headers: {
                        'Content-type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' },
                    responseType: 'arraybuffer'
                })
                .then(downloadReportCCompleted)
                .catch(downloadReportCFailed);

            function downloadReportCCompleted(response) {
                return response.data;
            }

            function downloadReportCFailed(xhr) {
                console.log(xhr);
            }
        }

        function getProducts() {
            return $http.get(baseURL + "productos")
                .then(getProductsCompleted)
                .catch(getProductsFailed);

            function getProductsCompleted(response) {
                return response.data;
            }

            function getProductsFailed(xhr) {
                console.log(xhr);
            }
        }

        function getdlc() {
            return $http.get(baseURL + "dlcs")
                .then(getgetdlcCompleted)
                .catch(getgetdlcFailed);

            function getgetdlcCompleted(response) {
                return response.data;
            }

            function getgetdlcFailed(xhr) {
                console.log(xhr);
            }
        }

        function testVision() {
            return $http.get("/WAVision/TestVisionAsync")
                .then(testVisionCompleted)
                .catch(testVisionFailed);

            function testVisionCompleted(response) {
                return response.data;
            }

            function testVisionFailed(xhr) {
                console.log(xhr);
            }
        }

        function sendToWinners() {
            return $http.get("/WAVision/WinnersWhatsapp")
                .then(sendToWinnersCompleted)
                .catch(sendToWinnersFailed);

            function sendToWinnersCompleted(response) {
                return response.data;
            }

            function sendToWinnersFailed(xhr) {
                console.log(xhr);
            }
        }

        function getSubProducts() {
            return $http.get(baseURL + "subproducts")
                .then(getSubProductsCompleted)
                .catch(getSubProductsFailed);

            function getSubProductsCompleted(response) {
                return response.data;
            }

            function getSubProductsFailed(xhr) {
                console.log(xhr);
            }
        }

        function saveProduct(product) {
            return $http.post(baseURL + "saveProduct", product)
                .then(saveProductCompleted)
                .catch(saveProductFailed);

            function saveProductCompleted(response) {
                return response.data;
            }

            function saveProductFailed(xhr) {
                console.log(xhr);
            }
        }

        function editProduct(product) {
            return $http.post(baseURL + "editProduct", product)
                .then(editProductCompleted)
                .catch(editProductFailed);

            function editProductCompleted(response) {
                return response.data;
            }

            function editProductFailed(xhr) {
                console.log(xhr);
            }
        }

        function editSubProduct(subProduct) {
            return $http.post(baseURL + "editSubProduct",
                    {
                        Id:subProduct.id,
                        Name  : subProduct.SubProduct
                    })
                .then(editSubProductCompleted)
                .catch(editSubProductFailed);

            function editSubProductCompleted(response) {
                return response.data;
            }

            function editSubProductFailed(xhr) {
                console.log(xhr);
            }
        }

        function saveSubProduct(product) {
            return $http.post(baseURL + "saveSubProduct", {
                ProductId: product.Product.id,
                SubProduct: product.SubProduct })
                .then(saveSubProductCompleted)
                .catch(saveSubProductFailed);

            function saveSubProductCompleted(response) {
                return response.data;
            }

            function saveSubProductFailed(xhr) {
                console.log(xhr);
            }
        }

        function deleteSubProduct(id) {
            return $http.post(baseURL + "deleteSubProduct", { SubProductId : id})
                .then(deleteSubProductCompleted)
                .catch(deleteSubProductFailed);

            function deleteSubProductCompleted(response) {
                return response.data;
            }

            function deleteSubProductFailed(xhr) {
                console.log(xhr);
            }
        }

        function validarLata(id) {
            return $http.post(baseURL + "validarLata", { Id: id, Correcto: true })
                .then(validarLataCompleted)
                .catch(validarLataFailed);

            function validarLataCompleted(response) {
                return response.data;
            }

            function validarLataFailed(xhr) {
                console.log(xhr);
            }
        }



        function RechazarLata(id) {
            return $http.post(baseURL + "validarLata", { Id: id, Correcto:false })
                .then(validarLataCompleted)
                .catch(validarLataFailed);

            function validarLataCompleted(response) {
                return response.data;
            }

            function validarLataFailed(xhr) {
                console.log(xhr);
            }
        }




        function deleteProduct(id) {
            return $http.post(baseURL + "deleteProduct", { ProductId: id })
                .then(deleteProductCompleted)
                .catch(deleteProductFailed);

            function deleteProductCompleted(response) {
                return response.data;
            }

            function deleteProductFailed(xhr) {
                console.log(xhr);
            }
        }

        //premios
        function getPremios() {
            return $http.get(baseURL + "premios")
                .then(getPremiosCompleted)
                .catch(getPremiosFailed);

            function getPremiosCompleted(response) {
                return response.data;
            }

            function getPremiosFailed(xhr) {
                console.log(xhr);
            }
        }

        function deletePremio(id) {
            return $http.post(baseURL + "deletePremio", { Id: id })
                .then(deletePremioCompleted)
                .catch(deletePremioFailed);

            function deletePremioCompleted(response) {
                return response.data;
            }

            function deletePremioFailed(xhr) {
                console.log(xhr);
            }
        }

        function savePremio(premio) {
            return $http.post(baseURL + "savePremio", premio)
                .then(savePremioCompleted)
                .catch(savePremioFailed);

            function savePremioCompleted(response) {
                return response.data;
            }

            function savePremioFailed(xhr) {
                console.log(xhr);
            }
        }

        function editConfig(conf) {
            return $http.post(baseURL + "editConfig", conf)
                .then(editConfigCompleted)
                .catch(editConfigFailed);

            function editConfigCompleted(response) {
                return response.data;
            }

            function editConfigFailed(xhr) {
                console.log(xhr);
            }
        }

        //Ganadores
        function getGanadores() {
            return $http.get(baseURL + "ganadores")
                .then(getGanadoresCompleted)
                .catch(getGanadoresFailed);

            function getGanadoresCompleted(response) {
                return response.data;
            }

            function getGanadoresFailed(xhr) {
                console.log(xhr);
            }
        }


        function geUsuarios() {
            return $http.get(baseURL + "usuariosRegistrados")
                .then(geUsuariosCompleted)
                .catch(geUsuariosFailed);

            function geUsuariosCompleted(response) {
                return response.data;
            }

            function geUsuariosFailed(xhr) {
                console.log(xhr);
            }
        }

        function getLatas(newPage, pageSize) {
            return $http.post(baseURL + "latas", { "Paged": newPage,"ItemsPaged": pageSize})
                .then(getLatasCompleted)
                .catch(getLatasFailed);

            function getLatasCompleted(response) {
                return response.data;
            }

            function getLatasFailed(xhr) {
                console.log(xhr);
            }
        }

        function getReporte2(newPage, pageSize) {
            return $http.post(baseURL + "reportenewtwo", { "Paged": newPage, "ItemsPaged": pageSize })
                .then(getReporte2Completed)
                .catch(getReporte2Failed);

            function getReporte2Completed(response) {
                return response.data;
            }

            function getReporte2Failed(xhr) {
                console.log(xhr);
            }
        }

        function getReporte3(newPage, pageSize) {
            return $http.post(baseURL + "reporte3", { "Paged": newPage, "ItemsPaged": pageSize })
                .then(getReporte3Completed)
                .catch(getReporte3Failed);

            function getReporte3Completed(response) {
                return response.data;
            }

            function getReporte3Failed(xhr) {
                console.log(xhr);
            }
        }

        //ConfiguracionesÇ
        function getConfigs() {
            return $http.get(baseURL + "configuraciones")
                .then(getConfigsCompleted)
                .catch(getConfigsFailed);

            function getConfigsCompleted(response) {
                return response.data;
            }

            function getConfigsFailed(xhr) {
                console.log(xhr);
            }
        }

        
    }
})();
