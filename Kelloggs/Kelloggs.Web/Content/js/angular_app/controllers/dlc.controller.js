(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('dlc', dlcController);

    dlcController.$inject = ['$scope', 'i18nService', 'cloud','fileUpload'];

    function dlcController($scope, i18nService, cloud, fileUpload) {
        var sp = this;
        sp.lang = 'es';
        sp.ganador = {};
        sp.getDlc = getDlc;
        sp.addDlcs = addDlcs;
        sp.displayAdd = false;


        //Grids
        sp.dlcGrid = {
            columnDefs: [
                { name: 'TIPO', field: 'Nombre' },
                { name: 'CANTIDAD', field: 'Cantidad' }
           ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getDlc();


        function getDlc() {
            return cloud.getdlc().then(function (data) {
                sp.dlcGrid.data = data;

            });
        }

        function addDlcs() {
            var file = $scope.csvFile;
            var uploadUrl = "/api/fileUploaddlc";
            fileUpload.uploadFileToUrl(file, uploadUrl);
            
        }
        
    };
})();

