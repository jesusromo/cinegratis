(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('premio', premioController);

    premioController.$inject = ['$scope', 'i18nService', 'cloud'];

    function premioController($scope, i18nService, cloud) {
        var sp = this;
        sp.lang = 'es';
        sp.premio = {};
        sp.savePremio = savePremio;
        sp.getPremios = getPremios;
        $scope.deletePremio = deletePremio;
        sp.displayAdd = false;


        //Grids
        sp.premiosGrid = {
            columnDefs: [
                { name: 'NOMBRE ARCHIVO', field: 'NombreArchivo' },
                { name: 'TIPO', field: 'Nombre' },
                { name: 'CANTIDAD', field: 'Cantidad' },
                { name: 'CANJEADO', field: 'Canjeado' },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getPremios();


        function getPremios() {
            return cloud.getPremios().then(function (data) {
                sp.premiosGrid.data = data;

            });
        }

        function savePremio() {
            return cloud.savePremio(sp.premio).then(function (data) {
                console.log(data);
                if (data.Success) {
                    sp.getPremios();
                    sp.displayAdd = false;
                }
            });
        }

        function deletePremio(id) {
            var r = confirm("Al eleminar este premio se elminaran todas su dependencias, �Deseas continuar?");
            if (r) {
                return cloud.deletePremio(id).then(function (data) {
                    console.log(data);
                    if (data.Success) {
                        sp.getPremios();
                        sp.displayAdd = false;
                    }
                });
            } 
        }
        
    };
})();

