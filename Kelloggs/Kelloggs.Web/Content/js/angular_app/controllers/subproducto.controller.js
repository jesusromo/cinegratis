(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('subproducto', subproductoController);

    subproductoController.$inject = ['$scope', 'i18nService', 'cloud'];

    function subproductoController($scope, i18nService, cloud) {
        var sp = this;
        sp.lang = 'es';
        sp.products = [];
        sp.subProducts = [];
        sp.product = {};
        sp.subProduct = {};
        sp.saveProduct = saveProduct;
        sp.getSubProducts = getSubProducts;
        $scope.deleteSubProduct = deleteSubProduct;
        sp.openAddProductModal = openAddProductModal;
        $scope.editModal = editModal;
        sp.edit = edit;
        sp.displayAdd = false;
        sp.add = true;


        //Grids
        sp.subProductsGrid = {
            columnDefs: [
                { name: 'PRODUCTO', field: 'product' },
                { name: 'SUB PRODUCTO', field: 'subProducto' },
                { name: 'ACCION', field: '', cellTemplate: "<div><i ng-click='grid.appScope.deleteSubProduct(row.entity.id)' class='fa fa-trash-o fa-2' aria-hidden='true' style='color:red; cursor:pointer; font-size: 24px;'></i>&nbsp;<i ng-click='grid.appScope.editModal(row.entity)' class='fa fa-edit fa-2' aria-hidden='true' style='color:#e6e600; cursor:pointer; font-size: 24px;'></i></div>" },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getProducts();
        sp.getSubProducts();

        function openAddProductModal() {
            sp.displayAdd = true;
            sp.add = true;
        }

        function edit() {
            return cloud.editSubProduct(sp.subProduct).then(function (data) {
                if (data.Success) {
                    sp.getSubProducts();
                    sp.displayAdd = false;
                    sp.add = true;
                }
            });
        }

        function editModal(subProduct) {
            sp.subProduct = subProduct;
            sp.subProduct.SubProduct = subProduct.subProducto;
            sp.displayAdd = true;
            sp.add = false;
        }


        function getProducts() {
            return cloud.getProducts().then(function (data) {
                sp.products = data;
            });
        }

        function getSubProducts() {
            return cloud.getSubProducts().then(function (data) {
                sp.subProductsGrid.data = data;
            });
        }

        function saveProduct() {
            return cloud.saveSubProduct(sp.subProduct).then(function (data) {
                console.log(data);
                if (data.Success) {
                    sp.getSubProducts();
                    sp.displayAdd = false;
                    sp.add = true;

                }
            });
        }

        function deleteSubProduct(id) {
            var r = confirm("Al eleminar este Subproducto, se eliminaran todo lo relacionado a el (latas registradas), � Deseas continuar?");
            if (r) {
                return cloud.deleteSubProduct(id).then(function (data) {
                    console.log(data);
                    if (data.Success) {
                        sp.getSubProducts();
                        sp.displayAdd = false;
                    }
                });
            } 
        }
    };
})();

