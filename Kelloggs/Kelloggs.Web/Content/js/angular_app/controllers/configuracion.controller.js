(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('configuracion', configuracionController);

    configuracionController.$inject = ['$scope', 'i18nService', 'cloud','fileUpload'];

    function configuracionController($scope, i18nService, cloud, fileUpload) {
        var sp = this;
        sp.lang = 'es';
        sp.conf = {};
        sp.getConfigs = getConfigs;
        $scope.deleteConfig = deleteConfig;
        $scope.editConfig = editConfig;
        sp.displayAdd = false;
        sp.testVision = testVision;
        sp.sendToWinners = sendToWinners;

        //Grids
        sp.confGrid = {
            columnDefs: [
                { name: 'Tipo Config', field: 'name' },
                { name: 'Status', field: 'status' },
                { name: 'ACCION', field: '', cellTemplate: "<div><i ng-click='grid.appScope.deleteConfig(row.entity.id)' class='fa fa-trash-o fa-2' aria-hidden='true' style='color:red; cursor:pointer;font-size: 24px;'></i>&nbsp;<i ng-click='grid.appScope.editConfig(row.entity.id)' class='fa fa-exchange fa-2' aria-hidden='true' style='color:#e6e600; cursor:pointer; font-size: 24px;'></i></div>" },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Configuraciones
        sp.displayAdd = false;
        getConfigs();


        function getConfigs() {
            return cloud.getConfigs().then(function (data) {
                sp.confGrid.data = data;

            });
        }


        function deleteConfig(id) {
            return cloud.deleteConfig(id).then(function (data) {
                console.log(data);
                if (data.Success) {
                    sp.getConfigs();
                    sp.displayAdd = false;
                }
            });
        }

        function editConfig(id) {
            return cloud.editConfig({ Id: id }).then(function (data) {
                console.log(data);
                if (data.Success) {
                    alert("Actualizacion exitosa");
                    sp.getConfigs();
                } else {
                    alert("Error al cambiar el estado de la configuracion");
                }
            });
        }


        function testVision(id) {
            return cloud.testVision().then(function (data) {
                alert(data);
            });
        }

        function sendToWinners() {
            return cloud.sendToWinners().then(function (data) {
                alert(data);
            });
        }

        
    };
})();

