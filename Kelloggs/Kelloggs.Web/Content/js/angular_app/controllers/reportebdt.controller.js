(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('reporte', reporteController);

    reporteController.$inject = ['$scope', 'i18nService', 'cloud', 'fileUpload', 'exportUiGridService'];

    function reporteController($scope, i18nService, cloud, fileUpload, exportUiGridService) {
        var sp = this;
        sp.lang = 'es';
        sp.lata = {};
        sp.getLatas = getLatas;
        sp.displayAdd = false;
        sp.dowloading = false;
        sp.downloadReport = downloadReport;
        $scope.openImg = openImg;
        var hoy = new Date();
        sp.FechaInicial = formatDate(hoy);
        sp.fechaFinal = formatDate(new Date(hoy.setMonth(hoy.getMonth() + 1)));

        var fInicial = '';
        var fFinal = '';

        var DateInicial = '';
        var DateFinal = '';

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 5,
            sort: null
        };

        $scope.$watch('fechaInicial', function (value) {
	        try {
		        DateInicial = new Date(value);
		        fInicial = value;
		        console.log(fInicial);
	        } catch (e) { }
	        if (!DateInicial) {
		        fInicial = '';
		        $scope.error = "This is not a valid date";
	        } else {
		        $scope.error = false;
	        }
        });




        $scope.$watch('fechaFinal', function (value) {
	        try {

		        DateFinal = new Date(value);

		        if (DateInicial > DateFinal) {

		        }


		        fFinal = value;
		        console.log(fFinal);
	        } catch (e) { }

	        if (!DateFinal) {
		        fFinal = '';
		        $scope.error = "This is not a valid date";
	        } else {
		        $scope.error = false;
	        }
        });

        //Grids
        sp.reporte = {
            paginationPageSizes: [5, 10, 20],
            paginationPageSize: 5,
            useExternalPagination: true,
            enableColumnResizing: true,
            columnDefs: [
                { name: 'Id', field: 'Id' },
                { name: 'Usuario', field: 'Usuario' },
                { name: 'Email', field: 'CorreoElectronico' },
                { name: 'Fecha Registro', field: 'FechaRegistro' },
                { name: 'Lote A', field: 'LoteA' },
                { name: 'Lote B', field: 'LoteB' },
                { name: 'Cargado', field: 'Cargado' },
	            { name: 'Membrana', field: '', cellTemplate: "<div ng-if='row.entity.ProductImageMembrana'><i ng-click='grid.appScope.openImg(row.entity.ProductImageMembrana)' class='fa fa-eye fa-2' aria-hidden='true' style='color:blue; cursor:pointer;font-size: 24px;'></i></div>" },
               ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte Lotes.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getLatas(newPage, pageSize);
                });
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getLatas(paginationOptions.pageNumber, paginationOptions.pageSize);
        //Grids
        
        function getLatas(newPage, pageSize) {
	        sp.dowloading = true;
            return cloud.getReporte2(newPage, pageSize).then(function (data) {
	            sp.dowloading = false;
		        console.log(data);
		        data.Items.forEach(function (item) {
                    item.FechaRegistro = new Date(parseInt(item.FechaRegistro.substring(6)));
		        });

                sp.reporte.data = data.Items;
                sp.reporte.totalItems = data.Total;
	        });
        }

        function openImg(im) {
	        console.log(im);
	        var image = new Image();
	        image.src = "data:image/jpg;base64," + im;

	        var w = window.open("");
	        w.document.write(image.outerHTML);
        }

        function formatDate(date) {
	        var d = new Date(date),
		        month = '' + (d.getMonth() + 1),
		        day = '' + d.getDate(),
		        year = d.getFullYear();

	        if (month.length < 2)
		        month = '0' + month;
	        if (day.length < 2)
		        day = '0' + day;

	        return [year, month, day].join('-');
        }

        function downloadReport() {
            sp.dowloading = true;

            return cloud.downloadReportB(fInicial, fFinal).then(function (data) {
                sp.dowloading = false;

                // TODO when WS success
                var file = new Blob([data], {
                    type: 'application/csv'
                });
                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Reporte.xlsx';
                document.body.appendChild(a); //create the link "a"
                a.click(); //click the link "a"
                document.body.removeChild(a);
            });
        }



    };
})();
