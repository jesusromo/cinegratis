﻿(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('reportec', reportecController);

    reportecController.$inject = ['$scope', 'i18nService', 'cloud', 'fileUpload', 'exportUiGridService'];

    function reportecController($scope, i18nService, cloud, fileUpload, exportUiGridService) {
        var sp = this;
        sp.lang = 'es';
        sp.geUsuarios = geUsuarios;
        sp.dowloading = false;
        sp.downloadReport = downloadReport;

        var hoy = new Date();
        sp.FechaInicial = formatDate(hoy);
        sp.fechaFinal = formatDate(new Date(hoy.setMonth(hoy.getMonth() + 1)));

        var fInicial = '';
        var fFinal = '';

        var DateInicial = '';
        var DateFinal = '';

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 20,
            sort: null
        };



        $scope.$watch('fechaInicial', function (value) {
            try {
                DateInicial = new Date(value);
                fInicial = value;
                console.log(fInicial);
            } catch (e) { }
            if (!DateInicial) {
                fInicial = '';
                $scope.error = "This is not a valid date";
            } else {
                $scope.error = false;
            }
        });




    $scope.$watch('fechaFinal', function (value) {
        try {
            
            DateFinal = new Date(value);

            if (DateInicial > DateFinal)
            {

            }
            

            fFinal = value;
            console.log(fFinal);
        } catch (e) { }

        if (!DateFinal) {
            fFinal = '';
            $scope.error = "This is not a valid date";
        } else {
            $scope.error = false;
        }
    });




        //Grids
        sp.usersGrid = {
            paginationPageSizes: [20, 50, 100],
            paginationPageSize: 20,
            useExternalPagination: true,
            enableColumnResizing: true,
            columnDefs: [
                { name: 'IdUsuario', field: 'IdUsuario' },
                { name: 'Correo Electonico', field: 'CorreoElectonico' },
                { name: 'Nombre Usuario', field: 'NombreUsuario' },
                { name: 'Apellido', field: 'Apellido' },
                { name: 'Fecha Nacimiento', field: 'FechaNacimiento' },
                { name: 'Fecha Canje Completa', field: 'FechaCanjeCompleta' },
                { name: 'Cedula', field: 'Cedula' },
                { name: 'Direccion', field: 'Direccion' },
                { name: 'Ciudad', field: 'Ciudad' },
                { name: 'Telefono', field: 'Telefono' },

                { name: 'LoteA', field: 'LoteA' },
                { name: 'LoteB', field: 'LoteB' },
                { name: 'Registro Completado', field: 'RegistroCompletado' },
                { name: 'Nombre Premio', field: 'NombrePremio' },
                { name: 'Premio Entregado', field: 'PremioEntregado' }
               
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte concentrado.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    geUsuarios(newPage, pageSize);
                });
            }
        };
        //Grids

        geUsuarios(paginationOptions.pageNumber, paginationOptions.pageSize);

        function geUsuarios(newPage, pageSize) {
            return cloud.getReporte3(newPage, pageSize).then(function (data) {
                sp.usersGrid.data = data.Items;
                sp.usersGrid.totalItems = data.Total;
            });
        }



        function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();

            if (month.length < 2)
                month = '0' + month;
            if (day.length < 2)
                day = '0' + day;

            return [year, month, day].join('-');
        }



        function downloadReport() {
            sp.dowloading = true;
            return cloud.downloadReportC(fInicial, fFinal).then(function (data) {
                sp.dowloading = false;
                // TODO when WS success
                var file = new Blob([data], {
                    type: 'application/csv'
                });
                //trick to download store a file having its URL
                var fileURL = URL.createObjectURL(file);
                var a = document.createElement('a');
                a.href = fileURL;
                a.target = '_blank';
                a.download = 'Reporte_concentrado.xlsx';
                document.body.appendChild(a); //create the link "a"
                a.click(); //click the link "a"
                document.body.removeChild(a);
            });
        }
    };
})();