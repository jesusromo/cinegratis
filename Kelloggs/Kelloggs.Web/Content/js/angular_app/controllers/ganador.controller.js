(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('ganador', ganadorController);

    ganadorController.$inject = ['$scope', 'i18nService', 'cloud','fileUpload'];

    function ganadorController($scope, i18nService, cloud, fileUpload) {
        var sp = this;
        sp.lang = 'es';
        sp.ganador = {};
        sp.getGanadores = getGanadores;
        $scope.deleteGanador = deleteGanador;
        sp.addMultipleWinners = addMultipleWinners;
        sp.displayAdd = false;
        sp.getLayout = getLayout;


        //Grids
        sp.ganadoresGrid = {
            columnDefs: [
                { name: 'Id Usuario', field: 'Id' },
                { name: 'NOMBRE', field: 'NombreGanador' },
                { name: 'APELLIDO', field: 'ApellidoGanador' },
                { name: 'PREMIO', field: 'NombrePremio' },
                { name: 'CODIGO', field: 'Codigo' },
                { name: 'CORTE', field: 'Corte' },
                { name: 'ENCABEZADO', field: 'Encabezado' },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte Ganadores.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getGanadores();


        function getGanadores() {
            return cloud.getGanadores().then(function (data) {
                sp.ganadoresGrid.data = data;

            });
        }


        function deleteGanador(id) {
            return cloud.deleteGanador(id).then(function (data) {
                console.log(data);
                if (data.Success) {
                    sp.getGanadores();
                    sp.displayAdd = false;
                }
            });
        }

        function addMultipleWinners() {
            var file = $scope.csvFile;
            console.log('file is ');
            console.dir(file);
            var uploadUrl = "/api/fileUpload";
            fileUpload.uploadFileToUrl(file, uploadUrl);
        }

        function getLayout() {
            var link = document.createElement("a");
            link.download = "ganadores.csv";
            link.href = "https://pringlesgamermx.azurewebsites.net/content/pdf/ganadores.csv";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
        
    };
})();

