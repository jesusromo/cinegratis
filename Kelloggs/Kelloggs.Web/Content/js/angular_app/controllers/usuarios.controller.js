(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('usuarios', usuariosController);

    usuariosController.$inject = ['$scope', 'i18nService', 'cloud', 'fileUpload', 'exportUiGridService'];

    function usuariosController($scope, i18nService, cloud, fileUpload, exportUiGridService) {
        var sp = this;
        sp.lang = 'es';
        sp.geUsuarios = geUsuarios;

        sp.downloadReport = downloadReport;
        var hoy = new Date();
        sp.FechaInicial = formatDate(hoy);
        sp.fechaFinal = formatDate(new Date(hoy.setMonth(hoy.getMonth() + 1)));

        var fInicial = '';
        var fFinal = '';

        var DateInicial = '';
        var DateFinal = '';
        $scope.$watch('fechaInicial', function (value) {
	        try {
		        DateInicial = new Date(value);
		        fInicial = value;
		        console.log(fInicial);
	        } catch (e) { }
	        if (!DateInicial) {
		        fInicial = '';
		        $scope.error = "This is not a valid date";
	        } else {
		        $scope.error = false;
	        }
        });




        $scope.$watch('fechaFinal', function (value) {
	        try {

		        DateFinal = new Date(value);

		        if (DateInicial > DateFinal) {

		        }


		        fFinal = value;
		        console.log(fFinal);
	        } catch (e) { }

	        if (!DateFinal) {
		        fFinal = '';
		        $scope.error = "This is not a valid date";
	        } else {
		        $scope.error = false;
	        }
        });

        //Grids
        sp.usersGrid = {
            columnDefs: [
                { name: 'Usuario Id', field: 'Id' },
                { name: 'NOMBRE', field: 'Nombre' },
                { name: 'E-MAIL', field: 'Email' },
                { name: 'TELEFONO', field: 'Telefono' },
                { name: 'CIUDAD', field: 'Ciudad' },
                { name: 'DIRECCION', field: 'Direccion' },
                { name: 'NACIMIENTO', field: 'FechaNacimiento' },
                { name: 'REGISTRADO', field: 'FechaRegistro' }
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte Usuarios.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }, gridMenuCustomItems: [{
                title: 'Descargar reporte EXCEL',
                action: function ($event) {
                    exportUiGridService.exportToExcel('Sheet 1', $scope.gridApi, 'all', 'all');
                },
                order: 110
            }
            ]
        };
        //Grids

        geUsuarios();
        function geUsuarios() {
            return cloud.geUsuarios().then(function (data) {
                sp.usersGrid.data = data;

            });
        }

        function formatDate(date) {
	        var d = new Date(date),
		        month = '' + (d.getMonth() + 1),
		        day = '' + d.getDate(),
		        year = d.getFullYear();

	        if (month.length < 2)
		        month = '0' + month;
	        if (day.length < 2)
		        day = '0' + day;

	        return [year, month, day].join('-');
        }

        function downloadReport() {
	        sp.dowloading = true;

            return cloud.downloadReportUser(fInicial, fFinal).then(function (data) {
		        sp.dowloading = false;

		        // TODO when WS success
		        var file = new Blob([data], {
			        type: 'application/csv'
		        });
		        //trick to download store a file having its URL
		        var fileURL = URL.createObjectURL(file);
		        var a = document.createElement('a');
		        a.href = fileURL;
		        a.target = '_blank';
		        a.download = 'ReporteUsuarios.xlsx';
		        document.body.appendChild(a); //create the link "a"
		        a.click(); //click the link "a"
		        document.body.removeChild(a);
	        });
        }

    };
})();