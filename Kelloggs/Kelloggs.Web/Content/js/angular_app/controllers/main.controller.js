(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('main', mainController);

    mainController.$inject = ['$scope', 'i18nService', 'cloud'];

    function mainController($scope, i18nService, cloud) {
        var mn = this;
        $scope.page = "0";
       $scope.loggedName = "";
        mn.lang = 'es';

        //Grids
      mn.productsGrid = {
          columnDefs: [
              { name: 'PRODUCTO', field: 'name' },
              { name: 'ACCION', field: '', cellTemplate: "<div><i ng-click='grid.appScope.deleteProduct(row.entity.id)' class='fa fa-trash-o fa-2' aria-hidden='true' style='color:red; cursor:pointer; font-size: 24px;'></i>&nbsp;<i ng-click='grid.appScope.editModal(row.entity)' class='fa fa-edit fa-2' aria-hidden='true' style='color:#e6e600; cursor:pointer; font-size: 24px;'></i></div>" },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
           // exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            }
        };
        //Grids

        //Productos
        mn.displayAdd = false;
        mn.add = true;
        mn.product = {};
        getProducts();
        mn.openAddProductModal = openAddProductModal;
        mn.saveProduct = saveProduct;
        mn.edit = edit;
        $scope.editModal = editModal;
        $scope.deleteProduct = deleteProduct;


        function openAddProductModal() {
            mn.displayAdd = true;
            mn.add = true;
        }

        function getProducts() {
            return cloud.getProducts().then(function (data) {
                console.log(data);
                mn.productsGrid.data = data;
            });
        }

        function saveProduct() {
            return cloud.saveProduct(mn.product).then(function (data) {
                console.log(data);
                if (data.Success) {
                    getProducts();
                    mn.displayAdd = false;
                    mn.add = true;
                }
            });
        }

        function edit() {
            return cloud.editProduct(mn.product).then(function (data) {
                if (data.Success) {
                    getProducts();
                    mn.displayAdd = false;
                    mn.add = true;
                }
            });
        }

        function editModal(producto) {
            mn.product = producto;
            mn.displayAdd = true;
            mn.add = false;
        }

        function deleteProduct(id) {
            var r = confirm("Al eleminar este producto, se eliminaran todos sus subproductos, �Deseas continuar?");
            if (r) {
                return cloud.deleteProduct(id).then(function (data) {
                    if (data.Success) {
                        getProducts();
                    }
                });
            } 
           
        }


       
    };
})();

