(function () {
    'use strict';

    angular.module('pringlesApp')
        .controller('validarLata', validarLataController);

    validarLataController.$inject = ['$scope', 'i18nService', 'cloud','fileUpload'];

    function validarLataController($scope, i18nService, cloud, fileUpload) {
        var sp = this;
        sp.lang = 'es';
        sp.lata = {};
        sp.getLatas = getLatas;
        sp.displayAdd = false;
        $scope.openImg = openImg;
        $scope.validar = validar;
        $scope.rechazar = rechazar;

        var paginationOptions = {
            pageNumber: 1,
            pageSize: 5,
            sort: null
        };

        //Grids
        sp.validarLataGrid = {
            paginationPageSizes: [5,10,20],
            paginationPageSize: 5,
            useExternalPagination: true,
            enableColumnResizing: true,
            columnDefs: [
                { name: 'LOTE', field: 'Lote' },
                { name: 'FECHA', field: 'FechaLote' },
                { name: 'CODIGO', field: 'CodigoBarras' },
                {
                    name: 'STATUS', field: 'StatusStr',
                    cellClass: function (grid, row, col, rowIndex, colIndex) {
                        var val = grid.getCellValue(row, col);
                        if (val === 'Valido') {
                            return 'greentext';
                        }
                        if (val === 'Rechazado') {
                            return 'redtext';
                        }
                    }},
                { name: 'PUNTOS', field: 'Puntaje' },
                { name: 'LOTELINK', field: '', cellTemplate: "<div ng-if='row.entity.HasImgLote'><i ng-click='grid.appScope.openImg(row.entity.ImgLoteFechaStr)' class='fa fa-eye fa-2' aria-hidden='true' style='color:blue; cursor:pointer;font-size: 24px;'></i></div>" },
                { name: 'BARRALINK', field: '', cellTemplate: "<div ng-if='row.entity.HasImgBarras'><i ng-click='grid.appScope.openImg(row.entity.ImgBarrasStr)' class='fa fa-eye fa-2' aria-hidden='true' style='color:blue; cursor:pointer;font-size: 24px;'></i></div>" },
                { name: 'Registro', field: 'FechaRegistro',cellFilter: 'date:\'MM/dd/yyyy HH:MM:ss\'' },
                { name: 'ACCION', field: '', cellTemplate: "<div  ng-if='!row.entity.Status'><i ng-click='grid.appScope.validar(row.entity)' class='fa fa-check-circle fa-2' aria-hidden='true' style='color:green; cursor:pointer;font-size: 24px;'></i><i ng-click='grid.appScope.rechazar(row.entity)' class='fa fa-times-circle fa-2' aria-hidden='true' style='color:red; cursor:pointer;font-size: 24px;margin-left: 15px;'></i></div>" },
            ],
            enableGridMenu: true,
            enableSelectAll: true,
            exporterCsvFilename: 'Reporte.csv',
            exporterMenuPdf: false, // ADD THIS
            exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location")),
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                    paginationOptions.pageNumber = newPage;
                    paginationOptions.pageSize = pageSize;
                    getLatas(newPage,pageSize);
                });
            }
        };
        //Grids

        //Productos
        sp.displayAdd = false;
        getLatas(paginationOptions.pageNumber, paginationOptions.pageSize);

        function getLatas(newPage, pageSize) {
            return cloud.getLatas(newPage, pageSize).then(function (data) {
                console.log(data);
                data.Items.forEach(function(item) {
                    item.FechaRegistro = new Date(parseInt(item.FechaRegistro.substring(6)));
                });

                sp.validarLataGrid.data = data.Items;
                sp.validarLataGrid.totalItems = data.Total;
            });
        }

        function openImg(im) {
            console.log(im);
            var image = new Image();
            image.src = "data:image/jpg;base64," + im;

            var w = window.open("");
            w.document.write(image.outerHTML);
        }

        function validar(item) {
                return cloud.validarLata(item.Id).then(function (data) {
                    console.log(data);
                    if (data.Success) {
                        item.StatusStr = 'Valido';
                        item.Status = true;
                    }
                });
        }
        
        function rechazar(item) {
                return cloud.RechazarLata(item.Id).then(function (data) {
                    console.log(data);
                    if (data.Success) {
                        item.StatusStr = 'Rechazado';
                        item.Status = true;
                    }
                });
        };

    };


})();

