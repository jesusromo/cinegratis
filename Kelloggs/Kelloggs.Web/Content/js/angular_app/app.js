﻿(function () {
    'use strict';
    angular.module('pringlesApp', ['ui.grid', 'ui.grid.selection', 'ui.grid.exporter', 'ui.grid.pagination', 'ui.grid.resizeColumns', '720kb.datepicker']);

    angular.module('pringlesApp').directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);

    angular.module('pringlesApp').service('fileUpload', ['$http', function ($http) {
        this.uploadFileToUrl = function (file, uploadUrl) {
            var fd = new FormData();
            fd.append('file', file);

            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined }
            })
                .success(function () {
                    alert("Se cargo correctamente, Refresca la pagina");
                })
                .error(function () {
                    alert("Recarga la pagina e intenta con lotes de 5mil lineas");
                });
        }
    }]);

})();