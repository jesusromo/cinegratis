﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kelloggs.Startup))]
namespace Kelloggs
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
