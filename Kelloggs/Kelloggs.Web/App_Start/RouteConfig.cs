﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Kelloggs
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            routes.MapRoute(
                name: "uploadticketcompra",
                url: "api/uploadticketcompra",
                defaults: new { controller = "Admin", action = "UploadTicketCompra" }
            );


            routes.MapRoute(
                name: "DeleteImages",
                url: "WAVision/WinnersWhatsapp",
                defaults: new { controller = "WAVision", action = "WinnersWhatsapp" }
            );




            routes.MapRoute(
                name: "Prueba",
                url: "Prueba/GetImage/{id}",
                defaults: new { controller = "Prueba", action = "GetImage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "Authenticate",
               url: "Authenticate/registro",
              defaults: new { controller = "Authenticate", action = "registro" }
              );

            routes.MapRoute(
                name: "AuthenticateCustom",
                url: "Authenticate/customlogin",
                defaults: new { controller = "Authenticate", action = "CustomLogin" }
            );

            routes.MapRoute(
             name: "Account",
             url: "Account/LogOff",
            defaults: new { controller = "Account", action = "LogOff" }
            );

            routes.MapRoute(
              name: "Login",
              url: "Account/Login",
             defaults: new { controller = "Account", action = "Login" }
             );


            routes.MapRoute(
                name: "HomeRegistro",
                url: "Registro",
                defaults: new { controller = "Promo", action = "Registro" }
            );

            #region ADMIN ROUTES

            routes.MapRoute(
                name: "loginAdmin",
                url: "admin/login",
                defaults: new { controller = "Admin", action = "Login" }
            );



            routes.MapRoute(
                name: "ReportCExcel",
                url: "api/dwreportc",
                defaults: new { controller = "Admin", action = "ReportCExcel" }
            );

            routes.MapRoute(
                name: "ReportBExcel",
                url: "api/dwreportb",
                defaults: new { controller = "Admin", action = "ReportBExcel" }
            );
            routes.MapRoute(
	            name: "ReportUser",
	            url: "api/dwreportUser",
	            defaults: new { controller = "Admin", action = "ReportUser" }
            );

            routes.MapRoute(
                name: "AdminProducto",
                url: "Panel",
                defaults: new { controller = "Admin", action = "Index" }
            );

            routes.MapRoute(
                name: "AdminSubProducto",
                url: "SNihJEvSO",
                defaults: new { controller = "Admin", action = "SubProducto" }
            );
            routes.MapRoute(
                name: "AdminPremio",
                url: "AZbKCunahKX",
                defaults: new { controller = "Admin", action = "Premio" }
            );
            routes.MapRoute(
                name: "AdminGanador",
                url: "bPIpfaho",
                defaults: new { controller = "Admin", action = "Ganador" }
            );

            routes.MapRoute(
                name: "AdminDLC",
                url: "bjkdscho",
                defaults: new { controller = "Admin", action = "DLC" }
            );

            routes.MapRoute(
                name: "Configuracion",
                url: "bPIkfahohioj",
                defaults: new { controller = "Admin", action = "Configuracion" }
            );

            routes.MapRoute(
                name: "ValidarLatas",
                url: "fdgookgflsdfer",
                defaults: new { controller = "Admin", action = "ValidarLatas" }
            );

            routes.MapRoute(
                name: "AdminReporte",
                url: "jhWuGpuMjBRT",
                defaults: new { controller = "Admin", action = "Reporte" }
            );

            routes.MapRoute(
                name: "AdminReporteB",
                url: "jhWuGBRTODMgf",
                defaults: new { controller = "Admin", action = "ReporteB" }
            );

            routes.MapRoute(
                name: "AdminReporteC",
                url: "jhWuGpyu",
                defaults: new { controller = "Admin", action = "ReporteC" }
            );


            routes.MapRoute(
                name: "productos",
                url: "api/productos",
                defaults: new { controller = "Admin", action = "Productos" }
            );


            routes.MapRoute(
                name: "latas",
                url: "api/latas",
                defaults: new { controller = "Admin", action = "GetLatas" }
            );


            routes.MapRoute(
                name: "configuraciones",
                url: "api/configuraciones",
                defaults: new { controller = "Admin", action = "Configuraciones" }
            );


            routes.MapRoute(
                name: "deleteProduct",
                url: "api/deleteProduct",
                defaults: new { controller = "Admin", action = "DeleteProduct" }
            );
            routes.MapRoute(
                name: "saveProduct",
                url: "api/saveProduct",
                defaults: new { controller = "Admin", action = "SaveProduct" }
            );

            routes.MapRoute(
                name: "editProduct",
                url: "api/editProduct",
                defaults: new { controller = "Admin", action = "EditProduct" }
            );

            routes.MapRoute(
                name: "editSubProduct",
                url: "api/editSubProduct",
                defaults: new { controller = "Admin", action = "EditSubProduct" }
            );

            routes.MapRoute(
                name: "saveSubProduct",
                url: "api/saveSubProduct",
                defaults: new { controller = "Admin", action = "SaveSubProduct" }
            );


            routes.MapRoute(
                name: "deleteSubProduct",
                url: "api/deleteSubProduct",
                defaults: new { controller = "Admin", action = "DeletSubProduct" }
            );
            routes.MapRoute(
                name: "TiposProductos",
                url: "api/tiposproductos",
                defaults: new { controller = "Admin", action = "TiposProductos" }
            );
            routes.MapRoute(
                name: "SubProductos",
                url: "api/subproducts",
                defaults: new { controller = "Admin", action = "SubProductos" }
            );

            routes.MapRoute(
                name: "SubProductosById",
                url: "api/subproductsbyid",
                defaults: new { controller = "Admin", action = "SubProductosById" }
            );

            routes.MapRoute(
                name: "SubProductosUnicos",
                url: "api/subproductosunicos",
                defaults: new { controller = "Admin", action = "SubProductosUnicos" }
            );

            routes.MapRoute(
                name: "Ganadores",
                url: "api/ganadores",
                defaults: new { controller = "Admin", action = "Ganadores" }
            );

            routes.MapRoute(
                name: "UsuariosRegistrados",
                url: "api/usuariosRegistrados",
                defaults: new { controller = "Admin", action = "UsuariosRegistrados" }
            );

            routes.MapRoute(
                name: "Reporte2",
                url: "api/reportenewtwo",
                defaults: new { controller = "Admin", action = "Reporte2" }
            );

            routes.MapRoute(
                name: "Reporte3",
                url: "api/reporte3",
                defaults: new { controller = "Admin", action = "Reporte3" }
            );

            routes.MapRoute(
                name: "UploadFile",
                url: "api/fileUpload",
                defaults: new { controller = "Admin", action = "UploadFile" }
            );

            routes.MapRoute(
                name: "UploadFileDLC",
                url: "api/fileUploaddlc",
                defaults: new { controller = "Admin", action = "UploadFileDLC" }
            );

            routes.MapRoute(
                name: "UploadMembrana",
                url: "api/uploadMembrana",
                defaults: new { controller = "Promo", action = "UploadMembrana" }
            );


            #region Premios
            routes.MapRoute(
                name: "premios",
                url: "api/premios",
                defaults: new { controller = "Admin", action = "Premios" }
            );
            routes.MapRoute(
                name: "deletePremio",
                url: "api/deletePremio",
                defaults: new { controller = "Admin", action = "DeletePremio" }
            );
            routes.MapRoute(
                name: "savePremio",
                url: "api/savePremio",
                defaults: new { controller = "Admin", action = "SavePremio" }
            );


            #endregion

            routes.MapRoute(
                name: "editConfig",
                url: "api/editConfig",
                defaults: new { controller = "Admin", action = "EditConfig" }
            );


            #region Seed
            routes.MapRoute(
                name: "SeedPremiosPromocion",
                url: "api/SeedPremiosPromocion",
                defaults: new { controller = "Admin", action = "SeedPremiosPromocion" }
            );
            routes.MapRoute(
                name: "SeedProductosPromocion",
                url: "api/SeedProductosPromocion",
                defaults: new { controller = "Admin", action = "SeedProductosPromocion" }
            );

            routes.MapRoute(
                name: "SeedDLC",
                url: "api/SeedDLC",
                defaults: new { controller = "Admin", action = "SeedDLC" }
            );

            #endregion

            routes.MapRoute(
                name: "getDlcs",
                url: "api/dlcs",
                defaults: new { controller = "Admin", action = "Dlcs" }
            );

            routes.MapRoute(
                name: "validarLata",
                url: "api/validarLata",
                defaults: new { controller = "Admin", action = "ValidarLata" }
            );

            routes.MapRoute(
                name: "savecode",
                url: "api/savecode",
                defaults: new { controller = "Admin", action = "SaveCode" }
            );

            routes.MapRoute(
               name: "getCitys",
               url: "api/getCitys",
               defaults: new { controller = "Admin", action = "GetCitys" }
           );
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Promo", action = "Home", id = UrlParameter.Optional }
            );
        }
    }
}
