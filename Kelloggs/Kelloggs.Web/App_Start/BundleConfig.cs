﻿using System.Web.Optimization;

namespace Kelloggs
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region CSS
            bundles.Add(new StyleBundle("~/bundles/promoCss").Include(
                "~/Content/css/main.css",
                "~/Content/css/bootstrap.min.css"));
            #endregion


            #region JavaScript
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/jquery.lettering.js",
                "~/Scripts/ccquery.js",
                "~/Scripts/Mask.min.js"
            ));

            bundles.Add(new Bundle("~/bundles/login").Include(
                "~/Scripts/datepicker.min.js",
                "~/Scripts/datepicker-locales-es.js",
                "~/Scripts/select2.min.js",
                "~/Scripts/select2-es.js"
            ));

            bundles.Add(new Bundle("~/bundles/registro").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/ccquery.js",
                "~/Scripts/datepicker.min.js",
                "~/Scripts/datepicker-locales-es.js",
                "~/Scripts/select2.min.js",
                "~/Scripts/select2-es.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/epsilon").Include(
                "~/Scripts/epsilon.js"));



            bundles.Add(new ScriptBundle("~/bundles/control").Include(
                
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquerySolo").Include(
                "~/Scripts/jquery-{version}.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js"));


            bundles.Add(new ScriptBundle("~/bundles/promo").Include(
                "~/Content/js/promo_app/app.js",
                "~/Content/js/promo_app/controllers/main.controller.js",
                "~/Content/js/promo_app/services/maincloud.service.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/home").Include(
                "~/Scripts/home.js"));
            #endregion


        }
    }
}
