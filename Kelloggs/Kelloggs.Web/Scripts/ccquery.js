
$(window).on('load', function () {


    $.ajax({
        url: "https://services.kelloggs.com/bin/ccquerybuilder.json?propertyName=sitepath&propertyValue=https://www.promokelloggs.com",
        dataType: "jsonp",
        jsonpCallback: "getAlert"
    });

});


function getAlert(jsonObject) {
    console.log(jsonObject);
    console.log("Proccesing...");
    var json = JSON.parse(jsonObject);
    var i;
    var msg = "";
    for (i = 0; i < json.hits.length; i++) {
        // check this condition                             
        var ser = json.hits[i].detMsgURL;
        // check the URL whether its a day based application or non-day based application       
        var alertImgURL = json.hits[i].alertImgURL;
        var destURL = json.hits[i].detMsgURL;
        var shortMsg = json.hits[i].shortMsg;
        msg += '<div class="errorwrap"><p><img alt="Message Icon" class="error_img" style="vertical-align:middle;margin-top:0px" src="' + alertImgURL
            + '" height="20" width="20" alt="Alert Image" />&nbsp;<a class="error_msg" href=\''
            + destURL + '\' target=\'_blank\'> ' + shortMsg + '</a></div>';
    }
    //if (document.getElementById('error_wp').innerHTML != null)
    //    document.getElementById('error_wp').innerHTML = msg;
}