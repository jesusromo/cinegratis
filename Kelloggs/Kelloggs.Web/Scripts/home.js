$(document).ready(function () {
    $("#BtnLoginRegistroCodigos").click(function () {
        var lote = $('#lote').val();
        var fechaVencimiento = $('#dateven').val();
        var tipoProducto = $("#tipoProducto").val();
        var productId = $("#marca").val();
        var subproductId = $("#gramaje").val();
        window.localStorage.setItem('lote', lote); 
        window.localStorage.setItem('fechaVencimiento', fechaVencimiento); 
        window.localStorage.setItem('tipoProducto', tipoProducto); 
        window.localStorage.setItem('productId', productId); 
        window.localStorage.setItem('subproductId', subproductId); 
        window.location = '/login';
    });
    $(".containerBtnGanadores").click(function () {
        /*$(this).parent().toggleClass("active");
        $(this).toggleClass("active");
        $(this).next("div").slideToggle(200);
        $('.panel-ganadores').not($(this).next("div")).slideUp(200).prev("div").removeClass("active");*/
        //Hago esto porque el display none hace que no funcione el slider
        $(this).parent().toggleClass("active");
        $(this).toggleClass("active");
        $(this).next("div").toggleClass("panelOculto");
        $('.panel-ganadores').not($(this).next("div")).addClass("panelOculto").prev("div").removeClass("active");
    });
    $(".FlechaGanadores").on('keypress', function (e) {
        if (e.which === 13 || e.which === 32) {
            $(this).parent().toggleClass("active");
            $(this).toggleClass("active");
            $(this).next("div").toggleClass("panelOculto");
            $('.panel-ganadores').not($(this).next("div")).addClass("panelOculto").prev("div").removeClass("active");
        }
    });
    $(".main-n__icon").on('keypress', function (e) {
        if (e.which === 13) {
            $(this).trigger('click');
        }
    });
    $(".toggle-password").on('keypress', function (e) {
        if (e.which === 13 || e.which === 32) {
            $(this).trigger('click');
        }
    });
    $(".footer-social_item").on('keypress', function (e) {
        if (e.which === 13 || e.which === 32) {
            var href = $(this).find('a:first').attr('href');
            if (href !== null)
                window.open(href, "_blank");
        }
    });
    $(document).on('keypress',
        ".toggle-password",
    function (e) {
        if (e.which === 13 || e.which === 32) {
            $(this).trigger('click');
        }
    });
    $.get("/api/tiposproductos", function (data) {
        data.forEach(function (valor, indice, array) {
            var o = new Option(valor, valor);
            o.className = "option-select-producto";
            $("#tipoProducto").append(o);
        });
        if (window.localStorage.getItem("lote") !== null) {
            if (window.localStorage.getItem("tipoProducto") !== null) {
                var tipoProducto = window.localStorage.getItem("tipoProducto");
                $('#tipoProducto').val(tipoProducto);
                window.localStorage.removeItem('tipoProducto');
            }
        }
    });
    $.get("/api/productos",
        function (data) {
        data.forEach(function (element) {
            var o = new Option(element.name, element.id);
            o.className = "option-select-producto";
            $("#marca").append(o);
        });
        if (window.localStorage.getItem("lote") !== null) {
            if ($("#lote").length > 0) {
                var lote = window.localStorage.getItem("lote");
                var fechaVencimiento = window.localStorage.getItem("fechaVencimiento");
                var tipoProducto = window.localStorage.getItem("tipoProducto");
                var productId = window.localStorage.getItem("productId");
                var subproductId = window.localStorage.getItem("subproductId");
                if (window.localStorage.getItem("lote") !== null) {
                    $('#lote').val(lote);
                    window.localStorage.removeItem('lote');
                }
                if (window.localStorage.getItem("fechaVencimiento") !== null) {
                    $('#dateven').val(fechaVencimiento);
                    window.localStorage.removeItem('fechaVencimiento');
                }
                if (window.localStorage.getItem("tipoProducto") !== null) {
                    $('#tipoProducto').val(tipoProducto);
                    window.localStorage.removeItem('tipoProducto');
                }
                if (window.localStorage.getItem("productId") !== null) {
                    $('#marca').val(productId);
                    window.localStorage.removeItem('productId');
                    var req = new XMLHttpRequest();
                    req.open("GET", "/api/subproductsbyid?id=" + productId, false);
                    req.send(null);
                    var subprods = JSON.parse(req.responseText);
                    subprods.forEach(function (element) {
                        var o = new Option(element.subProducto, element.id);
                        o.className = "option-select-producto";
                        $("#gramaje").append(o);
                    });
                    if (window.localStorage.getItem("subproductId") !== null) {
                        $('#gramaje').val(subproductId);
                        window.localStorage.removeItem('subproductId');
                    }
                }
            }
            }
    });
    //Put our input DOM element into a jQuery Object
    var $jq = jQuery('input[name="dateven"]');
    if ($('#dateven').length) {
        $('#dateven').inputmask("datetime", {
            showMaskOnHover: false
        });
        $('#lote').inputmask("datetime", {
            showMaskOnHover: false
        });
    }
    /*$jq.bind('keyup',
        'keydown',
        function (e) {
            if (e.which !== 8) {
                var numChars = $jq.val().length;
                if (numChars === 2) {
                    var thisVal = $jq.val();
                    thisVal += '/';
                    $jq.val(thisVal);
                }
            }
        });*/
    //Put our input DOM element into a jQuery Object
    /*var $jqlot = jQuery('input[name="lote"]');
    $jqlot.bind('keyup',
        'keydown',
        function (e) {
            var t = e.target, i = t.name, s = t.value;
            if ("lote" === i) {
                e.which ? e.which : e.keyCode;
                (1 !== s.length && 10 !== s.length ||
                    8 === e.keyCode && 46 != e.keyCode ||
                    !isNaN(parseInt(s, 10))) &&
                    (isNaN(parseInt(s, 10)) || 8 !== s.length || 8 === e.keyCode && 46 != e.keyCode) ||
                    (this.value = s + " ");
            }
        });*/
        function isValidDate(dateString) {
            var parts = dateString.split("/");
            var month = parseInt(parts[0], 10);
            var year = parseInt(parts[2], 10);
            // Check the ranges of month and year
            if (year < 2020 || year > 2025)
                return false;

            return true;
        };


        function isValidDateTime(dateTimeString) {
            var fechaHr = dateTimeString.split(" ");
            var fecha = fechaHr[0];
            var parts = fecha.split("/");
            var year = parseInt(parts[2], 10);
            // Check the ranges of month and year
            if (year < 2020 || year > 2025)
                return false;

            return true;
        };

    /*function fixDateTime(dateString) {
        var fechaHr = dateString.split(" ");
        var fecha = fechaHr[0];
        var hora = fechaHr[1];
        var parts = fecha.split("/");
        var month = parseInt(parts[0], 10);
        var year = parseInt(parts[1], 10);
        var newDate = "01" + "/" + month + "/" + year + " " + hora;
        return newDate;
    };*/

    $("#marca").on('change',
        function () {
            $('#gramaje')
                .find('option')
                .remove()
                .end();
            $("#gramaje").append('<option class="option-select-producto" value="" disabled="" selected="">GRAMAJE</option>');
            var req = new XMLHttpRequest();
            req.open("GET", "/api/subproductsbyid?id=" + this.value, false);
            req.send(null);
            var subprods = JSON.parse(req.responseText);
            subprods.forEach(function (element) {
                var o = new Option(element.subProducto, element.id);
                o.className = "option-select-producto";
                $("#gramaje").append(o);
            });
        });

    $(document).on('click',
        ".toggle-password",
        function () {
            $(this).toggleClass("glyphicon-eye-close glyphicon-eye-open");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });

    $("#enviar").click(function () {
        $('#loteErr').text('');
        $('#datevenError').text('');
        $('#tipoProductoErr').text('');
        $('#marcaErr').text('');
        $('#gramajeErr').text('');
        if ($('#lote').val() == "") {
            $('#loteErr').text('Por favor llene este campo');
            return;
        }
        if (!isValidDateTime($('#lote').val())) {
            $('#loteErr').text('Fecha incorrecta');
            return;
        }
        if ($('#dateven').val() == "") {
            $('#datevenError').text('Por favor llene este campo');
            return;
        }
        if (!isValidDate($('#dateven').val())) {
            $('#datevenError').text('Fecha incorrecta');
            return;
        }
        if ($('#tipoProducto').val() == null) {
            $('#tipoProductoErr').text('Por favor llene este campo');
            return;
        }
        if ($('#marca').val() == null) {
            $('#marcaErr').text('Por favor llene este campo');
            return;
        }
        if ($('#gramaje').val() == null) {
            $('#gramajeErr').text('Por favor llene este campo');
            return;
        }
        const obj = {
            Lote: $('#lote').val(),
            FechaVencimiento: $('#dateven').val(),
            ProductId: $("#marca").val(),
            TipoProductId: $("#tipoProducto").val(),
            SubproductId: $("#gramaje").val()
        };
        var x = new XMLHttpRequest(); // new HttpRequest instance 
        var theUrl = "/api/savecode";
        x.open("POST", theUrl, false);
        x.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        x.send(JSON.stringify(obj));
        var response = JSON.parse(x.responseText);
        if (response.Success) {
            //fbq('track', 'CompleteRegistration');
            if (response.Code != "0") {
                console.log("Entro");
                $("#modalFolio").modal('show');
                $("#modalFolio #Folio").html(response.Code);
            }
            else {
                $("#success").modal('show');
            }
            $('#lote').val('');
            $('#dateven').val('');
            $('#marca').val('');
            $('#tipoProducto').val('');
            $('#gramaje').val('');
        } else {
            console.log(response);
            if (response.Code == 2)
                $("#errorFinalizo").modal('show');
            else
                $("#error").modal('show');
        }
    });

    /*$("#loginButton").click(function () {
        $('#loginErrorEmail').hide();
        $('#loginErrorNick').hide();
        var email = $('#email').val();
        var pass = $('#pass').val();

        $.ajax('/Authenticate/customlogin', {
            type: 'POST',
            data: {
                __RequestVerificationToken: "token",
                Email: email, Password: pass
            },
            success: function (dat) {
                if (dat.Success) {
                    location.href = "/";
                } else {
                    if (dat.EmailError && dat.NickNameError) {
                        $('#loginErrorEmail').show();
                        $('#loginErrorNick').show();
                    } else {
                        if (dat.NickNameError) {
                            $('#loginErrorNick').show();
                        }
                        if (dat.EmailError) {
                            $('#loginErrorEmail').show();
                        }
                    }
                           
                }
            }
        }).done(function (data) {
                    
        });
    });*/
    

    $("#BtnAyuda1").click(function () {
        $("#question").modal('show');
    });

    $("#BtnAyuda2").click(function () {
        $("#question").modal('show');
    });

    var callSuccess = function () {
        $("#lot").val("");
        $("#date").val("");
        $("#product").val("");
        $("#gramaje").val("");
        $("#success").modal('show');
    }

    var callError = function () {
        $("#error").modal('show');
    }

    var callWelcome = function () {
        $("#welcome").modal('show');
    }

    window.callWelcome = callWelcome;

    $(".close-btn").click(function () {
        closeModal();
    });

    var closeModal = function () {
        var targetModal = $('.modal.in');
        $(targetModal).addClass('away');
        setTimeout(function () {
            $(targetModal).modal("toggle");
            $(targetModal).removeClass('away');
        },
            500);
    }




    $("#mantenimiento").modal('show');
});