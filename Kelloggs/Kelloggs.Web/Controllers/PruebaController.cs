﻿using System;
using System.Net;
using System.Web.Mvc;
using RestSharp;

namespace Kelloggs.Controllers
{
    public class PruebaController : Controller
    {
        [HttpPost]
        public ActionResult GetImage(ImageRequest r)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            try
            {
                var client = new RestClient(r.URL);
                var request = new RestRequest(Method.GET);


                string archivo = System.IO.Path.Combine(Server.MapPath("~/Content/Tickets/"), DateTime.Now.ToString("yyyyMMddhhmmss") + ".jpg");


                byte[] respuesta = client.DownloadData(request);
                System.IO.File.WriteAllBytes(archivo, respuesta);

                byte[] imageArray = System.IO.File.ReadAllBytes(archivo);
                string base64ImageRepresentation = Convert.ToBase64String(imageArray);




                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                serializer.MaxJsonLength = 500000000;

                var json = Json(base64ImageRepresentation, JsonRequestBehavior.AllowGet);
                json.MaxJsonLength = 500000000;
                return json;

            }
            catch (Exception ex)
            {
                return Json("error");
            }

        }
    }

    public class ImageRequest
    {
        public string URL { get; set; }
    }
}