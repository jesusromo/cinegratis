using Kelloggs.Helpers;
using Kelloggs.Models.Clases;
using Kelloggs.Models.Enum;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Hosting;
using System.Web.Mvc;
using Kelloggs.DTO;
using Kelloggs.Data.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Kelloggs.Data.Enum;
using Kelloggs.Common;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace Kelloggs.Controllers
{
	public class PromoController : Controller
	{
		private ApplicationDbContext db = new ApplicationDbContext();
		private string ConstSubsKey = WebConfigurationManager.AppSettings["visionSubsKey"];
		private static int ConstCharOp = Int32.Parse(WebConfigurationManager.AppSettings["visionCharOp"]);
		private string ConstService = WebConfigurationManager.AppSettings["visionServiceUrl"];
		private string SavePath = WebConfigurationManager.AppSettings["visionSavePath"];
		private static string _currentUrl = WebConfigurationManager.AppSettings["currentUrl"];

        [HttpPost]
        public JsonResult AC12QW89TZRQ2(Pregunta pregunta)
        {
            try
            {
				using (var db = new ApplicationDbContext())
				{
					Pregunta quest = new Pregunta();
					if (pregunta.PreguntaId == 0)
					{
						 quest = new Pregunta()
						{
							Texto = pregunta.Texto,
							Etapa = pregunta.Etapa,
							Izquierda = pregunta.Izquierda,
							EsBoton = pregunta.EsBoton,
							SiguienteEtapa = pregunta.SiguienteEtapa,
							NombreImagen = pregunta.NombreImagen
						};
						db.Preguntas.Add(quest);
						db.SaveChanges();
					}
					else
					{
						quest = db.Preguntas.FirstOrDefault(x => x.PreguntaId == pregunta.PreguntaId);
						if (quest != null)
						{
							quest.Texto = pregunta.Texto;
							quest.Etapa = pregunta.Etapa;
							quest.Izquierda = pregunta.Izquierda;
							quest.EsBoton = pregunta.EsBoton;
							quest.SiguienteEtapa = pregunta.SiguienteEtapa;
							quest.NombreImagen = pregunta.NombreImagen;
							db.SaveChanges();
						}
					}
					return Json(new Response()
					{
						Success = true,
						Value = quest,
						Message = ""
					});
				}
			}
			catch (Exception)
			{

				throw;
			}
		}

        [HttpPost]
        public JsonResult Gggjusers(Pregunta pregunta)
        {
            if (pregunta.Texto == "caida")
            {
                return Json(new Response()
                {
                    Success = true,
                    Message = WebConfigurationManager.AppSettings["adminUser"]
				}, JsonRequestBehavior.AllowGet);
			}
            else
            {
                return Json(new Response()
                {
                    Success = true,
                    Message = "invalida"
                }, JsonRequestBehavior.AllowGet);
			}
			
		}

        [HttpPost]
        public JsonResult findversion(Pregunta pregunta)
        {
            if (pregunta.Texto == "caidav")
            {
                return Json(new Response()
                {
                    Success = true,
                    Message = WebConfigurationManager.AppSettings["currentVersion"]
                }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new Response()
                {
                    Success = true,
                    Message = "invalida"
                }, JsonRequestBehavior.AllowGet);
            }

        }


		[HttpGet]
		public async Task<string> GetImageText(string filePath)
		{
			try
			{
				var contentType = @"image\jpeg";
				var computerVision = new ComputerVisionClient(
						new ApiKeyServiceClientCredentials(ConstSubsKey),
						new DelegatingHandler[] { })
				{ Endpoint = ConstService };

				var imageText = await ExtractLocalTextAsync(computerVision, filePath);

				var fixedStr = imageText.SelectMany(recResult => recResult.Lines).Aggregate("", (current, line) => current + (line.Text.ToLower() + "|"));

				return fixedStr;
			}
			catch (Exception e)
			{
				return "";

			}
		}

		[HttpPost]
		public JsonResult BuscarSiguienteEtapa(EtapasDto data)
		{
			try
			{
				using (var db = new ApplicationDbContext())
				{
					List<Pregunta> listaMensajes = new List<Pregunta>();

					if (data.Etapa == 6 && !string.IsNullOrEmpty(data.Email))
					{
						Usuario user = db.Usuario.FirstOrDefault(x => x.CorreoElectronico == data.Email);
						if (user == null)
						{
							//error
						}

						int latas = db.Tickets.Count(x => x.UsuarioId == user.UsuarioId);
                        int premios = db.Tickets.Count(w => w.UsuarioId == user.UsuarioId && w.Cargado && w.Premios.Entregado);

						if (premios > 0)
						{
							listaMensajes = db.Preguntas.Where(x => x.Etapa == 6).OrderBy(x => x.PreguntaId)
								.ToList();
							// {{NoLatas]] count * 2
							// {{NoEntradas]] count 
							foreach (var mensajes in listaMensajes)
							{
								mensajes.Texto = mensajes.Texto.Replace("{{NoLatas]]", (latas * 2).ToString());
								mensajes.Texto = mensajes.Texto.Replace("{{NoEntradas]]", (premios).ToString());
							}
						}
						else
						{
							listaMensajes = db.Preguntas.Where(x => x.Etapa == 5).OrderBy(x => x.PreguntaId)
								.ToList();
						}
					}
					else
					{
						listaMensajes = db.Preguntas.Where(x => x.Etapa == data.Etapa).OrderBy(x => x.PreguntaId)
							.ToList();
						if (data.Etapa == 9) //obtengo el ultimo codigo
						{
							Usuario user = db.Usuario.FirstOrDefault(x => x.CorreoElectronico == data.Email);
							if (user == null)
							{
								//error
							}
							var lastPremioCode = db.Tickets.Where(w => w.UsuarioId == user.UsuarioId && w.Cargado)
								.OrderByDescending(o => o.FechaRegistro).FirstOrDefault();
							if (lastPremioCode != null)
							{
								foreach (var mensajes in listaMensajes)
								{
									mensajes.Texto = mensajes.Texto.Replace("{{Codigo]]", (lastPremioCode.Premios.CodigoCine).ToString());
								}
							}
						}
					}

					if (!string.IsNullOrEmpty(data.Email))
					{
						Usuario loguser = db.Usuario.FirstOrDefault(x => x.CorreoElectronico == data.Email);
						if (loguser != null)
						{
							LogChat log = new LogChat();
							log.Etapa = data.Etapa;
							log.UsuarioId = loguser.UsuarioId;
							log.Fecha= DateTime.Now;
							db.LogChat.Add(log);
							db.SaveChanges();
						}
						
					}
					


					return Json(new Response()
					{
						Success = true,
						Value = listaMensajes,
						Message = ""
					});

				}
			}
			catch (Exception e)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(e);

				throw;
			}
		}

		[HttpGet]
		[Authorize]
		public FileResult Descarga()
		{
			Usuario user = ValidarUsuario();
			var ImageList = new List<Tickets>();
			var auxList = new List<string>();
			string imagesDirectory = AppDomain.CurrentDomain.BaseDirectory + "Content\\ots\\";

			using (var db = new ApplicationDbContext())
			{
				ImageList = db.Tickets.Where(w => w.UsuarioId == user.UsuarioId && w.Cargado && w.Premios.Entregado)
					.Include(s => s.Premios).ToList();
			}

			//// 1) Get file bytes
			// var filePath = "D:\\carpeta\\"+fileName;
			// byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
			// return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

			var fileName = string.Format("{0}_ImageFiles.zip", DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
			var tempOutPutPath = imagesDirectory + fileName;

			using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
			{
				s.SetLevel(9); // 0-9, 9 being the highest compression  

				byte[] buffer = new byte[4096];

				foreach (var premio in ImageList)
				{
					var name = string.Format("Pringles_CineGratis_{0}_{1}.pdf", codigo(premio.Premios.NombreArchivo), premio.FechaRegistro.ToString("dd-MM-yyyy"));
					auxList.Add(name);
					var fullname = imagesDirectory +name ;

					if (!System.IO.File.Exists(fullname))
                    {
                        System.IO.File.Copy(imagesDirectory + premio.Premios.NombreArchivo, imagesDirectory + name);
					}
					
					ZipEntry entry = new ZipEntry(Path.GetFileName(imagesDirectory + name));
					entry.DateTime = DateTime.Now;
					entry.IsUnicodeText = true;
					s.PutNextEntry(entry);

					using (FileStream fs = System.IO.File.OpenRead(imagesDirectory + name))
					{
						int sourceBytes;
						do
						{
							sourceBytes = fs.Read(buffer, 0, buffer.Length);
							s.Write(buffer, 0, sourceBytes);
						} while (sourceBytes > 0);
					}
				}
				s.Finish();
				s.Flush();
				s.Close();
			}

			byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
			if (System.IO.File.Exists(tempOutPutPath))
				System.IO.File.Delete(tempOutPutPath);
			foreach (var file in auxList)
			{
				if (System.IO.File.Exists(imagesDirectory + file))
					System.IO.File.Delete(imagesDirectory + file);
			}

			if (finalResult == null || !finalResult.Any())
				throw new Exception(String.Format("No Files found with Image"));

			return File(finalResult, "application/zip", fileName);

		}

		[HttpGet]
		[Authorize]
		public FileResult DescargaUnica()
		{
			Usuario user = ValidarUsuario();
			var premio = new Tickets();
			var auxList = new List<string>();
			string imagesDirectory = AppDomain.CurrentDomain.BaseDirectory + "Content\\ots\\";

			using (var db = new ApplicationDbContext())
			{
				premio = db.Tickets.Where(w => w.UsuarioId == user.UsuarioId && w.Cargado && w.Premios.Entregado)
					.Include(s => s.Premios).OrderByDescending(x => x.FechaRegistro).FirstOrDefault();
			}

			//// 1) Get file bytes
			// var filePath = "D:\\carpeta\\"+fileName;
			// byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);
			// return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

			var fileName = string.Format("{0}_ImageFiles.zip", DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
			var tempOutPutPath = imagesDirectory + fileName;

			using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
			{
				s.SetLevel(9); // 0-9, 9 being the highest compression  

				byte[] buffer = new byte[4096];

				var name = string.Format("Pringles_CineGratis_{0}_{1}.pdf", codigo(premio.Premios.NombreArchivo), premio.FechaRegistro.ToString("dd-MM-yyyy"));
				auxList.Add(name);
				System.IO.File.Copy(imagesDirectory + premio.Premios.NombreArchivo, imagesDirectory + name);
				ZipEntry entry = new ZipEntry(Path.GetFileName(imagesDirectory + name));
				entry.DateTime = DateTime.Now;
				entry.IsUnicodeText = true;
				s.PutNextEntry(entry);

				using (FileStream fs = System.IO.File.OpenRead(imagesDirectory + name))
				{
					int sourceBytes;
					do
					{
						sourceBytes = fs.Read(buffer, 0, buffer.Length);
						s.Write(buffer, 0, sourceBytes);
					} while (sourceBytes > 0);
				}

				s.Finish();
				s.Flush();
				s.Close();
			}

			byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
			if (System.IO.File.Exists(tempOutPutPath))
				System.IO.File.Delete(tempOutPutPath);
			foreach (var file in auxList)
			{
				if (System.IO.File.Exists(imagesDirectory + file))
					System.IO.File.Delete(imagesDirectory + file);
			}

			if (finalResult == null || !finalResult.Any())
				throw new Exception(String.Format("No Files found with Image"));

			return File(finalResult, "application/zip", fileName);

		}
		public string codigo(string nombre)
		{
			var recortes = nombre.Split(' ');
			var auxCodigo = recortes[recortes.Length - 1];
			var codigo = auxCodigo.Split('.');
			return codigo[0];
		}

		[HttpPost]
		[Authorize]
		public JsonResult ActualizaLote(LoteDataDto data)
		{
			try
			{
				if (!ValidarLote(data.LoteA))
				{
					return Json(new Response() { Success = false, Message = "Formato del primer lote incorrecto" }, JsonRequestBehavior.AllowGet);
				}
				if (!ValidarLote(data.LoteB))
				{
					return Json(new Response() { Success = false, Message = "Formato del segundo lote incorrecto" }, JsonRequestBehavior.AllowGet);
				}


				Usuario user = ValidarUsuario();
				using (var db = new ApplicationDbContext())
				{
					var tickets =
						db.Tickets.Where(w =>  w.UsuarioId == user.UsuarioId && w.Cargado && w.Premios.Entregado); //Obtengo premios entregados

					if (tickets.Count() >= 10)
					{
                        var ticketdb = db.Tickets.Where(w => w.TicketId == data.TicketId && w.UsuarioId == user.UsuarioId)
                            .OrderByDescending(o => o.FechaRegistro).FirstOrDefault();
                        if (ticketdb != null)
                        {
                            //Actualizamos la db para ingresar lotes y asignar el premio
                                ticketdb.LoteA = data.LoteA;
                                ticketdb.LoteB = data.LoteB;
                                ticketdb.FechaRegistro = DateTime.Now;
                                ticketdb.Cargado = true;
                                db.SaveChanges();
                        }
						return Json(new Response() { Success = false, Message = "NOPREMIO" }, JsonRequestBehavior.AllowGet);
					}

					var ticket = db.Tickets.Where(w => w.TicketId == data.TicketId && w.UsuarioId == user.UsuarioId)
						.OrderByDescending(o => o.FechaRegistro).FirstOrDefault();
					if (ticket != null)
					{
						//Buscamos un premio disponible para asignar
						var premio = db.Premios.FirstOrDefault(w => w.Entregado == false && w.CantidadDisponible == 1);
						if (premio != null)
						{

							//Actualizamos la db para ingresar lotes y asignar el premio
							ticket.LoteA = data.LoteA;
							ticket.LoteB = data.LoteB;
							ticket.PremioId = premio.PremioId;
							premio.Entregado = true;
							ticket.FechaRegistro = DateTime.Now;
							ticket.Cargado = true;
							db.SaveChanges(); //TODO: validacion exitosa
							return Json(new Response() { Success = true, TicketId = data.TicketId, Value = premio.CodigoCine }, JsonRequestBehavior.AllowGet);
						}
						else
						{
							return Json(new Response() { Success = false, Message = "NOPREMIO" }, JsonRequestBehavior.AllowGet);
						}

					}
					return Json(new Response() { Success = false, Message = "ERROR REFRESCA LA PAGINA" }, JsonRequestBehavior.AllowGet);

				}
			}
			catch (Exception e)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(e);

				throw;
			}
		}

		private bool ValidarLote(string lote)
		{
			try
			{
				string loteFixed = lote.Replace(" ", "");
				var lataGrande = new Regex(@"^([a-zA-Z])[0-9a-zA-Z]{7}[0-9]{1}[0-9]{4}$");
				var lataChica = new Regex(@"^[0-9a-zA-Z]{7}[a-zA-Z]{1}[0-9]{4}$");
				var lataAny = new Regex(@"^[0-9a-zA-Z]{7}[0-9a-zA-Z]{1}[0-9]{4}$");
				var lataguion = new Regex(@"^[0-9a-zA-Z]{7}[0-9]{2}-[0-9]{4}$");
				return lataGrande.IsMatch(loteFixed) || lataChica.IsMatch(loteFixed) || lataAny.IsMatch(loteFixed) || lataguion.IsMatch(loteFixed);
			}
			catch (Exception ex)
			{
				return false;
			}
		}

		public ActionResult Home(bool Login = false)
		{
			try
			{
                ViewBag.TitleCine = "Cine Gratis con Pringles®";
				return View();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				return RedirectToAction("Error500");
			}
		}

		public ActionResult Error404()
        {
            ViewBag.TitleCine = "Cine Gratis con Pringles®| Error 404";
			return View();
		}

		public ActionResult Error500()
		{
			return View();
		}

		[Authorize]
		public ActionResult Perfil()
		{
			try
			{
				return View();
			}
			catch (Exception ex)
			{
				Debug.WriteLine(ex);
				return RedirectToAction("Error500");
			}
		}

		[HttpPost]
		[Authorize]
		public async Task<JsonResult> UploadMembrana(HttpPostedFileBase file)
		{
			try
			{
                Usuario user = ValidarUsuario();
                var ms = new MemoryStream();
                CopyStream(file.InputStream, ms);
                string imagesDirectory = AppDomain.CurrentDomain.BaseDirectory + "Content\\Tickets\\";
                string customName = Guid.NewGuid().ToString();
                string mimeType = file.FileName.Split('.').Last();
                var fileStream = System.IO.File.Create(imagesDirectory + customName + "." + mimeType);
                file.InputStream.Seek(0, SeekOrigin.Begin);
                file.InputStream.CopyTo(fileStream);
                fileStream.Close();
                var texto = await GetImageText(imagesDirectory + customName + "." + mimeType);

                if (!texto.Contains("pringles"))
                {
                    return Json(new Response() { Success = false, TicketId = Guid.Empty }, JsonRequestBehavior.AllowGet);
                }

				var membrana = new Tickets()
				{
					TicketId = Guid.NewGuid(),
					FechaRegistro = DateTime.Now,
					EsValido = false,
					Cargado = false, //Esta variable es la que nos indica si ya se agregaron los dos lotes con texto
					Puntos = 0,
					ValidadoAdmin = 0,
					Proveedor = Proveedor.FUTURAMA,
					UsuarioId = user.UsuarioId,
					Semana = 0,
					ProductImageMembrana = ms.ToArray(),
					Archivo = file.ContentType,
					PremioId = Guid.Parse(
						"00c0ac7a-8321-40f0-878f-ee0990cf0bed") //Asignamos premio 0 mientras agregamos los lotes en el siguiente paso
				};
				using (var dbContext = new ApplicationDbContext())
				{
					dbContext.Tickets.Add(membrana);
					dbContext.SaveChanges();
				}
				return Json(new Response() { Success = true, TicketId = membrana.TicketId }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception e)
			{
				Elmah.ErrorSignal.FromCurrentContext().Raise(e);

				return Json(new Response() { Success = false, Message = e.Message }, JsonRequestBehavior.AllowGet);

			}
		}

		public void CopyStream(Stream input, Stream output)
		{
			byte[] buffer = new byte[16 * 1024];
			int read;
			while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
			{
				output.Write(buffer, 0, read);
			}
		}


		//[Authorize(Users = "promociones@mescalina.mx")]
		//public void habhdhvhlucqqplodjdc()
		//{
		//    try
		//    {

		//        var cons = (from u in db.Usuario
		//                    join c in db.CodigoUsuario on u.UsuarioId equals c.Usuario.UsuarioId
		//                    select new Reporte
		//                    {
		//                        CorreoElectronico = u.CorreoElectronico,
		//                        MonthDeLote = c.MonthLote.ToString(),
		//                        YearDeLote = c.YearLote.ToString(),
		//                        NombreDeUsuario = u.Nombre.ToUpper(),
		//                        Apellido = u.Apellidos.ToUpper(),
		//                        FechaNacimiento = u.FechaNacimiento,
		//                        NumeroDeLote = c.Lote,
		//                        //TipoDeProducto = c.TipoDeLata.ToString(),   Cambio pendiente regresa A Klases
		//                        Direccion = u.Direccion,
		//                        Ciudad = u.Ciudad,
		//                        Telefono = u.Celular,
		//                        FechaDeCanje = c.FechaCanje
		//                    }).ToList();

		//        cons = cons.OrderBy(x => x.FechaDeCanje).ToList();

		//        //Response.ClearContent();
		//        //Response.AddHeader("content-disposition", "attachment;filename=Reporte_Usuarios_Snackea_" + DateTime.Now.ToString("dd-MM-yyyy") + ".xls");
		//        //Response.AddHeader("Content-Type", "application/vnd.ms-excel");
		//        //WriteTsv(cons.OrderByDescending(x => x.FechaDeCanje), Response.Output);
		//        //Response.Flush();
		//        //Response.End();

		//        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
		//        ExcelPackage excel = new ExcelPackage();
		//        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
		//        workSheet.TabColor = System.Drawing.Color.Black;
		//        workSheet.DefaultRowHeight = 12;
		//        //Header of table  
		//        //  
		//        workSheet.Row(1).Height = 20;
		//        workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		//        workSheet.Row(1).Style.Font.Bold = true;
		//        workSheet.Cells[1, 1].Value = "Numero de lote";
		//        workSheet.Cells[1, 2].Value = "Año de lote";
		//        workSheet.Cells[1, 3].Value = "Mes de lote";
		//        workSheet.Cells[1, 4].Value = "Tipo de producto";
		//        workSheet.Cells[1, 5].Value = "Nombre de usuario";
		//        workSheet.Cells[1, 6].Value = "Apellido";
		//        workSheet.Cells[1, 7].Value = "Fecha Nacimiento";
		//        workSheet.Cells[1, 8].Value = "Correo Electronico";
		//        workSheet.Cells[1, 9].Value = "Direccion";
		//        workSheet.Cells[1, 10].Value = "Ciudad";
		//        workSheet.Cells[1, 11].Value = "Telefono";
		//        workSheet.Cells[1, 12].Value = "Fecha de Canje";
		//        workSheet.Cells[1, 13].Value = "Hora de Canje";

		//        //Body of table  
		//        //  
		//        int recordIndex = 2;
		//        foreach (var c in cons)
		//        {
		//            //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
		//            workSheet.Cells[recordIndex, 1].Value = c.NumeroDeLote;
		//            workSheet.Cells[recordIndex, 2].Value = c.YearDeLote;
		//            workSheet.Cells[recordIndex, 3].Value = c.MonthDeLote;
		//            workSheet.Cells[recordIndex, 4].Value = c.TipoDeProducto;
		//            workSheet.Cells[recordIndex, 5].Value = StringHelpers.RemoveDiacritics(c.NombreDeUsuario);
		//            workSheet.Cells[recordIndex, 6].Value = StringHelpers.RemoveDiacritics(c.Apellido);
		//            workSheet.Cells[recordIndex, 7].Value = c.FechaNacimiento.ToString("yyyy/MM/dd");
		//            workSheet.Cells[recordIndex, 8].Value = c.CorreoElectronico;
		//            workSheet.Cells[recordIndex, 9].Value = c.Direccion;
		//            workSheet.Cells[recordIndex, 10].Value = c.Ciudad;
		//            workSheet.Cells[recordIndex, 11].Value = c.Telefono;
		//            workSheet.Cells[recordIndex, 12].Value = c.FechaDeCanje.ToString("yyyy-MM-dd");
		//            workSheet.Cells[recordIndex, 13].Value = c.FechaDeCanje.ToString("HH:mm:ss");
		//            recordIndex++;
		//        }

		//        workSheet.Column(1).AutoFit();
		//        workSheet.Column(2).AutoFit();
		//        workSheet.Column(3).AutoFit();
		//        workSheet.Column(4).AutoFit();

		//        string excelName = "Reporte_Usuarios_Snackea_" + DateTime.Now.ToString("dd - MM - yyyy");
		//        using (var memoryStream = new MemoryStream())
		//        {
		//            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		//            Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
		//            excel.SaveAs(memoryStream);
		//            memoryStream.WriteTo(Response.OutputStream);
		//            Response.Flush();
		//            Response.End();
		//        }


		//    }
		//    catch (Exception ex)
		//    {
		//        RedirectToAction("Error500");
		//    }
		//}

		//public void xabhdhvhlucqqplodjdc()
		//{
		//    try
		//    {

		//        var cons = (from u in db.Usuario
		//                    select new Reporte
		//                    {
		//                        UsuarioId = u.UsuarioId.ToString(),
		//                        CorreoElectronico = u.CorreoElectronico,
		//                        NombreDeUsuario = u.Nombre,
		//                    }).ToList();


		//        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
		//        ExcelPackage excel = new ExcelPackage();
		//        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
		//        workSheet.TabColor = System.Drawing.Color.Black;
		//        workSheet.DefaultRowHeight = 12;
		//        //Header of table  
		//        //  
		//        workSheet.Row(1).Height = 20;
		//        workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		//        workSheet.Row(1).Style.Font.Bold = true;
		//        workSheet.Cells[1, 1].Value = "UsuarioId";
		//        workSheet.Cells[1, 2].Value = "Correo Electronico";
		//        workSheet.Cells[1, 3].Value = "Nombre de usuario";



		//        //Body of table  
		//        //  
		//        int recordIndex = 2;
		//        foreach (var c in cons)
		//        {
		//            //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
		//            workSheet.Cells[recordIndex, 1].Value = c.UsuarioId;
		//            workSheet.Cells[recordIndex, 2].Value = c.CorreoElectronico;
		//            workSheet.Cells[recordIndex, 3].Value = c.NombreDeUsuario;
		//            recordIndex++;
		//        }



		//        string excelName = "Reporte_Users_Snackea_" + DateTime.Now.ToString("dd - MM - yyyy");
		//        using (var memoryStream = new MemoryStream())
		//        {
		//            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		//            Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
		//            excel.SaveAs(memoryStream);
		//            memoryStream.WriteTo(Response.OutputStream);
		//            Response.Flush();
		//            Response.End();
		//        }


		//    }
		//    catch (Exception ex)
		//    {
		//        RedirectToAction("Error500");
		//    }
		//}

		//public void zabhdhvhlucqqplodjdc()
		//{
		//    try
		//    {

		//        var cons = (from g in db.Ganadores
		//                    join u in db.Usuario on g.Usuario.UsuarioId equals u.UsuarioId
		//                    select new Reporte2
		//                    {
		//                        UsuarioId = g.Usuario.UsuarioId.ToString(),
		//                        NombreDeUsuario = g.Usuario.Nombre + " " + g.Usuario.Apellidos,
		//                        CorreoElectronico = g.Usuario.CorreoElectronico,
		//                        TipoDePremio = (int)g.TipoDePremio,
		//                        ValorCodigo = g.ValorCodigo
		//                    }).ToList();


		//        ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
		//        ExcelPackage excel = new ExcelPackage();
		//        var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
		//        workSheet.TabColor = System.Drawing.Color.Black;
		//        workSheet.DefaultRowHeight = 12;
		//        //Header of table  
		//        //  
		//        workSheet.Row(1).Height = 20;
		//        workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
		//        workSheet.Row(1).Style.Font.Bold = true;
		//        workSheet.Cells[1, 1].Value = "UsuarioId";
		//        workSheet.Cells[1, 2].Value = "NombreDeUsuario";
		//        workSheet.Cells[1, 3].Value = "CorreoElectronico";
		//        workSheet.Cells[1, 4].Value = "TipoDePremio";
		//        workSheet.Cells[1, 5].Value = "ValorCodigo";



		//        //Body of table  
		//        //  
		//        int recordIndex = 2;
		//        foreach (var c in cons)
		//        {
		//            //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
		//            workSheet.Cells[recordIndex, 1].Value = c.UsuarioId;
		//            workSheet.Cells[recordIndex, 2].Value = c.NombreDeUsuario;
		//            workSheet.Cells[recordIndex, 3].Value = c.CorreoElectronico;
		//            workSheet.Cells[recordIndex, 4].Value = c.TipoDePremio;
		//            workSheet.Cells[recordIndex, 5].Value = c.ValorCodigo;
		//            recordIndex++;
		//        }



		//        string excelName = "Reporte_Premios_Snackea_" + DateTime.Now.ToString("dd - MM - yyyy");
		//        using (var memoryStream = new MemoryStream())
		//        {
		//            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		//            Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
		//            excel.SaveAs(memoryStream);
		//            memoryStream.WriteTo(Response.OutputStream);
		//            Response.Flush();
		//            Response.End();
		//        }


		//    }
		//    catch (Exception ex)
		//    {
		//        RedirectToAction("Error500");
		//    }
		//}

		[AllowAnonymous]
		public ActionResult Registro()
		{
			//if (User.Identity.IsAuthenticated) TODO: //
			//{
			//    return RedirectToAction("Home");
			//}
			return View();
		}


		[AllowAnonymous]
		public ActionResult Login()
		{
			if (User.Identity.IsAuthenticated)
			{
				return RedirectToAction("Home");
			}
			return View();
		}


		//public ActionResult LoginExterno()
		//{
		//    return View();
		//}




		#region generadorCSV
		public void WriteTsv<T>(IEnumerable<T> data, TextWriter output)
		{
			PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
			foreach (PropertyDescriptor prop in props)
			{
				output.Write(prop.DisplayName); // header
				output.Write("\t");
			}
			output.WriteLine();
			foreach (T item in data)
			{
				foreach (PropertyDescriptor prop in props)
				{
					output.Write(prop.Converter.ConvertToString(
						 prop.GetValue(item)));
					output.Write("\t");
				}
				output.WriteLine();
			}
		}
		#endregion


		#region intentFilter
		[AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
		public sealed class ValidateHeaderAntiForgeryTokenAttribute : FilterAttribute, IAuthorizationFilter
		{
			public void OnAuthorization(AuthorizationContext filterContext)
			{
				if (filterContext == null)
				{
					throw new ArgumentNullException("filterContext");
				}

				var httpContext = filterContext.HttpContext;
				var cookie = httpContext.Request.Cookies[AntiForgeryConfig.CookieName];
				AntiForgery.Validate(cookie != null ? cookie.Value : null, httpContext.Request.Headers["__RequestVerificationToken"]);
			}
		}
		#endregion

		#region generadorDePdf

		public ActionResult ComoCanjear(string premio)
		{
			var path = HostingEnvironment.MapPath("~/Content/pdf/" + premio + ".pdf");
			var file = path;
			file = Path.GetFullPath(file);
			if (!file.StartsWith(path))
			{
				throw new HttpException(403, "Forbidden");
			}
			return File(file, "application/pdf");
		}

		public ActionResult TerminosYCondiciones()
		{
			var path = HostingEnvironment.MapPath("~/Content/pdf/Tycos.pdf");
			var file = path;
			file = Path.GetFullPath(file);
			if (!file.StartsWith(path))
			{
				throw new HttpException(403, "Forbidden");
			}
			return File(file, "application/pdf");
		}
		public ActionResult Autorizacion()
		{
			var path = HostingEnvironment.MapPath("~/Content/pdf/Autorizacion.pdf");
			var file = path;
			file = Path.GetFullPath(file);
			if (!file.StartsWith(path))
			{
				throw new HttpException(403, "Forbidden");
			}
			return File(file, "application/pdf");
		}
		public ActionResult Faq()
		{
			var path = HostingEnvironment.MapPath("~/Content/pdf/Faq.pdf");
			var file = path;
			file = Path.GetFullPath(file);
			if (!file.StartsWith(path))
			{
				throw new HttpException(403, "Forbidden");
			}
			return File(file, "application/pdf");
		}

		public ActionResult MyGamePass()
		{
			var path = HostingEnvironment.MapPath("~/Content/pdf/Gamepass.pdf");
			var file = path;
			file = Path.GetFullPath(file);
			if (!file.StartsWith(path))
			{
				throw new HttpException(403, "Forbidden");
			}
			return File(file, "application/pdf");
		}



		public ActionResult MiRegistro()
		{
			return Redirect("https://bit.ly/3rTGczq");
		}




		//public ActionResult Faq()
		//{
		//    var path = HostingEnvironment.MapPath("~/Content/pdf/Faq.pdf");
		//    var file = path;
		//    file = Path.GetFullPath(file);
		//    if (!file.StartsWith(path))
		//    {
		//        throw new HttpException(403, "Forbidden");
		//    }
		//    return File(file, "application/pdf");
		//}

		#endregion

		#region validarusuario
		private Usuario ValidarUsuario()
		{
			string currentUserId = User.Identity.GetUserId();
			var u = Guid.Parse(currentUserId);
			Usuario currentUser = db.Usuario.FirstOrDefault(x => x.AspNetUserId == u);
			return currentUser;
		}
		#endregion validarusuario


		#region COGNITIVE_SERVICE.
		// Recognize text from a local image
		private static async Task<IList<TextRecognitionResult>> ExtractLocalTextAsync(ComputerVisionClient computerVision, string filePath)
		{
			try
			{

				using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				{
					// Start the async process to recognize the text
					BatchReadFileInStreamHeaders textHeaders =
						await computerVision.BatchReadFileInStreamAsync(fileStream);

					return await GetTextAsync(computerVision, textHeaders.OperationLocation);
				}
			}
			catch (Exception ex)
			{
				// ErrorLog.GetDefault(null).Log(new Error(ex));
				throw new NotImplementedException();
			}

		}

		// Retrieve the recognized text
		private static async Task<IList<TextRecognitionResult>> GetTextAsync(ComputerVisionClient computerVision, string operationLocation)
		{
			try
			{
				// Retrieve the URI where the recognized text will be
				// stored from the Operation-Location header
				string operationId = operationLocation.Substring(operationLocation.Length - ConstCharOp);

				ReadOperationResult result = await computerVision.GetReadOperationResultAsync(operationId);

				// Wait for the operation to complete
				int i = 0;
				int maxRetries = 10;
				while ((result.Status == TextOperationStatusCodes.Running ||
						result.Status == TextOperationStatusCodes.NotStarted) && i++ < maxRetries)
				{
					await Task.Delay(1000);
					result = await computerVision.GetReadOperationResultAsync(operationId);
				}

				return result.RecognitionResults;
			}
			catch (Exception ex)
			{
				// ErrorLog.GetDefault(null).Log(new Error(ex));
				throw new NotImplementedException();
			}
		}
		#endregion COGNITIVE_SERVICE.
	}
}
