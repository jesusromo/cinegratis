﻿using Kelloggs.Common;
using Kelloggs.Models;
using Kelloggs.Models.Clases;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Kelloggs.Data.Models;
using Kelloggs.DTO;
using Kelloggs.Helpers;
using System.Data.Entity.SqlServer;

namespace Kelloggs.Controllers
{
    public class AuthenticateController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationDbContext db = new ApplicationDbContext();
        //private UtilidadError ut = UtilidadError.Instance;



        public AuthenticateController()
        {
           
        }

        public AuthenticateController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }


        [AllowAnonymous]
        public ActionResult Registro()
        {
            return View();
        }

        #region validarusuario
        private Usuario ValidarUsuario()
        {
            using (var db = new ApplicationDbContext())
            {
                string currentUserId = User.Identity.GetUserId();
                var u = Guid.Parse(currentUserId);
                Usuario currentUser = db.Usuario.FirstOrDefault(x => x.AspNetUserId == u);
                return currentUser;
            }
        }
        #endregion validarusuario

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<bool> Registro(RegisterDTO registerDTO)
        {
            try
            {
                EpsilonUserDTO epsilonUserDTO = new EpsilonUserDTO();  //contraseña SuperS3cure4.


                epsilonUserDTO = registerDTO.EpsilonUserDTO;
                //return false;
                RegisterViewModel model = new RegisterViewModel();
                model.Email = epsilonUserDTO.UsernameForServiceAuth;
                model.Password = "Mescalina.@879848646";
                // Validar si ya existe el usuario e iniciar sesión.
                var usuarioDb = db.Usuario.Where(x => x.CorreoElectronico == epsilonUserDTO.UsernameForServiceAuth).FirstOrDefault();
                if (usuarioDb != null)
                {
                    if (usuarioDb.IP == "10.20.18.21" || usuarioDb.IP == "10.20.2.21")
                    {
                        // Actualizamos la IP del usuario.
                        var ip = ObtenerIP();
                        usuarioDb.IP = ip;
                        db.SaveChanges();
                    }
                    else
                    {
                        if (string.IsNullOrWhiteSpace(usuarioDb.Celular) && string.IsNullOrWhiteSpace(registerDTO.EpsilonUserDTO.Celular) == false)
                        {
                            usuarioDb.Celular = registerDTO.EpsilonUserDTO.Celular;
                            db.SaveChanges();
                        }
                        if (string.IsNullOrWhiteSpace(usuarioDb.Genero) && string.IsNullOrWhiteSpace(registerDTO.EpsilonUserDTO.Gender) == false)
                        {
                            usuarioDb.Genero = registerDTO.EpsilonUserDTO.Gender;
                            db.SaveChanges();
                        }
                        if (string.IsNullOrWhiteSpace(usuarioDb.Rol) && string.IsNullOrWhiteSpace(registerDTO.EpsilonUserDTO.FamilyRole) == false)
                        {
                            usuarioDb.Rol = registerDTO.EpsilonUserDTO.FamilyRole;
                            db.SaveChanges();
                        }
                    }
                    var result = await SignInManager.PasswordSignInAsync(usuarioDb.CorreoElectronico,
                        model.Password, false, shouldLockout: false);

                    var cuentaBloqueada = (from x in db.Users where x.Email == usuarioDb.CorreoElectronico select x.LockoutEndDateUtc).FirstOrDefault();
                    switch (result)
                    {
                        case SignInStatus.Success:
                            return true;
                        case SignInStatus.LockedOut:
                            return false;
                        case SignInStatus.RequiresVerification:
                            return false;
                        case SignInStatus.Failure:
                        default:
                            return false;
                    }
                }
                using (var context = new ApplicationDbContext())
                {
                    var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    if (result.Errors.Any())
                    {
                        ModelState.AddModelError("", result.Errors.FirstOrDefault());
                        return false;
                    }
                    var roleStore = new RoleStore<IdentityRole>(context);
                    var roleManager = new RoleManager<IdentityRole>(roleStore);
                    if (!roleManager.RoleExists("USER"))
                    {
                        await roleManager.CreateAsync(new IdentityRole("USER"));
                    }
                    var userStore = new UserStore<ApplicationUser>(context);
                    var userManager = new UserManager<ApplicationUser>(userStore);
                    userManager.AddToRole(user.Id, "USER");
                    if (result.Succeeded)
                    {

                        DateTime FechaNacimiento = epsilonUserDTO.BirthDate;

                        if(FechaNacimiento<=new DateTime(1900,01,01))
                        {
                            FechaNacimiento = new DateTime(2021,01,01);
                        }

                        try
                        {
                            var ip = ObtenerIP();
                            var nickName = epsilonUserDTO.UsernameForServiceAuth;
                            var usuario = new Usuario
                            {
                                UsuarioId = Guid.NewGuid(),
                                FechaRegistro = Utilidades.FechaColmbia(),
                                IP = ip,
                                Nombre = epsilonUserDTO.FirstName,
                                Apellidos = epsilonUserDTO.LastName,
                                CorreoElectronico = model.Email,
                                FechaNacimiento = FechaNacimiento,
                                AspNetUserId = Guid.Parse(user.Id),
                                Celular = epsilonUserDTO.Celular!=null? epsilonUserDTO.Celular:"",
                                Ciudad = epsilonUserDTO.City,
                                Direccion = epsilonUserDTO.AddressLine1,
                                Cedula = epsilonUserDTO.ProfileAttributes.CEDULATEXT,
                                Genero = epsilonUserDTO.Gender,
                                Rol = epsilonUserDTO.FamilyRole,
                                Folio = 0
                            };
                            db.Usuario.Add(usuario);
                            db.SaveChanges();
                        }
                        catch (DbEntityValidationException ex)
                        {
                            var errorMessages = ex.EntityValidationErrors
                                .SelectMany(x => x.ValidationErrors)
                                .Select(x => x.ErrorMessage);
                            var fullErrorMessage = string.Join("; ", errorMessages);
                            var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                            throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
                        }
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return true;
                    }
                    AddErrors(result);
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /*[HttpPost]
        [AllowAnonymous]
        public JsonResult CustomLogin(RegisterDTO registerDTO)
        {
            try
            {
                var response = new LoginResponse();
                RegisterViewModel model = new RegisterViewModel();



                if (string.IsNullOrEmpty(registerDTO.Email))
                {
                    response.Success = false;
                    response.EmailError = true;
                    response.NickNameError = false;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                if (string.IsNullOrEmpty(registerDTO.Password))
                {
                    response.Success = false;
                    response.EmailError = false;
                    response.NickNameError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }


                model.Email = registerDTO.Email.ToLower();
                var pass = Regex.Replace(registerDTO.Password, @"\s", "");
                model.Password = pass.ToLower();

                var nickName = db.Usuario.FirstOrDefault(w => w.Nickname == model.Password);
                if (nickName == null)
                {
                    response.Success = false;
                    response.EmailError = false;
                    response.NickNameError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }


                // Validar si ya existe el usuario e iniciar sesión.
                var usuarioDb = db.Usuario.FirstOrDefault(x => x.CorreoElectronico == registerDTO.Email);
                if (usuarioDb == null)
                {
                    response.Success = false;
                    response.EmailError = true;
                    response.NickNameError = false;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

            
                var result =  SignInManager.PasswordSignIn(usuarioDb.CorreoElectronico,
                    model.Password, false, shouldLockout: false);

                if (result == SignInStatus.Success)
                {
                    
                    response.Success = true;
                    response.EmailError = false;
                    response.NickNameError = false;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    response.Success = false;
                    response.EmailError = true;
                    response.NickNameError = true;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                var response = new LoginResponse
                {
                    Success = false,
                    EmailError = false,
                    NickNameError = false
                };
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }*/

        //public void OcurrioUnError(Exception ex, Usuario u)
        //{
        //    ElmaLog e = new ElmaLog();
        //    e.Error = ex.ToString();
        //    e.Usuario = u;
        //    db.ElmaLog.Add(e);
        //    db.SaveChanges();
        //    ut.Html = "<strong>" + ex.ToString() + "</strong>";
        //    ut.EnviarMensaje(ut);
        //}

        protected string ObtenerIP()
        {
            HttpContext context = System.Web.HttpContext.Current;
            string ipAddress = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipAddress))
            {
                string[] addresses = ipAddress.Split(',');
                if (addresses.Length != 0)
                    return addresses[0];
            }

            return context.Request.ServerVariables["REMOTE_ADDR"];
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

    }
}