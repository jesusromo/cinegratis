using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Kelloggs.DTO;
using Kelloggs.Data.Models;
using System.Net.Http;
using System.Text.RegularExpressions;
using Kelloggs.Data.Enum;
using Kelloggs.Helpers;
using Kelloggs.Models.Clases;

namespace Kelloggs.Controllers
{
    public class AdminController : Controller
    {
        private string adminUser = WebConfigurationManager.AppSettings["adminUser"];

        [Authorize]
        public ActionResult Index()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 2;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult SubProducto()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 1;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult Premio()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 2;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult Ganador()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 3;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult DLC()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 4;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult Configuracion()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 5;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }


        [Authorize]
        public ActionResult ValidarLatas()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 6;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }


        [Authorize]
        public ActionResult Reporte()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 7;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult ReporteB()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 8;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        [Authorize]
        public ActionResult ReporteC()
        {
            Usuario user = ValidarUsuario();
            if (adminUser.Contains(user.CorreoElectronico))
            {
                ViewBag.Page = 9;
                return View();
            }

            return RedirectToAction("Error404", "PromoController");
        }

        #region Productos
        [HttpGet]
        public JsonResult TiposProductos()
        {
            try
            {
                List<string> TiposProducto = new List<string>();
                TiposProducto.Add("Caja");
                TiposProducto.Add("Bolsa");
                return Json(TiposProducto, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }

        [HttpGet]
        public JsonResult Productos()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var productos = db.Productos.Select(s => new
                    {
                        id = s.ProductoId,
                        name = s.Nombre
                    }).OrderBy(p => p.name).ToList();
                    return Json(productos, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }


        [HttpGet]
        public JsonResult Configuraciones()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var configs = db.PromoSettings.Select(s => new
                    {
                        id = s.PromoSettingId,
                        name = s.TipoConfig,
                        status = s.EsActivo
                    }).OrderBy(x => x.name).ToList();
                    return Json(configs, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult SaveProduct(ProductDTO product)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }

                using (var db = new ApplicationDbContext())
                {
                    product.Name = product.Name.ToUpper();

                    var dbProduct = new Producto()
                    {
                        ProductoId = Guid.NewGuid(),
                        Nombre = product.Name,
                        FechaDeCreacion = DateTime.Now
                    };

                    db.Productos.Add(dbProduct);
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Registro Exitoso"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al guardar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult EditProduct(ProductDTO product)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }

                using (var db = new ApplicationDbContext())
                {

                    var id = Guid.Parse(product.Id);
                    var dbProduct = db.Productos.FirstOrDefault(w => w.ProductoId == id);
                    dbProduct.Nombre = product.Name.ToUpper();

                    if (db.ChangeTracker.HasChanges())
                    {
                        if (db.SaveChanges() > 0)
                        {
                            return Json(new Response()
                            {
                                Success = true,
                                Message = "Actualización Exitosa"
                            });
                        }
                        else
                        {
                            return Json(new Response()
                            {
                                Success = false,
                                Message = "Error al actualizar"
                            });
                        }
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "No se realizaron cambios"
                        });
                    }


                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult EditSubProduct(ProductDTO subProduct)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }

                using (var db = new ApplicationDbContext())
                {

                    var id = Guid.Parse(subProduct.Id);
                    var dbProduct = db.SubProductos.FirstOrDefault(w => w.SubProductoId == id);
                    dbProduct.Nombre = subProduct.Name.ToUpper();

                    if (db.ChangeTracker.HasChanges())
                    {
                        if (db.SaveChanges() > 0)
                        {
                            return Json(new Response()
                            {
                                Success = true,
                                Message = "Actualización Exitosa"
                            });
                        }
                        else
                        {
                            return Json(new Response()
                            {
                                Success = false,
                                Message = "Error al actualizar"
                            });
                        }
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "No se realizaron cambios"
                        });
                    }


                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }
        [HttpPost]
        [Authorize]
        public JsonResult SaveSubProduct(SubProductDTO product)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {

                    var dbSubProducto = new SubProducto()
                    {
                        SubProductoId = Guid.NewGuid(),
                        Nombre = product.SubProduct.ToUpper(),
                        ProductoId = Guid.Parse(product.ProductId),
                        FechaDeCreacion = DateTime.Now
                    };

                    db.SubProductos.Add(dbSubProducto);
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Registro Exitoso"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al guardar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpGet]
        public JsonResult SubProductos()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var productos = db.SubProductos.Select(s => new
                    {
                        id = s.SubProductoId,
                        product = s.Producto.Nombre,
                        subProducto = s.Nombre
                    }).ToList();

                    return Json(productos, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }




        [HttpGet]
        [Authorize]
        public JsonResult Dlcs()
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var dlcsAc = db.PremiosEspeciales.Count(w => w.FechaDeCanje != null);
                    var dlcsDisponibles = db.PremiosEspeciales.Count(w => w.FechaDeCanje == null);

                    var list = new List<DCLCounterDTO>();
                    list.Add(new DCLCounterDTO()
                    {
                        Nombre = "CANJEADOS",
                        Cantidad = dlcsAc.ToString()
                    });

                    list.Add(new DCLCounterDTO()
                    {
                        Nombre = "DISPONIBLES",
                        Cantidad = dlcsDisponibles.ToString()
                    });

                    return Json(list, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult DeletSubProduct(SubProductDTO product)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var searchId = Guid.Parse(product.SubProductId);
                    var dbSubProducto = db.SubProductos.FirstOrDefault(w => w.SubProductoId == searchId);
                    db.SubProductos.Remove(dbSubProducto);
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Eliminacion Exitosa"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al eliminar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult DeleteProduct(SubProductDTO product)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var searchId = Guid.Parse(product.ProductId);
                    var dbProduct = db.Productos.FirstOrDefault(w => w.ProductoId == searchId);
                    var subProducs = db.SubProductos.Where(w => w.ProductoId == searchId);
                    db.SubProductos.RemoveRange(subProducs);
                    db.Productos.Remove(dbProduct);
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Eliminacion Exitosa"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al eliminar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpGet]
        public JsonResult SubProductosById(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                    return Json(new List<SubProducto>(), JsonRequestBehavior.AllowGet);
                if (id == "null")
                    return Json(new List<SubProducto>(), JsonRequestBehavior.AllowGet);
                using (var db = new ApplicationDbContext())
                {
                    var idSearch = Guid.Parse(id);
                    var productos = db.SubProductos
                        .Where(w => w.ProductoId == idSearch).Select(s => new
                        {
                            id = s.SubProductoId,
                            subProducto = s.Nombre + " " + s.Gramaje,
                            gramaje = s.Puntaje
                        }).OrderBy(s => s.gramaje).ToList();

                    return Json(productos, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        /// <summary>
        /// Este metodo se utiliza cuando solamente existe un tipo de producto, si la promocion tiene mas de un producto usar el metodo SubProductosById
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public JsonResult SubProductosUnicos()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var producto = db.Productos.FirstOrDefault();
                    if (producto != null)
                    {
                        var idSearch = Guid.Parse(producto.ProductoId.ToString());
                        var productos = db.SubProductos
                            .Where(w => w.ProductoId == idSearch).Select(s => new
                            {
                                id = s.SubProductoId,
                                subProducto = s.Nombre
                            }).ToList();

                        return Json(productos, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new List<object>(), JsonRequestBehavior.AllowGet);
                    }

                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        #region premios
        [HttpGet]
        [Authorize]
        public JsonResult Premios()
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var productos = db.Premios.Select(s => new PremioDTO()
                    {
                        Id = s.PremioId.ToString(),
                        Nombre = s.Item,
                        Marca = s.Marca,
                        Cantidad = s.Cantidad,
                        NombreArchivo = s.NombreArchivo,
                        CantidadDisponible = s.CantidadDisponible,
                        Canjeado = s.Entregado
                    }).ToList();
                    return Json(productos, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }



        [HttpPost]
        [Authorize]
        public JsonResult SavePremio(PremioDTO premio)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var dbPremio = new Premio()
                    {
                        PremioId = Guid.NewGuid(),
                        Cantidad = premio.Cantidad,
                        CantidadDisponible = premio.Cantidad,
                        NombreArchivo = premio.NombreArchivo,
                        FechaDeCreacion = DateTime.Now,
                        Item = premio.Nombre,
                        Marca = premio.Marca
                    };

                    db.Premios.Add(dbPremio);

                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Registro Exitoso"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al guardar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult EditConfig(SettingsDTO settings)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var id = Guid.Parse(settings.Id);
                    var dbPremio = db.PromoSettings.FirstOrDefault(f => f.PromoSettingId == id);

                    dbPremio.EsActivo = !dbPremio.EsActivo;

                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Actualizacion Exitosa"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al guardar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult DeletePremio(PremioDTO premio)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var searchId = Guid.Parse(premio.Id);

                    var dbProduct = db.Premios.FirstOrDefault(w => w.PremioId == searchId);

                    db.Premios.Remove(dbProduct);
                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Eliminacion Exitosa"
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al eliminar"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }
        #endregion

        [HttpPost]
        [Authorize]
        public JsonResult SaveCode(CodeDTO code)
        {
            /*return Json(new Response()
            {
                Success = false,
                Message = "La fase de registro de empaques ha concluido, ¡Mucha suerte!",
                Code = 2
            });*/
            try
            {
                Usuario u = ValidarUsuario();
                using (var db = new ApplicationDbContext())
                {
                    /*var lanzarmodal = db.PromoSettings.FirstOrDefault(w => w.TipoConfig == "LanzarModal");
                    if (lanzarmodal != null && lanzarmodal.EsActivo == true)
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Ya finalizó el registro de codigos",
                            Code = 2
                        }, JsonRequestBehavior.AllowGet);
                    }*/
                    var auxDate = code.FechaVencimiento;
                    DateTime? fechaVencimiento = FixDate(code.FechaVencimiento);
                    if (string.IsNullOrEmpty(code.FechaVencimiento))
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en la fecha"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (fechaVencimiento == null)
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en la fecha"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    DateTime? fechaLote = FixDate(code.Lote);
                    if (string.IsNullOrEmpty(code.Lote))
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en el lote"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (fechaLote == null)
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en el lote"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (code.TipoProductId == null)
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en el tipo de producto"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    if (code.TipoProductId != "Caja" && code.TipoProductId != "Bolsa")
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en el tipo de producto"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    Guid SubproductoId = Guid.Parse(code.SubproductId);
                    var subProducto = db.SubProductos.FirstOrDefault(s => s.SubProductoId == SubproductoId);
                    if (subProducto == null)
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error en el gramaje"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    var monthLote = ((DateTime)fechaLote).Month;
                    var yearLote = ((DateTime)fechaLote).Year;
                    var lote = code.Lote;
                    var codigo = new CodigoUsuario()
                    {
                        UsuarioId = u.UsuarioId,
                        TipoProducto = code.TipoProductId,
                        SubProductoId = SubproductoId,
                        CodigoUsuarioId = Guid.NewGuid(),
                        FechaVencimiento = (DateTime)fechaVencimiento,
                        FechaCanje = DateTime.Now,
                        Lote = lote,
                        MonthLote = monthLote,
                        YearLote = yearLote,
                        Gramos = Convert.ToInt32(subProducto.Puntaje)
                    };

                    db.CodigoUsuario.Add(codigo);
                    if (db.SaveChanges() > 0)
                    {
                        //Verificamos si el usuario no tiene folio
                        if (u.Folio == 0)
                        {
                            Usuario currentUser = db.Usuario.FirstOrDefault(x => x.AspNetUserId == u.AspNetUserId);
                            //Buscamos ultimo Folio
                            int proximoFolio = 1;
                            var ultimoUsuario = db.Usuario.OrderByDescending(p => p.Folio).FirstOrDefault();
                            if (ultimoUsuario != null)
                            {
                                proximoFolio = ultimoUsuario.Folio + 1;
                            }
                            //Validamos si ya se asignó el folio, si ya se asignó subimos un folio
                            while(db.Usuario.FirstOrDefault(x => x.Folio == proximoFolio) != null)
                            {
                                proximoFolio++;
                            }
                            //Asignamos folio a usuario
                            currentUser.Folio = proximoFolio;
                            db.SaveChanges();
                            return Json(new Response()
                            {
                                Success = true,
                                Message = codigo.Gramos.ToString(),
                                Code = proximoFolio
                            });
                        }
                        return Json(new Response()
                        {
                            Success = true,
                            Message = codigo.Gramos.ToString()
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al subir codigo"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult UploadTicketCompra(TicketCompraInputDTO ticket)
        {
            try
            {
                using (var mydb = new ApplicationDbContext())
                {
                    Usuario u = ValidarUsuario();
                    byte[] fileData = null;
                    using (var binaryReader = new BinaryReader(ticket.File.InputStream))
                    {
                        fileData = binaryReader.ReadBytes(ticket.File.ContentLength);
                    }
                    var ticketCompra = new TicketCompra()
                    {
                        TicketCompraId = Guid.NewGuid(),
                        FechaSubido = DateTime.Now,
                        EsValido = EsValido.PENDIENTE,
                        Ticket = fileData,
                        UsuarioId = u.UsuarioId
                    };

                    mydb.TicketCompra.Add(ticketCompra);
                    if (mydb.SaveChanges() > 0)
                    {
                        int ticketsCompra = mydb.TicketCompra.Count(w => w.UsuarioId == u.UsuarioId);
                        return Json(new Response()
                        {
                            Success = true,
                            Message = ticketsCompra.ToString()
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al subir codigo"
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                EmailNotifier.sendError(ex.ToString() + Environment.NewLine + ex.Message.ToString() + Environment.NewLine + ex.StackTrace.ToString());
                return Json(new Response()
                {
                    Success = false,
                    Message = "Error: " + ex.Message
                });
            }
        }

        /*[HttpGet]
        public JsonResult SeedPremiosPromocion()
        {
            using (var db = new ApplicationDbContext())
            {
                if (db.Premios.Count() > 0)
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "los premios ya habian sido ingresados previamente..."
                    });
                }
                else
                {
                    Premio premio1 = new Premio() { PremioId = Guid.NewGuid(), Item = "RenaultKwidIconic", Marca = "Renault", Cantidad = 1, Descripcion = "Renault Kwid Iconic", CantidadDisponible = 1, FechaDeCreacion = DateTime.Now };
                    Premio premio2 = new Premio() { PremioId = Guid.NewGuid(), Item = "MotoAkt", Marca = "AKT", Cantidad = 10, Descripcion = "Moto AKT", CantidadDisponible = 600, FechaDeCreacion = DateTime.Now };
                    Premio premio3 = new Premio() { PremioId = Guid.NewGuid(), Item = "TarjetaBancolombia", Marca = "Bancolombia", Cantidad = 500, Descripcion = "Tarjeta Prepago Bancolombia", CantidadDisponible = 600, FechaDeCreacion = DateTime.Now };
                    db.Premios.Add(premio1);
                    db.Premios.Add(premio2);
                    db.Premios.Add(premio3);


                    //Se agrega seed de settings
                    db.PromoSettings.Add(new PromoSetting()
                    { EsActivo = false, Fecha = DateTime.Now, TipoConfig = "LanzarModal", PromoSettingId = Guid.Parse("f4348a20-3c2f-4142-9f78-bc7c22a58a08") });

                    db.PromoSettings.Add(new PromoSetting()
                    { EsActivo = false, Fecha = DateTime.Now, TipoConfig = "LanzarModalQuincenal", PromoSettingId = Guid.Parse("f4348a20-3c2f-4142-9f78-bc7c22a58a07") });


                    db.SaveChanges();
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Premios ingresados correctamente..."
                    });
                }
            }
        }*/

        /*[HttpGet]
        public JsonResult SeedDLC()
        {

            using (var context = new ApplicationDbContext())
            {

                if (context.PremiosEspeciales.Any())
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "los DLC ya han sido ingresados previamente..."
                    }, JsonRequestBehavior.AllowGet);
                }

                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {

                        context.SaveChanges();
                        transaction.Commit();
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Los DLC se cargaron correctamente"
                        }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al intentar registrar los DLC"
                        }, JsonRequestBehavior.AllowGet);
                    }

                }

            }
        }*/

        /*[HttpGet]
        public JsonResult SeedProductosPromocion()
        {
            using (var context = new ApplicationDbContext())
            {
                if (context.Productos.Any())
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "los productos ya habian sido ingresados previamente..."
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        try
                        {
                            var producto1 = new Producto() { ProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", FechaDeCreacion = DateTime.Now };
                            var producto2 = new Producto() { ProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", FechaDeCreacion = DateTime.Now };
                            var producto3 = new Producto() { ProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", FechaDeCreacion = DateTime.Now };
                            context.Productos.Add(producto1);
                            context.Productos.Add(producto2);
                            context.Productos.Add(producto3);
                            context.SaveChanges();

                            //Sub Productos
                            var subproducto1 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000846755", Gramaje = "40g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto2 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000846731", Gramaje = "37g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto3 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000846748", Gramaje = "40g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto4 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000847615", Gramaje = "40g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto5 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000159596", Gramaje = "40g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto6 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000846748", Gramaje = "40g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };
                            var subproducto7 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA CHICA", CodigoBarras = "38000846731", Gramaje = "37g", Puntaje = 1, FechaDeCreacion = DateTime.Now, ProductoId = producto1.ProductoId };

                            var subproducto8 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "7501008028582", Gramaje = "76g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };
                            var subproducto9 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "38000845246", Gramaje = "67g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };
                            var subproducto10 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "38000845246", Gramaje = "67g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };
                            var subproducto11 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "38000845253", Gramaje = "71g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };
                            var subproducto12 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "38000845253", Gramaje = "71g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };
                            var subproducto13 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA MEDIANA", CodigoBarras = "38000845260", Gramaje = "71g", Puntaje = 1.5, FechaDeCreacion = DateTime.Now, ProductoId = producto2.ProductoId };

                            var subproducto14 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184949", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto15 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000237966", Gramaje = "112g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto16 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184932", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto17 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184956", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto18 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000185182", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto19 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184932", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto20 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184949", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto21 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000184956", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto22 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000185182", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto23 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000185212", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto24 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000242717", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto25 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000242700", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto26 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000185083", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto27 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000185069", Gramaje = "124g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto28 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138638", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto29 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000165276", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto30 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138973", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto31 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000139048", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto32 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138720", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto33 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138812", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto34 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000139277", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto35 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000183928", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto36 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138874", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto37 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000138607", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto38 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000183713", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto39 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000216992", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto40 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000217012", Gramaje = "158g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto41 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000237997", Gramaje = "112g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto42 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000238123", Gramaje = "112g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto46 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000201141", Gramaje = "149g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto47 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000201158", Gramaje = "149g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto48 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000201165", Gramaje = "149g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto49 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000197864", Gramaje = "137g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };
                            var subproducto50 = new SubProducto() { SubProductoId = Guid.NewGuid(), Nombre = "LATA GRANDE", CodigoBarras = "38000197857", Gramaje = "137g", Puntaje = 2, FechaDeCreacion = DateTime.Now, ProductoId = producto3.ProductoId };



                            context.SubProductos.Add(subproducto1);
                            context.SubProductos.Add(subproducto2);
                            context.SubProductos.Add(subproducto3);
                            context.SubProductos.Add(subproducto4);
                            context.SubProductos.Add(subproducto5);
                            context.SubProductos.Add(subproducto6);
                            context.SubProductos.Add(subproducto7);
                            context.SubProductos.Add(subproducto8);
                            context.SubProductos.Add(subproducto9);
                            context.SubProductos.Add(subproducto10);
                            context.SubProductos.Add(subproducto11);
                            context.SubProductos.Add(subproducto12);
                            context.SubProductos.Add(subproducto13);
                            context.SubProductos.Add(subproducto14);
                            context.SubProductos.Add(subproducto15);
                            context.SubProductos.Add(subproducto16);
                            context.SubProductos.Add(subproducto17);
                            context.SubProductos.Add(subproducto18);
                            context.SubProductos.Add(subproducto19);
                            context.SubProductos.Add(subproducto20);
                            context.SubProductos.Add(subproducto21);
                            context.SubProductos.Add(subproducto22);
                            context.SubProductos.Add(subproducto23);
                            context.SubProductos.Add(subproducto24);
                            context.SubProductos.Add(subproducto25);
                            context.SubProductos.Add(subproducto26);
                            context.SubProductos.Add(subproducto27);
                            context.SubProductos.Add(subproducto28);
                            context.SubProductos.Add(subproducto29);
                            context.SubProductos.Add(subproducto30);
                            context.SubProductos.Add(subproducto31);
                            context.SubProductos.Add(subproducto32);
                            context.SubProductos.Add(subproducto33);
                            context.SubProductos.Add(subproducto34);
                            context.SubProductos.Add(subproducto35);
                            context.SubProductos.Add(subproducto36);
                            context.SubProductos.Add(subproducto37);
                            context.SubProductos.Add(subproducto38);
                            context.SubProductos.Add(subproducto39);
                            context.SubProductos.Add(subproducto40);
                            context.SubProductos.Add(subproducto41);
                            context.SubProductos.Add(subproducto42);
                            context.SubProductos.Add(subproducto46);
                            context.SubProductos.Add(subproducto47);
                            context.SubProductos.Add(subproducto48);
                            context.SubProductos.Add(subproducto49);
                            context.SubProductos.Add(subproducto50);
                            context.SaveChanges();
                            transaction.Commit();
                        }
                        catch (Exception e)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                            transaction.Rollback();
                        }
                    }
                    context.SaveChanges();

                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Productos ingresados correctamente..."
                    }, JsonRequestBehavior.AllowGet);
                }
            }

        }*/

        #region LATAS
        [HttpPost]
        [Authorize]
        public async System.Threading.Tasks.Task<JsonResult> GetLatas(ParametersDto parameters)
        {

            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var query = db.Tickets.Where(x => x.Cargado && !x.EsValido).Select(s => new TicketDTO()
                    {
                        Id = s.TicketId,
                        Lote = s.Folio,
                        FechaLote = s.FechaLote,
                        CodigoBarras = s.CodigoBarras,
                        Status = s.EsValido,
                        StatusStr = s.EsValido ? (s.EsValido && s.Puntos == 0) ? "Rechazado" : "Valido" : "Pendiente validar",
                        ImgLoteFecha = s.ProductImageMembrana,
                        ImgBarras = s.ProductImageBarras,
                        HasImgBarras = s.ProductImageBarras != null,
                        HasImgLote = s.ProductImageMembrana != null,
                        Puntaje = s.Puntos,
                        FechaRegistro = s.FechaRegistro
                    });



                    var count = query.Count();
                    var offset = (parameters.Paged - 1) * parameters.ItemsPaged;
                    var list = await query.OrderByDescending(id => id.Id).Skip(offset).Take(parameters.ItemsPaged).ToListAsync();

                    foreach (var item in list)
                    {
                        item.ImgBarrasStr = item.ImgBarras != null ? Convert.ToBase64String(item.ImgBarras) : "";
                        item.ImgLoteFechaStr =
                            item.ImgLoteFecha != null ? Convert.ToBase64String(item.ImgLoteFecha) : "";
                    }

                    var result = new PagedResultDto<TicketDTO>
                    {
                        Items = list,
                        Total = count
                    };




                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    serializer.MaxJsonLength = 500000000;

                    var json = Json(result, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = 500000000;
                    return json;


                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }


        #endregion

        #region GANADORES
        [HttpGet]
        public JsonResult Ganadores()
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var ganadores = db.Ganadores.Select(s => new
                    {
                        Id = s.GanadorId,
                        NombreGanador = s.Usuario.Nombre,
                        ApellidoGanador = s.Usuario.Apellidos,
                        NombrePremio = s.Premio.Item,
                        Codigo = s.ValorCodigo,
                        Corte = s.Semana,
                        Encabezado = s.GrupoAlias
                    }).ToList();
                    return Json(ganadores, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult UploadFile(HttpPostedFileBase file)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }

                if (file == null)
                {
                    return Json(new Response() { Success = false, Message = "No se encontro archivo adjunto" }, JsonRequestBehavior.AllowGet);
                }

                StreamReader csvreader = new StreamReader(file.InputStream);
                string[] values;
                var ganadores = new List<Ganador>();
                var count = 0;
                while (!csvreader.EndOfStream)
                {
                    var line = csvreader.ReadLine();
                    if (count == 0)
                    {
                        count++;
                    }
                    else
                    {
                        values = line.Split(',');
                        ganadores.Add(new Ganador()
                        {
                            GanadorId = Guid.NewGuid(),
                            UsuarioId = Guid.Parse(values[0].ToString()),
                            PremioId = Guid.Parse(values[1].ToString()),
                            TipoDePremio = 0,
                            FechaDeCreacion = DateTime.Now.AddYears(-1),
                            ValorCodigo = values[2].ToString(),
                            Semana = int.Parse(values[3].ToString()),
                            Grupo = "PENDIENTE",
                            GrupoAlias = values[4].ToString(),
                        });
                    }
                }
                using (var db = new ApplicationDbContext())
                {
                    db.Ganadores.AddRange(ganadores);
                    db.SaveChanges();
                    return Json(new Response() { Success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                return Json(new Response() { Success = false, Message = e.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult UploadFileDLC(HttpPostedFileBase file)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }

                StreamReader csvreader = new StreamReader(file.InputStream);
                string[] values;
                var premios = new List<PremioEspecial>();


                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required,
                    new System.TimeSpan(0, 30, 0)))
                {
                    ApplicationDbContext context = null;
                    try
                    {
                        context = new ApplicationDbContext();
                        context.Configuration.AutoDetectChangesEnabled = false;

                        int count = 0;
                        while (!csvreader.EndOfStream)
                        {
                            var line = csvreader.ReadLine();
                            var premio = new PremioEspecial()
                            {
                                PremioEspecialId = Guid.NewGuid(),
                                Codigo = line,
                                FechaDeCreacion = DateTime.Now,
                                TipoCuenta = "1"
                            };
                            ++count;
                            context = AddToContext(context, premio, count, 100, true);
                        }
                        context.SaveChanges();
                    }
                    finally
                    {
                        if (context != null)
                            context.Dispose();
                    }
                    scope.Complete();
                    return Json(new Response() { Success = true }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);

                return Json(new Response() { Success = false, Message = e.Message }, JsonRequestBehavior.AllowGet);

            }
        }

        private ApplicationDbContext AddToContext(ApplicationDbContext context,
            PremioEspecial entity, int count, int commitCount, bool recreateContext)
        {
            context.PremiosEspeciales.Add(entity);

            if (count % commitCount == 0)
            {
                context.SaveChanges();
                if (recreateContext)
                {
                    context.Dispose();
                    context = new ApplicationDbContext();
                    context.Configuration.AutoDetectChangesEnabled = false;
                }
            }
            return context;
        }


        #endregion

        #region validarusuario
        private Usuario ValidarUsuario()
        {
            using (var db = new ApplicationDbContext())
            {
                string currentUserId = User.Identity.GetUserId();
                var u = Guid.Parse(currentUserId);
                Usuario currentUser = db.Usuario.FirstOrDefault(x => x.AspNetUserId == u);
                return currentUser;
            }
        }
        #endregion validarusuario


        #region Reportes
        [HttpGet]
        [Authorize]
        public JsonResult UsuariosRegistrados()
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var usuarios = db.Usuario.ToList().Select(s => new
                    {
                        Id = s.UsuarioId,
                        Nombre = s.Nombre + " " + s.Apellidos,
                        Email = s.CorreoElectronico,
                        Ciudad = s.Ciudad,
                        Telefono = s.Celular,
                        Direccion = s.Direccion + ", " + s.Ciudad,
                        
                        FechaNacimiento = s.FechaNacimiento.ToString("dd/MM/yyyy"),
                        FechaRegistro = s.FechaRegistro.AddHours(-5).ToString("dd/MM/yyyy  hh:mm tt"),
                    }).ToList();
                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    serializer.MaxJsonLength = 500000000;

                    var json = Json(usuarios, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = 500000000;
                    return json;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }




        [HttpPost]
        [Authorize]
        public JsonResult Reporte2(ParametersDto parameters)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
	                var query = db.Tickets.Include(x => x.Usuarios).Select(s => new
	                {
                        s.TicketId,
                        s.FechaRegistro, 
                        s.ProductImageMembrana,
                        s.LoteA,
                        s.LoteB,
                        s.Usuarios.Nombre,
                        s.Usuarios.Apellidos,
                        s.Usuarios.CorreoElectronico,
                        s.Cargado
	                });
	                var count = query.Count();
	                var offset = (parameters.Paged - 1) * parameters.ItemsPaged;
	                var list =  query.OrderByDescending(id => id.FechaRegistro).Skip(offset).Take(parameters.ItemsPaged).ToList();

	                var formattedList = list.Select(s => new LatasDTO
                    {
		                Id = s.TicketId, 
                        CorreoElectronico = s.CorreoElectronico,
                        Usuario = s.Nombre+" "+ s.Apellidos,
                        FechaRegistro = s.FechaRegistro,
                        LoteB = s.LoteB,
                        LoteA = s.LoteA,
                        ProductImageMembrana = Convert.ToBase64String(s.ProductImageMembrana),
                        Cargado = s.Cargado
                    }).ToList();

                    var result = new PagedResultDto<LatasDTO>
	                {
		                Items = formattedList,
		                Total = count
	                };

                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
	                serializer.MaxJsonLength = 500000000;

	                var json = Json(result, JsonRequestBehavior.AllowGet);
	                json.MaxJsonLength = 500000000;
	                return json;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }



        [HttpPost]
        [Authorize]
        public async System.Threading.Tasks.Task<JsonResult> Reporte3(ParametersDto parameters)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var query = db.Tickets;

                    var count = query.Count();
                    var offset = (parameters.Paged - 1) * parameters.ItemsPaged;
                    var list = await query.OrderByDescending(id => id.FechaRegistro).Skip(offset).Take(parameters.ItemsPaged).ToListAsync();

                    var formattedList = list.Select(s => new ReportCDTO
                    {
                        IdUsuario = s.UsuarioId.ToString(),
                        LoteA = s.LoteA,
                        NombrePremio = s.Premios.NombreArchivo,
                        LoteB = s.LoteB,
                        PremioEntregado = s.Premios.Entregado ? "Entregado" : "Sin Premio",
                        RegistroCompletado = s.Cargado? "Si":"No",
                        NombreUsuario = s.Usuarios.Nombre,
                        Apellido = s.Usuarios.Apellidos,
                        FechaNacimiento = s.Usuarios.FechaNacimiento.ToString("yyyy/MM/dd"),
                        Cedula = s.Usuarios.Cedula,
                        CorreoElectonico = s.Usuarios.CorreoElectronico,
                        Direccion = s.Usuarios.Direccion,
                        Ciudad = s.Usuarios.Ciudad,
                        Telefono = s.Usuarios.Celular,
                        FechaCanjeCompleta = s.FechaRegistro.AddHours(-5).ToString("yyyy/MM/dd HH:mm:ss")
                    }).ToList();

                    var result = new PagedResultDto<ReportCDTO>
                    {
                        Items = formattedList,
                        Total = count
                    };


                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    serializer.MaxJsonLength = 500000000;

                    var json = Json(result, JsonRequestBehavior.AllowGet);
                    json.MaxJsonLength = 500000000;
                    return json;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public void ReportCExcel(RangoFecha Fechas)
        {
            try
            {
                using (var db = new ApplicationDbContext())
                {


                    DateTime FechaInicial = new DateTime();
                    DateTime FechaFinal = new DateTime();


                    var ValidacionfechaInicial = DateTime.TryParseExact(Fechas.FechaInicial, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaInicial);
                    var ValidacionfechaFinal = DateTime.TryParseExact(Fechas.FechaFinal, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal);

                    if (ValidacionfechaInicial == true)
                    {
                        FechaInicial = FechaInicial.AddHours(5);

                    }
                    else
                    {
                        FechaInicial = DateTime.Today.AddYears(-10);
                    }


                    if (ValidacionfechaFinal == true)
                    {
                        FechaFinal = FechaFinal.AddDays(1).AddSeconds(-1);
                        FechaFinal = FechaFinal.AddHours(5);
                    }
                    else
                    {
                        FechaFinal = DateTime.Today.AddYears(10);
                    }

                    var listed = db.Tickets.Where(x => x.FechaRegistro >= FechaInicial && x.FechaRegistro <= FechaFinal)
                        .OrderBy(x => x.FechaRegistro).ToList();

                    var cons = listed.Select(s => new ReportCDTO
                        {
                            IdUsuario = s.UsuarioId.ToString(),
                            LoteA = s.LoteA,
                            NombrePremio = s.Premios.NombreArchivo,
                            LoteB = s.LoteB,
                            PremioEntregado = s.Premios.Entregado ? "Entregado" : "Sin Premio",
                            RegistroCompletado = s.Cargado ? "Si" : "No",
                            NombreUsuario = s.Usuarios.Nombre,
                            Apellido = s.Usuarios.Apellidos,
                            FechaNacimiento = s.Usuarios.FechaNacimiento.ToString("yyyy/MM/dd"),
                            Cedula = s.Usuarios.Cedula,
                            CorreoElectonico = s.Usuarios.CorreoElectronico,
                            Direccion = s.Usuarios.Direccion,
                            Ciudad = s.Usuarios.Ciudad,
                            Telefono = s.Usuarios.Celular,
                            FechaCanjeCompleta = s.FechaRegistro.AddHours(-5).ToString("yyyy/MM/dd HH:mm:ss"),
                            FechaCargada = s.FechaRegistro.AddHours(-5)
                        }).ToList();

                   
                    

                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                    workSheet.TabColor = System.Drawing.Color.Black;
                    workSheet.DefaultRowHeight = 12;
                    //Header of table  
                    //  
                    workSheet.Row(1).Height = 20;
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "IdUsuario";
                    workSheet.Cells[1, 2].Value = "Correo Electonico";
                    workSheet.Cells[1, 3].Value = "Nombre Usuario";
                    workSheet.Cells[1, 4].Value = "Apellido";
                    workSheet.Cells[1, 5].Value = "Fecha Nacimiento";
                    workSheet.Cells[1, 6].Value = "Fecha Canje Completa";
                    workSheet.Cells[1, 7].Value = "Cedula";
                    workSheet.Cells[1, 8].Value = "Direccion";
                    workSheet.Cells[1, 9].Value = "Ciudad";
                    workSheet.Cells[1, 10].Value = "Telefono";
                    workSheet.Cells[1, 11].Value = "LoteA";
                    workSheet.Cells[1, 12].Value = "LoteB";
                    workSheet.Cells[1, 13].Value = "Registro Completado";
                    workSheet.Cells[1, 14].Value = "Nombre Premio";
                    workSheet.Cells[1, 15].Value = "Premio Entregado";

                    //Body of table  
                    //  
                    int recordIndex = 2;
                    foreach (var c in cons)
                    {
                        //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 1].Value = c.IdUsuario;
                        workSheet.Cells[recordIndex, 2].Value = c.CorreoElectonico;
                        workSheet.Cells[recordIndex, 3].Value = StringHelpers.RemoveDiacritics(c.NombreUsuario == null ? "" : c.NombreUsuario);
                        workSheet.Cells[recordIndex, 4].Value = StringHelpers.RemoveDiacritics(c.Apellido == null ? "" : c.Apellido);
                        workSheet.Cells[recordIndex, 5].Value = c.FechaNacimiento;
                        workSheet.Cells[recordIndex, 6].Value = c.FechaCanjeCompleta;
                        workSheet.Cells[recordIndex, 7].Value = c.Cedula;
                        workSheet.Cells[recordIndex, 8].Value = c.Direccion;
                        workSheet.Cells[recordIndex, 9].Value = c.Ciudad;
                        workSheet.Cells[recordIndex, 10].Value = c.Telefono;
                        workSheet.Cells[recordIndex, 11].Value = c.LoteA;
                        workSheet.Cells[recordIndex, 12].Value = c.LoteB;
                        workSheet.Cells[recordIndex, 13].Value = c.RegistroCompletado;
                        workSheet.Cells[recordIndex, 14].Value = c.NombrePremio;
                        workSheet.Cells[recordIndex, 15].Value = c.PremioEntregado;
                        recordIndex++;
                    }

                    workSheet.Column(1).AutoFit();
                    workSheet.Column(2).AutoFit();
                    workSheet.Column(3).AutoFit();
                    workSheet.Column(4).AutoFit();

                    string excelName = "Reporte_ceoncentrado_" + DateTime.Now.ToString("dd - MM - yyyy");
                    using (var memoryStream = new MemoryStream())
                    {
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception ex)
            {
                RedirectToAction("Error500");
            }
        }



        [HttpPost]
        [Authorize]
        public void ReportBExcel(RangoFecha Fechas)
        {
            try
            {

                DateTime FechaInicial = new DateTime();
                DateTime FechaFinal = new DateTime();


                var ValidacionfechaInicial = DateTime.TryParseExact(Fechas.FechaInicial, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaInicial);
                var ValidacionfechaFinal = DateTime.TryParseExact(Fechas.FechaFinal, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal);

                if (ValidacionfechaInicial == true)
                {
                    FechaInicial = FechaInicial.AddHours(5);

                }
                else
                {
                    FechaInicial = DateTime.Today.AddYears(-10);
                }


                if (ValidacionfechaFinal == true)
                {
                    FechaFinal = FechaFinal.AddDays(1).AddSeconds(-1);
                    FechaFinal = FechaFinal.AddHours(5);
                }
                else
                {
                    FechaFinal = DateTime.Today.AddYears(10);
                }

                using (var db = new ApplicationDbContext())
                {
	                var query = db.Tickets.Include(x => x.Usuarios).Where(x => x.FechaRegistro >= FechaInicial && x.FechaRegistro <= FechaFinal).Select(s => new
	                {
		                s.TicketId,
		                s.FechaRegistro,
		                s.LoteA,
		                s.LoteB,
		                s.Usuarios.Nombre,
		                s.Usuarios.Apellidos,
		                s.Usuarios.CorreoElectronico,
		                s.Cargado
	                }).OrderBy(x => x.FechaRegistro).ToList()
	                .Select(s => new
	                {
		                TicketId = s.TicketId,
		                FechaVencimiento = s.FechaRegistro.AddHours(-5).ToString("yyyy/MM/dd  hh:mm tt"),
		                LoteA = s.LoteA,
		                LoteB = s.LoteB,
		                Email = s.CorreoElectronico,
		                Completo = s.Cargado?"Si" :"No" ,
		                Nombre = s.Nombre +" "+ s.Apellidos,
	                });



                  


                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                    workSheet.TabColor = System.Drawing.Color.Black;
                    workSheet.DefaultRowHeight = 12;

                    workSheet.Row(1).Height = 20;
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "TicketId";
                    workSheet.Cells[1, 2].Value = "FechaVencimiento";
                    workSheet.Cells[1, 3].Value = "LoteA";
                    workSheet.Cells[1, 4].Value = "LoteB";
                    workSheet.Cells[1, 5].Value = "Email";
                    workSheet.Cells[1, 6].Value = "Completo";
                    workSheet.Cells[1, 7].Value = "Nombre";


                    var recordIndex = 2;
                    foreach (var c in query)
                    {
                        //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 1].Value = c.TicketId;
                        workSheet.Cells[recordIndex, 2].Value = c.FechaVencimiento;
                        workSheet.Cells[recordIndex, 3].Value = c.LoteA;
                        workSheet.Cells[recordIndex, 4].Value = c.LoteB;
                        workSheet.Cells[recordIndex, 5].Value = c.Email;
                        workSheet.Cells[recordIndex, 6].Value = c.Completo;
                        workSheet.Cells[recordIndex, 7].Value = c.Nombre;
                        recordIndex++;
                    }

                    string excelName = "Reporte_Latas_" + DateTime.Now.ToString("dd-MM-yyyy");
                    using (var memoryStream = new MemoryStream())
                    {
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        [Authorize]
        public void ReportUser(RangoFecha Fechas)
        {
            try
            {

                DateTime FechaInicial = new DateTime();
                DateTime FechaFinal = new DateTime();


                var ValidacionfechaInicial = DateTime.TryParseExact(Fechas.FechaInicial, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaInicial);
                var ValidacionfechaFinal = DateTime.TryParseExact(Fechas.FechaFinal, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out FechaFinal);

                if (ValidacionfechaInicial == true)
                {
                    FechaInicial = FechaInicial.AddHours(5);

                }
                else
                {
                    FechaInicial = DateTime.Today.AddYears(-10);
                }


                if (ValidacionfechaFinal == true)
                {
                    FechaFinal = FechaFinal.AddDays(1).AddSeconds(-1);
                    FechaFinal = FechaFinal.AddHours(5);
                }
                else
                {
                    FechaFinal = DateTime.Today.AddYears(10);
                }

                using (var db = new ApplicationDbContext())
                {
	                var query = db.Usuario.Where(x => x.FechaRegistro >= FechaInicial && x.FechaRegistro <= FechaFinal).Select(s => new
	                    {
		                    s.UsuarioId,
		                    s.Nombre,
		                    s.Apellidos,
		                    s.CorreoElectronico,
		                    s.Ciudad,
		                    s.Celular,
		                    s.Direccion,
		                    s.FechaNacimiento,
		                    s.FechaRegistro

                    }).OrderBy(x => x.FechaRegistro).ToList()
	                    .Select(s => new
	                    {
		                    Id = s.UsuarioId,
		                    Nombre = s.Nombre + " " + s.Apellidos,
		                    Email = s.CorreoElectronico,
		                    Ciudad = s.Ciudad,
		                    Telefono = s.Celular,
		                    Direccion = s.Direccion + ", " + s.Ciudad,
		                    FechaNacimiento = s.FechaNacimiento.ToString("dd/MM/yyyy"),
		                    FechaRegistro = s.FechaRegistro.AddHours(-5).ToString("dd/MM/yyyy  hh:mm tt"),
                        });





                    ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                    ExcelPackage excel = new ExcelPackage();
                    var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
                    workSheet.TabColor = System.Drawing.Color.Black;
                    workSheet.DefaultRowHeight = 12;

                    workSheet.Row(1).Height = 20;
                    workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Row(1).Style.Font.Bold = true;
                    workSheet.Cells[1, 1].Value = "UsuarioId";
                    workSheet.Cells[1, 2].Value = "Nombre";
                    workSheet.Cells[1, 3].Value = "Email";
                    workSheet.Cells[1, 4].Value = "Ciudad";
                    workSheet.Cells[1, 5].Value = "Telefono";
                    workSheet.Cells[1, 6].Value = "Direccion";
                    workSheet.Cells[1, 7].Value = "FechaNacimiento";
                    workSheet.Cells[1, 8].Value = "FechaRegistro";


                    var recordIndex = 2;
                    foreach (var c in query)
                    {
                        //workSheet.Cells[recordIndex, 1].Value = (recordIndex - 1).ToString();
                        workSheet.Cells[recordIndex, 1].Value = c.Id;
                        workSheet.Cells[recordIndex, 2].Value = c.Nombre;
                        workSheet.Cells[recordIndex, 3].Value = c.Email;
                        workSheet.Cells[recordIndex, 4].Value = c.Ciudad;
                        workSheet.Cells[recordIndex, 5].Value = c.Telefono;
                        workSheet.Cells[recordIndex, 6].Value = c.Direccion;
                        workSheet.Cells[recordIndex, 7].Value = c.FechaNacimiento;
                        workSheet.Cells[recordIndex, 8].Value = c.FechaRegistro;
                        recordIndex++;
                    }

                    string excelName = "Reporte_Usuarios_" + DateTime.Now.ToString("dd-MM-yyyy");
                    using (var memoryStream = new MemoryStream())
                    {
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("content-disposition", "attachment; filename=" + excelName + ".xlsx");
                        excel.SaveAs(memoryStream);
                        memoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }

                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        #endregion
        #endregion

        public DateTime? FixDate(string stringDate)
        {
            DateTime dt;
            if (DateTime.TryParse(stringDate, out dt))
            {
                return dt;
            }
            else
            {
                return null;
            }
        }

        public string FixDate2(string stringDate)
        {
            DateTime dt;
            if (DateTime.TryParse(stringDate, out dt))
            {
                return dt.ToString("dd/MM/yyyy HH:mm");
            }
            else
            {
                return "";
            }
        }

        [HttpPost]
        [Authorize]
        public JsonResult ValidarLata(TicketDTO lata)
        {
            try
            {
                Usuario user = ValidarUsuario();
                if (!adminUser.Contains(user.CorreoElectronico))
                {
                    return Json(new Response()
                    {
                        Success = false,
                        Message = "Usuario no autorizado"
                    });
                }
                using (var db = new ApplicationDbContext())
                {
                    var latadb = db.Tickets.FirstOrDefault(w => w.TicketId == lata.Id);
                    latadb.EsValido = true;

                    if (lata.Correcto == false)
                    {
                        latadb.Puntos = 0;
                    }


                    if (db.SaveChanges() > 0)
                    {
                        return Json(new Response()
                        {
                            Success = true,
                            Message = "Se valido el lote: " + latadb.Folio
                        });
                    }
                    else
                    {
                        return Json(new Response()
                        {
                            Success = false,
                            Message = "Error al validar, intenta mas tarde"
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                throw;
            }
        }

        public ActionResult GetCitys(string searchTerm)
        {
            List<string> citysSource = new List<string> { "Bogotá", "Leticia", "El Encanto", "La Chorrera", "La Pedrera", "La Victoria", "Mirití-Paraná", "Puerto Alegría", "Puerto Arica", "Puerto Nariño", "Puerto Santander", "Tarapacá", "Medellin", "Abejorral", "Abriaquí", "Alejandría", "Amagá", "Amalfi", "Andes", "Angelópolis", "Angostura", "Anorí", "Anza", "Apartadó", "Arboletes", "Argelia", "Armenia", "Barbosa", "Bello", "Belmira", "Betania", "Betulia", "Briceño", "Buriticá", "Cáceres", "Caicedo", "Caldas", "Campamento", "Cañasgordas", "Caracolí", "Caramanta", "Carepa", "Carolina", "Caucasia", "Chigorodó", "Cisneros", "Ciudad bolívar", "Cocorná", "Concepción", "Concordia", "Copacabana", "Dabeiba", "Don Matías", "Ebéjico", "El Bagre", "El Carmen de Viboral", "El Santuario", "Entrerríos", "Envigado", "Fredonia", "Frontino", "Giraldo", "Girardota", "Gómez Plata", "Granada", "Guadalupe", "Guarne", "Guatapé", "Heliconia", "Hispania", "Itagüí", "Ituango", "Jardín", "Jericó", "La Ceja", "La Estrella", "La Pintada", "La Unión", "Liborina", "Maceo", "Marinilla", "Montebello", "Murindó", "Mutatá", "Nariño", "Nechí", "Necoclí", "Olaya", "Peñol", "Peque", "Pueblorrico", "Puerto Berrío", "Puerto Nare", "Puerto Triunfo", "Remedios", "Retiro", "Rionegro", "Sabanalarga", "Sabaneta", "Salgar", "San Andrés de Cuerquia", "San carlos", "San Francisco", "San Jerónimo", "San José de la Montaña", "San Juan de Urabá", "San Luis", "San Pedro de los Milagros", "San Pedro de Urabá", "San rafael", "San Roque", "San Vicente", "Santa Bárbara", "Santa Fe de Antioquia", "Santa Rosa de Osos", "Santo Domingo", "Segovia", "Hijo, hijo", "Sopetrán", "Támesis", "Tarazá", "Tarso", "Titiribí", "Toledo", "Turbo", "Uramita", "Urrao", "Valdivia", "Valparaíso", "Vegachí", "Venecia", "Vigía del Fuerte", "Yalí", "Yarumal", "Yolombó", "Yondó", "Zaragoza", "Arauca", "Arauquita", "Cravo Norte", "Fortul", "Puerto Rondón", "Saravena", "Domar", "Barranquilla", "Baranoa", "Campo de la cruz", "Candelaria", "Galapa", "Juan de Acosta", "Luruaco", "Malambo", "Manatí", "Palmar de Varela", "Piojó", "Polonuevo", "Ponedera", "Puerto Colombia", "Repelón", "Sabanagrande", "Sabanalarga", "Santa Lucía", "Santo Tomás", "Soledad", "Suán", "Tubará", "Usiacurí", "Cartagena", "Achí", "Altos del Rosario", "Arenal del Sur", "Arjona", "Arroyohondo", "Barranco de Loba", "Calamar", "Cantagallo", "Carmen de bolivar", "Cicuco", "Clemencia", "Córdoba", "El Guamo", "El Peñón", "Hatillo de Loba", "Magangué", "Mahates", "Margarita", "María La Baja", "Monte Cristo", "Morales", "Pinillos", "Regidor", "Río Viejo", "San Cristóbal", "San Estanislao", "San Fernando", "San Jacinto del Cauca", "San Jacinto", "San Juan Nepomuceno", "San Martín de Loba", "San Pablo", "Santa catalina", "Santa Cruz de Mompox", "Santa Rosa del Sur", "Santa Rosa", "Simití", "Soplaviento", "Talaigua Nuevo", "Tiquisio", "Turbaco", "Turbaná", "Villanueva", "Zambrano", "Tunja", "Almeida", "Aquitania", "Arcabuco", "Belén", "Berbeo", "Betéitiva", "Boavita", "Boyacá", "Briceño", "Buena Vista", "Busbanzá", "Caldas", "Campohermoso", "Cerinza", "Chinavita", "Chiquinquirá", "Chíquiza", "Chiscas", "Chita", "Chitaraque", "Chivatá", "Chivor", "Ciénega", "Cómbita", "Barco con alcohol", "Corrales", "Covarachía", "Cubará", "Cucaita", "Cuítiva", "Duitama", "El Cocuy", "El Espino", "Firavitoba", "Floresta", "Gachantivá", "Gámeza", "Garagoa", "Guacamayas", "Guateque", "Guayatá", "Güicán", "Iza", "Jenesano", "Jericó", "La Capilla", "La Uvita", "La Victoria", "Labranzagrande", "Macanal", "Maripí", "Miraflores", "Mongua", "Monguí", "Moniquirá", "Motavita", "Muzo", "Nobsa", "Nuevo Colón", "Oicatá", "Otanche", "Pachavita", "Páez", "Paipa", "Pajarito", "Panqueba", "Pauna", "Paya", "Paz de Río", "Pesca", "Pisba", "Puerto Boyacá", "Quípama", "Ramiriquí", "Ráquira", "Rondón", "Saboyá", "Sáchica", "Samacá", "San Eduardo", "San José de Pare", "San Luis de Gaceno", "San Mateo", "San Miguel de Sema", "San Pablo de Borbur", "Santa Maria", "Santa Rosa de Viterbo", "Santa Sofía", "Santana", "Sativanorte", "Sativasur", "Siachoque", "Soatá", "Socha", "Socotá", "Sogamoso", "Somondoco", "Sora", "Soracá", "Sotaquirá", "Susacón", "Sutamarchán", "Sutatenza", "Tasco", "Tenza", "Tibaná", "Tibasosa", "Tinjacá", "Tipacoque", "Toca", "Togüí", "Tópaga", "Tota", "Tununguá", "Turmequé", "Tuta", "Tutazá", "Úmbita", "Ventaquemada", "Villa de Leyva", "Viracachá", "Zetaquira", "Manizales", "Aguadas", "Anserma", "Aranzazu", "Belalcázar", "Chinchiná", "Filadelfia", "La Dorada", "La Merced", "Manzanares", "Marmato", "Marquetalia", "Marulanda", "Neira", "Norcasia", "Pácora", "Palestina", "Pensilvania", "Riosucio", "Risaralda", "Salamina", "Samaná", "San Jose", "Supía", "Victoria", "Villamaría", "Viterbo", "Florencia", "Albania", "Belén de Andaquies", "Cartagena del Chairá", "Curillo", "El Doncello", "El Paujil", "La Montañita", "Milán", "Morelia", "Puerto Rico", "San José del Fragua", "San Vicente del Caguán", "Solano", "Solita", "Valparaíso", "Yopal", "Aguazul", "Chámeza", "Hato Corozal", "La Salina", "Maní", "Monterrey", "Nunchía", "Orocué", "Paz de Ariporo", "Poro", "Recetor", "Sabanalarga", "Sácama", "San Luis de Palenque", "Támara", "Tauramena", "Trinidad", "Villanueva", "Popayán", "Almaguer", "Argelia", "Balboa", "Bolívar", "Buenos Aires", "Cajibio", "Caldono", "Caloto", "Corinto", "El Tambo", "Florencia", "Guapi", "Inzá", "Jambaló", "La Sierra", "La vega", "López de Micay", "Mercaderes", "Miranda", "Morales", "Padilla", "Páez", "Patía", "Piamonte", "Piendamó", "Puerto Tejada", "Puracé", "Rosas", "San Sebastian", "Santa Rosa", "Santander de Quilichao", "Silvia", "Sotara", "Suárez", "Sucre", "Timbío", "Timbiquí", "Toribio", "Totoró", "Villa Rica", "Valledupar", "Aguachica", "Agustín Codazzi", "Astrea", "Becerril", "Bosconia", "Chimichagua", "Chiriguaná", "Curumaní", "El Copey", "El Paso", "Gamarra", "González", "La Gloria", "La Jagua de Ibirico", "La paz", "Manaure", "Pailitas", "Pelaya", "Pueblo Bello", "Río de Oro", "San Alberto", "San Diego", "San Martín", "Tamalameque", "Quibdó", "Acandí", "Alto Baudó", "Atrato", "Bagadó", "Bahía Solano", "Bajo Baudó", "Belén de Bajirá", "Bojayá", "El Carmen de Atrato", "El Carmen del Darién", "Cértegui", "Condoto", "El Cantón de San Pablo", "Istmina", "Juradó", "Litoral del San Juan", "Lloró", "Medio Atrato", "Medio Baudó", "Medio San Juan", "Nóvita", "Nuquí", "Río Iró", "Río Quito", "Riosucio", "San José del Palmar", "Sipí", "Tadó", "Unguía", "Unión Panamericana", "Montería", "Ayapel", "Buena Vista", "Canalete", "Cereté", "Chimá", "Chinú", "Ciénaga de Oro", "Cotorra", "La Apartada", "Los Córdobas", "Momil", "Moñitos", "Montelíbano", "Planeta Rica", "Pueblo Nuevo", "Puerto Escondido", "Puerto Libertador", "Purísima", "Sahagún", "San Andrés de Sotavento", "San Antero", "San Bernardo del Viento", "San carlos", "San Pelayo", "Santa Cruz de Lorica", "Tierralta", "Valencia", "Agua de Dios", "Albán", "Anapoima", "Anolaima", "Apulo", "Arbeláez", "Beltrán", "Bituima", "Bojacá", "Cabrera", "Cachipay", "Cajicá", "Caparrapí", "Cáqueza", "Carmen de Carupa", "Chaguaní", "Chía", "Chipaque", "Choachí", "Chocontá", "Cogua", "Cota", "Cucunubá", "El Colegio", "El Peñón", "El Rosal", "Facatativá", "Fómeque", "Fosca", "Funza", "Fúquene", "Fusagasugá", "Gachalá", "Gachancipá", "Gachetá", "Gama", "Girardot", "Granada", "Guachetá", "Guaduas", "Guasca", "Guataquí", "Guatavita", "Guayabal de Síquima", "Guayabetal", "Gutiérrez", "Jerusalén", "Junín", "La Calera", "La Mesa", "La palma", "La Peña", "La vega", "Lenguazaque", "Machetá", "Madrid", "Manta", "Medina", "Mosquera", "Nariño", "Nemocón", "Nilo", "Nimaima", "Nocaima", "Pacho", "Paime", "Pandi", "Paratebueno", "Pasca", "Puerto Salgar", "Pulí", "Quebradanegra", "Quetame", "Quipile", "Ricaurte", "San Antonio del Tequendama", "San Bernardo", "San Cayetano", "San Francisco", "San Juan de Rioseco", "Sasaima", "Sesquilé", "Sibaté", "Silvania", "Simijaca", "Soacha", "Sopó", "Subachoque", "Suesca", "Supatá", "Susa", "Sutatausa", "Tabio", "Tausa", "Tena", "Tenjo", "Tibacuy", "Tibiritá", "Tocaima", "Tocancipá", "Topaipí", "Ubalá", "Ubaque", "Ubaté", "Une", "Útica", "Venecia", "Vergara", "Vianí", "Villagómez", "Villapinzón", "Villeta", "Viotá", "Yacopí", "Zipacón", "Zipaquirá", "Inírida", "Barranco Minas", "Cacahual", "La Guadalupe", "Mapiripana", "Morichal Nuevo", "Pana Pana", "Puerto Colombia", "San Felipe", "San José del Guaviare", "Calamar", "El Retorno", "Miraflores", "Neiva", "Acevedo", "Agrado", "Aipe", "Algeciras", "Altamira", "Baraya", "Campoalegre", "Colombia", "El Pital", "Elías", "Garzón", "Gigante", "Guadalupe", "Obrero temporal", "Iquira", "Isnos", "La Argentina", "La Plata", "Nátaga", "Oporapa", "Paicol", "Palermo", "Palestina", "Pitalito", "Rivera", "Saladoblanco", "San Agustín", "Santa Maria", "Suaza", "Tarqui", "Tello", "Teruel", "Tesalia", "Timaná", "Villavieja", "Yaguará", "Riohacha", "Albania", "Barrancas", "Dibulla", "Distracción", "El Molino", "Fonseca", "Hatonuevo", "La Jagua del Pilar", "Maicao", "Manaure", "San Juan del Cesar", "Uribia", "Urumita", "Villanueva", "Santa marta", "Algarrobo", "Aracataca", "Ariguaní", "Cerro San Antonio", "Chibolo", "Ciénaga", "Concordia", "El Banco", "El Piñón", "El Retén", "Fundación", "Guamal", "Nueva Granada", "Pedraza", "Pijiño del Carmen", "Pivijay", "Platón", "Puebloviejo", "Remolino", "Sabanas de San Ángel", "Salamina", "San Sebastián de Buenavista", "San Zenón", "Santa Ana", "Santa Bárbara de Pinto", "Sitionuevo", "Tenerife", "Zapayán", "Zona Bananera", "Villavicencio", "Acacías", "Barranca de Upía", "Cabuyaro", "Castilla la Nueva", "Cubarral", "Cumaral", "El Calvario", "El Castillo", "El Dorado", "Fuente de oro", "Granada", "Guamal", "La Macarena", "Lejanías", "Mapiripán", "Mesetas", "Puerto Concordia", "Puerto Gaitán", "Puerto Lleras", "Puerto López", "Puerto Rico", "Restrepo", "San Carlos de Guaroa", "San Juan de Arama", "San Juanito", "San Martín", "La Uribe", "Vista Hermosa", "San Juan de Pasto", "Albán", "Aldana", "Ancuya", "Arboleda", "Barbacoas", "Belén", "Buesaco", "Chachagüí", "Colon", "Consaca", "Contadero", "Córdoba", "Cuaspud", "Cumbal", "Cumbitara", "El Charco", "El Peñol", "El rosario", "El Tablón", "El Tambo", "Francisco Pizarro", "Funes", "Guachucal", "Guaitarilla", "Gualmatán", "Iles", "Imués", "Ipiales", "La Cruz", "La Florida", "La Llanada", "La Tola", "La Unión", "Leiva", "Linares", "Los Andes", "Magüí Payán", "Mallama", "Mosquera", "Nariño", "Olaya Herrera", "Ospina", "Policarpa", "Potosí", "Providencia", "Puerres", "Pupiales", "Ricaurte", "Roberto Payán", "Samaniego", "San Bernardo", "San Lorenzo", "San Pablo", "San pedro de cartago", "Sandoná", "Santa Bárbara", "Santa Cruz", "Sapuyes", "Taminango", "Tangua", "Tumaco", "Túquerres", "Yacuanquer", "Cúcuta", "Abrego", "Arboledas", "Bochalema", "Bucarasica", "Cáchira", "Cácota", "Chinácota", "Chitagá", "Convención", "Cucutilla", "Duranía", "El Carmen", "El Tarra", "El Zulia", "Gramalote", "Hacarí", "Herrán", "La Esperanza", "La Playa de Belén", "Labateca", "Los Patios", "Lourdes", "Mutiscua", "Ocaña", "Pamplona", "Pamplonita", "Puerto Santander", "Ragonvalia", "Salazar de las Palmas", "San Calixto", "San Cayetano", "Santiago", "Sardinata", "Silos", "Teorama", "Tibú", "Toledo", "Villa Caro", "Villa del Rosario", "Mocoa", "Colon", "Puerto Leguízamo", "Orito", "Puerto Asís", "Puerto Caicedo", "Puerto Guzmán", "San Francisco", "San Miguel", "Santiago", "Sibundoy", "Valle del Guamuez", "Villagarzón", "Armenia", "Buena Vista", "Calarcá", "Circasia", "Córdoba", "Filandia", "Génova", "La Tebaida", "Montenegro", "Pijao", "Quimbaya", "Salento", "Pereira", "Apía", "Balboa", "Belén de Umbría", "Dosquebradas", "Guática", "La Celia", "La Virginia", "Marsella", "Mistrató", "Pueblo Rico", "Quinchía", "Santa Rosa de Cabal", "Santuario", "San Andrés", "Providencia", "Bucaramanga", "Aguada", "Albania", "Aratoca", "Barbosa", "Barichara", "Barrancabermeja", "Betulia", "Bolívar", "Cabrera", "California", "Capitanejo", "Carcasí", "Cepitá", "Cerrito", "Charalá", "Charta", "Chima", "Chipatá", "Cimitarra", "Concepción", "Fronteras", "Contratación", "Coromoro", "Curití", "El Carmen de Chucurí", "El Guacamayo", "El Peñón", "El Playón", "Encino", "Enciso", "Florián", "Floridablanca", "Galán", "Gámbita", "Guaca", "Guadalupe", "Guapotá", "Guavatá", "Güepsa", "Hato", "Jesús María", "Jordán", "La Belleza", "La paz", "Landázuri", "Lebrija", "los Santos", "Macaravita", "Málaga", "Matanza", "Mogotes", "Molagavita", "Ocamonte", "Oiba", "Onzaga", "De la palma", "Palmas del Socorro", "Páramo", "Piedecuesta", "Pinchote", "Puente Nacional", "Puerto Parra", "Puerto Wilches", "Rionegro", "Sabana de Torres", "San Andrés", "San Benito", "San Gil", "San Joaquín", "San José de Miranda", "San Juan de Girón", "San Miguel", "San Vicente de Chucurí", "Santa Bárbara", "Santa Helena del Opón", "Simacota", "Socorro", "Suaita", "Sucre", "Suratá", "Tona", "Valle de San José", "Vélez", "Vetas", "Villanueva", "Zapatoca", "Sincelejo", "Buena Vista", "Caimito", "Chalán", "Colosó", "Corozal", "Coveñas", "El Roble", "Galeras", "Guaranda", "La Unión", "Los Palmitos", "Majagual", "Morroa", "Ovejas", "Palmito", "Sampués", "San Benito Abad", "San Juan Betulia", "San Marcos", "San Onofre", "San Pedro", "San Luis de Sincé", "Sucre", "Toluviejo", "Tolú", "Ibagué", "Alpujarra", "Alvarado", "Ambalema", "Anzoátegui", "Armero", "Un taco", "Cajamarca", "Carmen de Apicalá", "Casabianca", "Chaparral", "Coello", "Coyaima", "Cunday", "Dolores", "Espinal", "Falan", "Flandes", "Fresno", "Guamo", "Herveo", "Honda", "Icononzo", "Lérida", "Líbano", "Mariquita", "Melgar", "Murillo", "Natagaima", "Ortega", "Palocabildo", "Piedras", "Planadas", "Prado", "Purificación", "Rioblanco", "Roncesvalles", "Rovira", "Saldaña", "San Antonio", "San Luis", "Santa isabel", "Suárez", "Valle de San Juan", "Venadillo", "Villahermosa", "Villarrica", "Cali", "Alcalá", "Andalucía", "Ansermanuevo", "Argelia", "Bolívar", "Buenaventura", "Bugalagrande", "Caicedonia", "Calima", "Candelaria", "Cartago", "Dagua", "El Águila", "El Cairo", "El Cerrito", "El Dovio", "Florida", "Ginebra", "Guacarí", "Guadalajara de Buga", "Jamundí", "La Cumbre", "La Unión", "La Victoria", "Obando", "Palmira", "Pradera", "Restrepo", "Riofrío", "Roldanillo", "San Pedro", "Sevilla", "Toro", "Trujillo", "Tuluá", "Ulloa", "Versalles", "Vijes", "Yotoco", "Yumbo", "Zarzal", "Mitú", "Carurú", "Pacoa", "Papunahua", "Taraira", "Yavaraté", "Puerto Carreño", "Cumaribo", "La Primavera", "Santa Rosalía" };
            List<CitysDto> Citys = new List<CitysDto>();
            var citysOutput = citysSource.Where(c => c.ToLower().Contains(searchTerm.ToLower())).OrderBy(c => c);
            Citys = citysOutput.Select(s => new CitysDto { Name = s, Value = s }).ToList();
            return Json(new { result = Citys }, JsonRequestBehavior.AllowGet);
        }

    }
}