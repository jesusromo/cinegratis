// <auto-generated />
namespace Kelloggs.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class FirstSchema : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(FirstSchema));
        
        string IMigrationMetadata.Id
        {
            get { return "202202042331267_FirstSchema"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
