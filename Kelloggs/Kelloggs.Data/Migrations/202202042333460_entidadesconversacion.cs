namespace Kelloggs.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class entidadesconversacion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LogChat",
                c => new
                    {
                        LogChatId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                        Etapa = c.Int(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.LogChatId);
            
            CreateTable(
                "dbo.Pregunta",
                c => new
                    {
                        PreguntaId = c.Guid(nullable: false),
                        Etapa = c.Int(nullable: false),
                        Izquierda = c.Boolean(nullable: false),
                        Texto = c.String(),
                        Hora = c.String(),
                        EsBoton = c.Boolean(nullable: false),
                        SiguienteEtapa = c.Int(nullable: false),
                        NombreImagen = c.String(),
                    })
                .PrimaryKey(t => t.PreguntaId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pregunta");
            DropTable("dbo.LogChat");
        }
    }
}
