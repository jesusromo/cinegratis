namespace Kelloggs.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdentificadoresChat : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.LogChat");
            DropPrimaryKey("dbo.Pregunta");
            DropColumn("dbo.LogChat", "LogChatId");
            DropColumn("dbo.Pregunta", "PreguntaId");
            AddColumn("dbo.LogChat", "LogChatId", c => c.Long(nullable: false, identity: true));
            AddColumn("dbo.Pregunta", "PreguntaId", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.LogChat", "LogChatId");
            AddPrimaryKey("dbo.Pregunta", "PreguntaId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Pregunta");
            DropPrimaryKey("dbo.LogChat");
            DropColumn("dbo.LogChat", "LogChatId");
            DropColumn("dbo.Pregunta", "PreguntaId");
            AddColumn("dbo.Pregunta", "PreguntaId", c => c.Guid(nullable: false));
            AddColumn("dbo.LogChat", "LogChatId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Pregunta", "PreguntaId");
            AddPrimaryKey("dbo.LogChat", "LogChatId");
        }
    }
}
