namespace Kelloggs.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModificacionPremios : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Premio", "CodigoCine", c => c.String());
            AddColumn("dbo.Premio", "PrefijoArchivo", c => c.String());
            AddColumn("dbo.Premio", "NombreArchivo", c => c.String());
            AddColumn("dbo.Premio", "Entregado", c => c.Boolean(nullable: false));
            AddColumn("dbo.Tickets", "ProductImageMembrana", c => c.Binary());
            AddColumn("dbo.Tickets", "LoteA", c => c.String());
            AddColumn("dbo.Tickets", "LoteB", c => c.String());
            AddColumn("dbo.Tickets", "PremioId", c => c.Guid(nullable: false));
            CreateIndex("dbo.Tickets", "PremioId");
            AddForeignKey("dbo.Tickets", "PremioId", "dbo.Premio", "PremioId", cascadeDelete: true);
            DropColumn("dbo.Tickets", "ProductImageLote");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "ProductImageLote", c => c.Binary());
            DropForeignKey("dbo.Tickets", "PremioId", "dbo.Premio");
            DropIndex("dbo.Tickets", new[] { "PremioId" });
            DropColumn("dbo.Tickets", "PremioId");
            DropColumn("dbo.Tickets", "LoteB");
            DropColumn("dbo.Tickets", "LoteA");
            DropColumn("dbo.Tickets", "ProductImageMembrana");
            DropColumn("dbo.Premio", "Entregado");
            DropColumn("dbo.Premio", "NombreArchivo");
            DropColumn("dbo.Premio", "PrefijoArchivo");
            DropColumn("dbo.Premio", "CodigoCine");
        }
    }
}
