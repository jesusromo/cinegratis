namespace Kelloggs.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FirstSchema : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CodigosErroneosHistorial",
                c => new
                    {
                        CodigosErroneosHistorialId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                        Codigo = c.String(),
                        Razon = c.String(),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CodigosErroneosHistorialId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Guid(nullable: false),
                        AspNetUserId = c.Guid(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        Nombre = c.String(),
                        Apellidos = c.String(),
                        CorreoElectronico = c.String(),
                        FechaNacimiento = c.DateTime(nullable: false),
                        Direccion = c.String(),
                        Ciudad = c.String(),
                        Celular = c.String(),
                        IP = c.String(),
                        Rol = c.String(),
                        Cedula = c.String(),
                        Genero = c.String(),
                        Folio = c.Int(nullable: false),
                        Tycos = c.Int(nullable: false),
                        PremioEspecialId = c.Guid(),
                        UsuarioEstatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.PremioEspecial", t => t.PremioEspecialId)
                .Index(t => t.PremioEspecialId);
            
            CreateTable(
                "dbo.CodigoUsuario",
                c => new
                    {
                        CodigoUsuarioId = c.Guid(nullable: false),
                        FechaVencimiento = c.DateTime(nullable: false),
                        FechaCanje = c.DateTime(nullable: false),
                        Lote = c.String(),
                        MonthLote = c.Int(nullable: false),
                        YearLote = c.Int(nullable: false),
                        Gramos = c.Int(nullable: false),
                        TipoProducto = c.String(),
                        SubProductoId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.CodigoUsuarioId)
                .ForeignKey("dbo.SubProducto", t => t.SubProductoId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.SubProductoId)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.SubProducto",
                c => new
                    {
                        SubProductoId = c.Guid(nullable: false),
                        Nombre = c.String(),
                        CodigoBarras = c.String(),
                        Gramaje = c.String(),
                        Puntaje = c.Double(nullable: false),
                        FechaDeCreacion = c.DateTime(nullable: false),
                        ProductoId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.SubProductoId)
                .ForeignKey("dbo.Producto", t => t.ProductoId, cascadeDelete: true)
                .Index(t => t.ProductoId);
            
            CreateTable(
                "dbo.Producto",
                c => new
                    {
                        ProductoId = c.Guid(nullable: false),
                        Nombre = c.String(),
                        FechaDeCreacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ProductoId);
            
            CreateTable(
                "dbo.Ganador",
                c => new
                    {
                        GanadorId = c.Guid(nullable: false),
                        ValorCodigo = c.String(),
                        TipoDePremio = c.Int(nullable: false),
                        FechaDeCreacion = c.DateTime(nullable: false),
                        UsuarioId = c.Guid(),
                        PremioId = c.Guid(nullable: false),
                        PremioEspecialId = c.Guid(),
                        Semana = c.Int(),
                        Grupo = c.String(),
                        GrupoAlias = c.String(),
                    })
                .PrimaryKey(t => t.GanadorId)
                .ForeignKey("dbo.Premio", t => t.PremioId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId)
                .Index(t => t.UsuarioId)
                .Index(t => t.PremioId);
            
            CreateTable(
                "dbo.Premio",
                c => new
                    {
                        PremioId = c.Guid(nullable: false),
                        Item = c.String(),
                        Marca = c.String(),
                        Cantidad = c.Int(nullable: false),
                        Descripcion = c.String(),
                        CantidadDisponible = c.Int(nullable: false),
                        FechaDeCreacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PremioId);
            
            CreateTable(
                "dbo.Mensajes",
                c => new
                    {
                        MensajeId = c.Guid(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        Mensaje = c.String(),
                        MensajeEstatus = c.Int(nullable: false),
                        TycosIntento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MensajeId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.PremioEspecial",
                c => new
                    {
                        PremioEspecialId = c.Guid(nullable: false),
                        Codigo = c.String(),
                        TipoCuenta = c.String(),
                        FechaDeCreacion = c.DateTime(nullable: false),
                        FechaDeCanje = c.DateTime(),
                    })
                .PrimaryKey(t => t.PremioEspecialId);
            
            CreateTable(
                "dbo.PromoSetting",
                c => new
                    {
                        PromoSettingId = c.Guid(nullable: false),
                        TipoConfig = c.String(),
                        EsActivo = c.Boolean(nullable: false),
                        Fecha = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PromoSettingId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.TicketCompra",
                c => new
                    {
                        TicketCompraId = c.Guid(nullable: false),
                        FechaSubido = c.DateTime(nullable: false),
                        EsValido = c.Int(nullable: false),
                        Ticket = c.Binary(),
                        UsuarioId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.TicketCompraId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.Guid(nullable: false),
                        FechaRegistro = c.DateTime(nullable: false),
                        Folio = c.String(),
                        FechaLote = c.String(),
                        CodigoBarras = c.String(),
                        FechaCompra = c.String(),
                        EsValido = c.Boolean(nullable: false),
                        Cargado = c.Boolean(nullable: false),
                        Puntos = c.Double(nullable: false),
                        ValidadoAdmin = c.Int(nullable: false),
                        Semana = c.Int(nullable: false),
                        Archivo = c.String(),
                        Data = c.String(),
                        ProductImageLote = c.Binary(),
                        ProductImageBarras = c.Binary(),
                        Proveedor = c.Int(nullable: false),
                        UsuarioId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Tickets", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.TicketCompra", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Usuarios", "PremioEspecialId", "dbo.PremioEspecial");
            DropForeignKey("dbo.Mensajes", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Ganador", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Ganador", "PremioId", "dbo.Premio");
            DropForeignKey("dbo.CodigoUsuario", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.SubProducto", "ProductoId", "dbo.Producto");
            DropForeignKey("dbo.CodigoUsuario", "SubProductoId", "dbo.SubProducto");
            DropForeignKey("dbo.CodigosErroneosHistorial", "UsuarioId", "dbo.Usuarios");
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Tickets", new[] { "UsuarioId" });
            DropIndex("dbo.TicketCompra", new[] { "UsuarioId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Mensajes", new[] { "UsuarioId" });
            DropIndex("dbo.Ganador", new[] { "PremioId" });
            DropIndex("dbo.Ganador", new[] { "UsuarioId" });
            DropIndex("dbo.SubProducto", new[] { "ProductoId" });
            DropIndex("dbo.CodigoUsuario", new[] { "UsuarioId" });
            DropIndex("dbo.CodigoUsuario", new[] { "SubProductoId" });
            DropIndex("dbo.Usuarios", new[] { "PremioEspecialId" });
            DropIndex("dbo.CodigosErroneosHistorial", new[] { "UsuarioId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Tickets");
            DropTable("dbo.TicketCompra");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.PromoSetting");
            DropTable("dbo.PremioEspecial");
            DropTable("dbo.Mensajes");
            DropTable("dbo.Premio");
            DropTable("dbo.Ganador");
            DropTable("dbo.Producto");
            DropTable("dbo.SubProducto");
            DropTable("dbo.CodigoUsuario");
            DropTable("dbo.Usuarios");
            DropTable("dbo.CodigosErroneosHistorial");
        }
    }
}
