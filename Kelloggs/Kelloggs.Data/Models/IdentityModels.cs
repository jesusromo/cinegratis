﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Kelloggs.Data.Models
{
    // Para agregar datos de perfil del usuario, agregue más propiedades a su clase ApplicationUser. Visite https://go.microsoft.com/fwlink/?LinkID=317594 para obtener más información.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("SqlConnectionString", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<CodigosErroneosHistorial> CodigosErroneosHistorial { get; set; }
        public DbSet<CodigoUsuario> CodigoUsuario { get; set; }
        public DbSet<Ganador> Ganadores { get; set; }
        public DbSet<Premio> Premios { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<PremioEspecial> PremiosEspeciales { get; set; }

        public DbSet<PromoSetting> PromoSettings { get; set; }
        public DbSet<SubProducto> SubProductos { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Mensajes> Mensajes { get; set; }
        public DbSet<Tickets> Tickets { get; set; }
        public DbSet<TicketCompra> TicketCompra { get; set; }
        public DbSet<LogChat> LogChat { get; set; }
        public DbSet<Pregunta> Preguntas { get; set; }
    }
}