﻿using Kelloggs.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("CodigoUsuario")]
    public class CodigoUsuario
    {
        [Key]
        public Guid CodigoUsuarioId { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime FechaCanje { get; set; }
        public string Lote { get; set; }
        public int MonthLote { get; set; }
        public int YearLote { get; set; }

        [Range(0,1600)]
        public int Gramos { get; set; }
        public string TipoProducto { get; set; }
        public Guid SubProductoId { get; set; }
        public Guid UsuarioId { get; set; }


        public virtual Usuario Usuario { get; set; }
        public virtual SubProducto SubProducto { get; set; }


        public CodigoUsuario()
        {
            CodigoUsuarioId = Guid.NewGuid();
        }
    }


}