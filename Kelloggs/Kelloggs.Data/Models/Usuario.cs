﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kelloggs.Common;
using Kelloggs.Data.Enum;

namespace Kelloggs.Data.Models
{
    [Table("Usuarios")]

    public class Usuario
    {
        [Key]
        public Guid UsuarioId { get; set; }
        public Guid AspNetUserId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public String Nombre { get; set; }
        public String Apellidos { get; set; }
        public String CorreoElectronico { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public String Direccion { get; set; }
        public String Ciudad { get; set; }
        public String Celular { get; set; }
        public String IP { get; set; }
        public String Rol { get; set; }
        public String Cedula { get; set; }
        public String Genero { get; set; }
        public int Folio { get; set; }
        public Tycos Tycos { get; set; }
        public Guid? PremioEspecialId { get; set; }
        public UsuarioEstatus UsuarioEstatus { get; set; }


        public Usuario()
        {
            UsuarioId = Guid.NewGuid();
            FechaRegistro = Utilidades.DateNow();
            UsuarioEstatus = UsuarioEstatus.ACTIVO;
            Tycos = Tycos.SIN_ACEPTAR;
        }

    }
}
