﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kelloggs.Data.Models
{
    [Table("LogChat")]
    public class LogChat
    {
        [Key]
        public long LogChatId { get; set; }
        public Guid UsuarioId { get; set; }
        public int Etapa { get; set; }
        public DateTime Fecha { get; set; }
    }
}
