﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kelloggs.Common;
using Kelloggs.Data.Enum;

namespace Kelloggs.Data.Models
{
    [Table("Mensajes")]
    public class Mensajes
    {
        [Key]
        public Guid MensajeId { get; set; }
        public Guid UsuarioId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Mensaje { get; set; }
        public MensajeEstatus MensajeEstatus { get; set; }
        public int TycosIntento { get; set; }
        public virtual Usuario Usuarios { get; set; }

        public Mensajes()
        {
            MensajeId = Guid.NewGuid();
            FechaRegistro = Utilidades.DateNow();
        }
    }
}
