﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("SubProducto")]
    public class SubProducto
    {

        public SubProducto()
        {
            this.CodigoUsuarios = new HashSet<CodigoUsuario>();
        }

        [Key]
        public System.Guid SubProductoId { get; set; }
        public string Nombre { get; set; }
        public string CodigoBarras { get; set; }
        public string Gramaje { get; set; }
        public double Puntaje { get; set; }
        public System.DateTime FechaDeCreacion { get; set; }
        public System.Guid ProductoId { get; set; }

        public virtual ICollection<CodigoUsuario> CodigoUsuarios { get; set; }

        public virtual Producto Producto { get; set; }
    }
}