﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kelloggs.Common;
using Kelloggs.Data.Enum;

namespace Kelloggs.Data.Models
{
    [Table("TicketCompra")]
    public class TicketCompra
    {
        [Key]
        public Guid TicketCompraId { get; set; }
        public DateTime FechaSubido { get; set; }
        public EsValido EsValido { get; set; }
        public byte[] Ticket { get; set; }
        public Guid UsuarioId { get; set; }

        public virtual Usuario Usuarios { get; set; }

        public TicketCompra()
        {
            TicketCompraId = Guid.NewGuid();
            FechaSubido = Utilidades.DateNow();
            EsValido = EsValido.PENDIENTE;
        }
    }
}
