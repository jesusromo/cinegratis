﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("PremioEspecial")]

    public class PremioEspecial
    {
        public PremioEspecial()
        {
            this.Usuarios = new HashSet<Usuario>();
        }

        [Key]
        public System.Guid PremioEspecialId { get; set; }
        public string Codigo { get; set; }
        public string TipoCuenta { get; set; }
        public System.DateTime FechaDeCreacion { get; set; }
        public System.DateTime? FechaDeCanje { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}