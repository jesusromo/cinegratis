﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("Producto")]

    public class Producto
    {
        public Producto()
        {
            this.SubProductos = new HashSet<SubProducto>();
        }

        [Key]
        public System.Guid ProductoId { get; set; }
        public string Nombre { get; set; }
        public System.DateTime FechaDeCreacion { get; set; }
        public virtual ICollection<SubProducto> SubProductos { get; set; }
    }
}