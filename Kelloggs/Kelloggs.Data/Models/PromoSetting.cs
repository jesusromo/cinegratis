﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("PromoSetting")]
    public class PromoSetting
    {
        [Key]
        public Guid PromoSettingId { get; set; }
        public String TipoConfig { get; set; }
        public bool EsActivo { get; set; }
        public DateTime Fecha { get; set; }       
    }
}