﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Kelloggs.Common;
using Kelloggs.Data.Enum;

namespace Kelloggs.Data.Models
{
    [Table("Tickets")]
    public class Tickets
    {
        [Key]
        public Guid TicketId { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Folio { get; set; }
        public string FechaLote { get; set; }
        public string CodigoBarras { get; set; }
        public string FechaCompra { get; set; }
        public bool EsValido { get; set; }
        public bool Cargado { get; set; }
        public double Puntos { get; set; }
        public ValidadoAdmin ValidadoAdmin { get; set; }
        public int Semana { get; set; }
        public string Archivo { get; set; } //imagen lote y fecha
        public string Data { get; set; } //imagen codigo de barras

        public byte[] ProductImageMembrana { get; set; }

        public byte[] ProductImageBarras { get; set; }

        public string LoteA{ get; set; }
        public string LoteB { get; set; }

        public Proveedor Proveedor { get; set; }
        public Guid UsuarioId { get; set; }
        public Guid PremioId { get; set; }

        public virtual Usuario Usuarios { get; set; }
        public virtual Premio Premios { get; set; }

        /// <summary>
        /// Cosntructor.
        /// </summary>
        public Tickets()
        {
            TicketId = Guid.NewGuid();
            FechaRegistro = Utilidades.DateNow();
            ValidadoAdmin = ValidadoAdmin.SINVALIDAR;
        }
    }
}
