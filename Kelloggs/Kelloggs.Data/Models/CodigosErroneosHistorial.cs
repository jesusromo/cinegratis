﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kelloggs.Data.Models
{
    [Table("CodigosErroneosHistorial")]
    public class CodigosErroneosHistorial
    {
        [Key]
        public Guid CodigosErroneosHistorialId { get; set; }
        public Guid UsuarioId { get; set; }
        public String Codigo { get; set; }
        public String Razon { get; set; }
        public DateTime Fecha { get; set; }


        public virtual Usuario Usuario { get; set; }

        public CodigosErroneosHistorial()
        {
            CodigosErroneosHistorialId = Guid.NewGuid();
        }
    }
}