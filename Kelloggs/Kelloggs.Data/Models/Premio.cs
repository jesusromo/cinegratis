﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.Eventing.Reader;

namespace Kelloggs.Data.Models
{
    [Table("Premio")]

    public class Premio
    {
        public Premio()
        {
            this.Ganadores = new HashSet<Ganador>();
        }

        [Key]
        public System.Guid PremioId { get; set; }
        public string Item { get; set; }
        public string Marca { get; set; }
        public int Cantidad { get; set; }
        public string Descripcion { get; set; }
        public string CodigoCine { get; set; }
        public string PrefijoArchivo { get; set; }
        public string NombreArchivo { get; set; }
        public bool Entregado { get; set; }
        public int CantidadDisponible { get; set; }
        public System.DateTime FechaDeCreacion { get; set; }
        public virtual ICollection<Ganador> Ganadores { get; set; }
    }
}