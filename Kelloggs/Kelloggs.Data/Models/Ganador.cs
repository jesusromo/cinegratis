﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kelloggs.Data.Models
{
    [Table("Ganador")]
    public class Ganador
    {
        [Key]
        public System.Guid GanadorId { get; set; }
        public string ValorCodigo { get; set; }
        public int TipoDePremio { get; set; }  
        public System.DateTime FechaDeCreacion { get; set; }
        public Nullable<System.Guid> UsuarioId { get; set; }
        public System.Guid PremioId { get; set; }
        public System.Guid? PremioEspecialId { get; set; }
        public int? Semana { get; set; }
        public string Grupo { get; set; }
        public string GrupoAlias { get; set; }
        public virtual Premio Premio { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}