﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kelloggs.Data.Models
{
    [Table("Pregunta")]

    public class Pregunta
    {
        [Key]
        public long PreguntaId { get; set; }
        public int Etapa { get; set; }
        public bool Izquierda { get; set; }
        public string Texto { get; set; }
        public string Hora { get; set; }
        public bool EsBoton { get; set; }
        public int SiguienteEtapa { get; set; }
        public string NombreImagen { get; set; }

    }
}
