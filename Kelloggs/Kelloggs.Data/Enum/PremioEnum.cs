﻿namespace Kelloggs.Data.Enum
{
    public enum PremioEnum
    {
        MesasPortables = 0,
        PortatilLenovo = 1,
        TabletLenovo = 2,
        AudifonosSony = 3,
        MaletasKellogs = 4,
        MouseGenius = 5
    }

}