﻿namespace Kelloggs.Data.Enum
{
    public enum TipoArchivo
    {
        Lote = 1,
        Fecha = 2,
        CodigoBarras = 3,
        LoteYFecha = 4,
        Invalido = 5,
    }
}