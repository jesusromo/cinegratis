﻿namespace Kelloggs.Data.Enum
{
    public enum ValidadoAdmin
    {
        VALIDADO,
        SINVALIDAR,
        RECHAZADO
    }
}
