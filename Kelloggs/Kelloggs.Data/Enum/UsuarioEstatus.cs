﻿namespace Kelloggs.Data.Enum
{
    public enum UsuarioEstatus
    {
        BLOQUEADO,
        ACTIVO,
        CREADO
    }
}
