﻿namespace Kelloggs.Data.Enum
{
	public enum Proveedor
    {
        BODEGA_AURRERA,
        CHEDRAUI,
        CALIMAX,
        MEGA_SORIANA,
        OXXO,
        SORIANA_HIPER,
        WALMART,
        COMEXA,
        SUPERAMA,
        FARMACIAS_GUADALAJARA,
        SEVEN_ELEVEN,
        HEB,
        FUTURAMA,
        EXTRA,
        CITY_FRESKO,
        RAPPI,
        CITY_CLUB,
        GARIS
    }
}
